#!/usr/bin/env bash

CWD=$(pwd)

docker run \
    -v "$CWD":/home/sw-cli-tools/src/Extensions/ShopwarePrivate \
    sw-cli-tools-plugins \
    vendor/bin/phpunit --configuration src/Extensions/ShopwarePrivate/phpunit.xml.dist