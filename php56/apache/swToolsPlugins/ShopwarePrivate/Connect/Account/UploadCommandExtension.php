<?php

namespace ShopwarePrivate\Connect\Account;

use Shopware\Plugin\Struct\Plugin;

class UploadCommandExtension extends \ShopwarePrivate\Account\Command\UploadCommand
{
    const RELEASE_TAG_PREFIX = 'release/plugin_';

    protected function configure()
    {
        parent::configure();

        $this->setName('connect:upload');
    }

    protected function execute(\Symfony\Component\Console\Input\InputInterface $input, \Symfony\Component\Console\Output\OutputInterface $output)
    {
        if ($input->getArgument('names')) {
            throw new \InvalidArgumentException('SwagConnect is always used for zipping. Don\'t provide a name.');
        }

        $input->setArgument('names', [ 'SwagConnect' ]);
        return parent::execute($input, $output);
    }

    /**
     * @param Plugin $plugin
     * @param $params
     */
    public function doUpdate($plugin, $params)
    {
        $plugin->module = 'Backend';
        parent::doUpdate($plugin, $params);
    }


    protected function tag($dir, $tag)
    {
        parent::tag($dir, self::RELEASE_TAG_PREFIX . $tag);
    }
}