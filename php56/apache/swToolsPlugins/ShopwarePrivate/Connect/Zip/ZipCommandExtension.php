<?php

namespace ShopwarePrivate\Connect\Zip;

use Shopware\Plugin\Command\ZipCommand;
use Shopware\Plugin\Struct\Plugin;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ZipCommandExtension extends ZipCommand
{
    protected function configure()
    {
        parent::configure();

        $this
            ->setName('connect:zip:vcs')
            ->setDescription('Creates a zip for the SwagConnect plugin.');
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        if ($input->getArgument('names')) {
            throw new \InvalidArgumentException('SwagConnect is always used for zipping. Don\'t provide a name.');
        }

        $input->setArgument('names', [ 'SwagConnect' ]);
        return parent::execute($input, $output);
    }

    /**
     * @param Plugin $plugin
     * @param array $params
     */
    public function doZip($plugin, $params)
    {
        if ($plugin->name === 'SwagConnect') {
            $plugin->module = 'Backend';
        }

        parent::doZip($plugin, $params);
    }
}