<?php

namespace ShopwarePrivate\License2Sql\Command;

use ShopwareCli\Command\BaseCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class License2Sql extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('license2sql')
            ->setDescription('Convert a shopware plaintext license to sql')
            ->addArgument(
                'file',
                InputArgument::REQUIRED,
                'License file'
            )
            ->setHelp(<<<EOF
The <info>%command.name%</info> converts a given license file to sql.
EOF
            );
        ;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = $input->getArgument('file');

        try {
            $finalSql = $this->convertLicenseToSql($file);
        } catch (\RuntimeException $e) {
            $output->writeln($e);
            exit(1);
        }

        $output->writeln("-- Generated SQL insert");
        $output->writeln("");
        $output->writeln($finalSql);
    }


    /**
     * @param $file
     * @return string
     */
    private function convertLicenseToSql($file)
    {
        $fileContent = file_get_contents($file);
        $licenseStrings = $this->splitLicences($fileContent);

        if (empty($licenseStrings)) {
            throw new \RuntimeException("No licences found.");
        }

        $sql = 'INSERT INTO `s_core_licenses` (`module`, `host`, `label`, `license`, `version`, `notation`, `type`, `source`, `added`, `creation`, `expiration`, `active`, `plugin_id`) VALUES';
        $sqlParts = [];


        foreach ($licenseStrings as $licenseString) {
            $info = $this->readLicenseInfo($licenseString);
            if (!$info) {
                throw new \RuntimeException("Could not read license info");
            }

            $sqlParts[] = $this->info2Sql($info);
        }


        $finalSql = $sql . implode(', ', $sqlParts);
        return $finalSql;
    }


    /**
     * @param  string $fileContent
     * @return string[]
     */
    private function splitLicences($fileContent)
    {
        $start = '-------- LICENSE BEGIN ---------';
        $end = '--------- LICENSE END ----------';

        $matches = $this->getStringBetween($start, $end, $fileContent);

        return $matches;
    }

    /**
     * @param  string $start
     * @param  string $end
     * @param  string $subject
     * @return string[]
     */
    private function getStringBetween($start, $end, $subject)
    {
        $delimiter = '#';
        $regex = $delimiter
            . preg_quote($start, $delimiter)
            . '(.*?)'
            . preg_quote($end, $delimiter)
            . $delimiter
            . 's';

        $matches = array();
        if (!preg_match_all($regex, $subject, $matches)) {
            return array();
        }

        return $matches[0];
    }


    /**
     * @param info :
     * (
     *   [label] => SwagExample
     *   [module] => SwagExample
     *   [product] =>
     *   [host] => .demo.shopware.in
     *   [source] => store
     *   [user] =>
     *   [type] => 1
     *   [notation] =>
     *   [expiration] =>
     *   [creation] => 20151029
     *   [version] => 1.0.0
     *   [license] => S00WyT8ssD1u81YW6eZFVx1StzSJXq64ajZxb7V60kznGXXZ79yvH2djSJGI37hGhrnuQ/5FM+m2lFc368t1E60MDa2qi61MrZRyEpNSc5Ssi0EiSsHliemuFYm5BTmpICEzK6Xc/JRSCAeLvLmVUkERUEFyiZK1H5BvYqWUkV9cAlYNlNNLSc3N1yvOyC8oTyxK1cvMg5pZnF9alAw2AGh/cUl+EZgN1FxanFoEYhpYKUFFSioLgJKZVoZAroWVUl5+SWJJZn4exDpDoLrUioLMIqgYXCNQZXJRKlwUyDUyMDQ1NDCyhLq6LLWoGCoJdIKhnoGegZJ1LQA=
     * )
     * @return string
     *
     */
    protected function info2Sql($info)
    {
        return "('{$info['module']}', '{$info['host']}', '{$info['label']}', '{$info['license']}', '{$info['version']}', '{$info['notation']}', '{$info['type']}', '{$info['source']}', NOW(), '{$info['creation']}', '{$info['expiration']}', 1, NULL)";
    }


    /**
     * @param  string $license
     * @return array|bool
     */
    private function readLicenseInfo($license)
    {
        $license = preg_replace('#--.+?--#', '', (string)$license);
        $license = preg_replace('#[^A-Za-z0-9+/=]#', '', $license);

        $info = base64_decode($license);
        if ($info === false) {
            return false;
        }
        $info = gzinflate($info);
        if ($info === false) {
            // License can not be unpacked.
            return false;
        }

        if (strlen($info) > (512 + 60) || strlen($info) < 100) {
            // License too long / short.
            return false;
        }

        $hash = substr($info, 0, 20);
        $coreLicense = substr($info, 20, 20);
        $moduleLicense = substr($info, 40, 20);
        $info = substr($info, 60);

        // test license integrity
        if ($hash !== sha1($coreLicense . $info . $moduleLicense, true)) {
            return false;
        }

        $info = unserialize($info);
        if ($info === false) {
            // Plugin payload can not be unserialized
            return false;
        }

        $info['license'] = $license;
        $info['moduleLicense'] = $moduleLicense;
        $info['coreLicense'] = $coreLicense;

        return $info;
    }
}
