<?php

namespace ShopwarePrivate\Tests\Unit\Translation\Services\Common;

use ShopwarePrivate\Translation\Services\Shopware\PluginMapCreator;

class PluginMapCreatorTest extends \PHPUnit_Framework_TestCase
{
    public function test_it_can_be_created()
    {
        $pluginMapCreator = new PluginMapCreator();
        $this->assertInstanceOf(PluginMapCreator::class, $pluginMapCreator);
    }

    public function test_it_should_detect_lowercase_snippet_folder()
    {
        $pluginMapCreator = new PluginMapCreator();

        $pluginMap = $pluginMapCreator->create(__DIR__ . '/_fixtures/PluginMapCreator');

        $this->assertArraySubset(
            [
                'SwagTest' => __DIR__ . '/_fixtures/PluginMapCreator/engine/Shopware/Plugins/Local/Backend/SwagTest/snippets',
            ],
            $pluginMap
        );
    }

    public function test_it_should_detect_uppercase_snippet_folder()
    {
        $pluginMapCreator = new PluginMapCreator();

        $pluginMap = $pluginMapCreator->create(__DIR__ . '/_fixtures/PluginMapCreator');

        $this->assertArraySubset(
            [
                'SwagUpperCase' => __DIR__ . '/_fixtures/PluginMapCreator/engine/Shopware/Plugins/Local/Core/SwagUpperCase/Snippets',
            ],
            $pluginMap
        );
    }

    public function test_it_should_detect_plugins_stored_in_local_module_folder()
    {
        $pluginMapCreator = new PluginMapCreator();

        $pluginMap = $pluginMapCreator->create(__DIR__ . '/_fixtures/PluginMapCreator');

        $this->assertArraySubset(
            [
                'SwagLocale' => __DIR__ . '/_fixtures/PluginMapCreator/engine/Shopware/Plugins/Local/Frontend/SwagLocale/Snippets',
            ],
            $pluginMap
        );
    }

    public function test_it_should_detect_plugins_stored_in_default_module_folder()
    {
        $pluginMapCreator = new PluginMapCreator();

        $pluginMap = $pluginMapCreator->create(__DIR__ . '/_fixtures/PluginMapCreator');

        $this->assertArraySubset(
            [
                'SwagDefault' => __DIR__ . '/_fixtures/PluginMapCreator/engine/Shopware/Plugins/Default/Frontend/SwagDefault/Snippets',
            ],
            $pluginMap
        );
    }

    public function test_it_should_ignore_invalid_plugin_files_and_directories()
    {
        $this->assertFileExists(__DIR__ . '/_fixtures/PluginMapCreator/engine/Shopware/Plugins/Local/Frontend/example.txt');

        $pluginMapCreator = new PluginMapCreator();

        $pluginMap = $pluginMapCreator->create(__DIR__ . '/_fixtures/PluginMapCreator');

        $this->assertArrayNotHasKey('example_new_system.txt', $pluginMap);
        $this->assertArrayNotHasKey('example.txt', $pluginMap);
    }

    public function test_it_should_map_plugins_based_on_the_new_plugin_system()
    {
        $pluginMapCreator = new PluginMapCreator();

        $pluginMap = $pluginMapCreator->create(__DIR__ . '/_fixtures/PluginMapCreator');

        $this->assertArraySubset(
            [
                'SwagNewPluginSystem' => __DIR__ . '/_fixtures/PluginMapCreator/custom/plugins/SwagNewPluginSystem/Resources/snippets'
            ],
            $pluginMap
        );
    }
}