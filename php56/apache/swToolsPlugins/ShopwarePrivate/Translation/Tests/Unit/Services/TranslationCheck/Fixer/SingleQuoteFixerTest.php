<?php

namespace ShopwarePrivate\Translation\Tests\Unit\TranslationCheck\Services\Fixer;

use PHPUnit\Framework\TestCase;
use ShopwarePrivate\Translation\Services\TranslationCheck\Fixer\FixerInterface;
use ShopwarePrivate\Translation\Services\TranslationCheck\Fixer\SingleQuoteFixer;
use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationLine;
use Symfony\Component\Finder\SplFileInfo;

class SingleQuoteFixerTest extends TestCase
{
    public function test_it_should_be_created()
    {
        $singleQuoteFixer = $this->createSingleQuoteFixer();
        $this->assertInstanceOf(SingleQuoteFixer::class, $singleQuoteFixer);
        $this->assertInstanceOf(FixerInterface::class, $singleQuoteFixer);
    }

    public function test_it_should_escape_single_quote()
    {
        $translationLine = new TranslationLine();
        $translationLine->content = <<<EOD
some/index = "ini ' content"
EOD;

        $singleQuoteFixer = $this->createSingleQuoteFixer();

        $result = $singleQuoteFixer->fix($translationLine);

        $this->assertEquals('some/index = "ini \\\' content"', $result);
    }

    public function test_it_should_not_double_escape_single_quotes()
    {
        $translationLine = new TranslationLine();
        $translationLine->content = <<<EOD
some/index = "ini \' content"
EOD;

        $singleQuoteFixer = $this->createSingleQuoteFixer();

        $result = $singleQuoteFixer->fix($translationLine);

        $this->assertEquals('some/index = "ini \\\' content"', $result);
    }

    public function test_it_should_escape_multiple_single_quotes()
    {
        $translationLine = new TranslationLine();
        $translationLine->content = <<<EOD
some/index = "ini ' further' content"
EOD;

        $singleQuoteFixer = $this->createSingleQuoteFixer();

        $result = $singleQuoteFixer->fix($translationLine);

        $this->assertEquals('some/index = "ini \\\' further\\\' content"', $result);
    }

    public function test_it_should_fix_single_quote_given_some_fixed_quotes_and_some_invalid_quotes()
    {
        $translationLine = new TranslationLine();
        $translationLine->content = <<<EOD
some/index = "ini \' further' content \', some quotes ' ' \''"
EOD;

        $singleQuoteFixer = $this->createSingleQuoteFixer();

        $result = $singleQuoteFixer->fix($translationLine);

        $expected = <<<EOD
some/index = "ini \' further\' content \', some quotes \' \' \'\'"
EOD;

        $this->assertEquals($expected, $result);
    }

    /**
     * @return SingleQuoteFixer
     */
    protected function createSingleQuoteFixer()
    {
        return new SingleQuoteFixer();
    }
}

class SplFileInfoDummy extends SplFileInfo
{
    public function __construct()
    {
    }
}