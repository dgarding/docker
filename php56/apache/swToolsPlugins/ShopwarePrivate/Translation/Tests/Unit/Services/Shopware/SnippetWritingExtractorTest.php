<?php

namespace ShopwarePrivate\Translations\Tests\Unit\Services\Shopware;

use ShopwarePrivate\Translation\Services\Data\SnippetFile;
use ShopwarePrivate\Translation\Services\Shopware\SnippetWritingExtractor;

class SnippetWritingExtractorTest extends \PHPUnit_Framework_TestCase
{
    public function test_it_can_be_created()
    {
        $service = new SnippetWritingExtractor();
        $this->assertInstanceOf(SnippetWritingExtractor::class, $service);
    }

    public function test_it_should_extract_snippet_file_into_array_format()
    {
        $snippetFiles = [];
        $snippetFiles[] = new SnippetFile(
            'main.ini',
            '/home/shopware/snippets/backend/view/main.ini',
            'backend/view/main',
            [
                'en_GB' => [
                    'snippet_name' => 'translation'
                ]
            ],
            'core'
        );

        $service = new SnippetWritingExtractor();
        $result = $service->extractForWriting($snippetFiles, [ 'en_GB' ]);

        $this->assertEquals([
            'backend/view/main.ini' => [
                'en_GB' => [
                    'snippet_name' => 'translation'
                ]
            ]
        ], $result);
    }

    public function test_it_should_extract_multiple_locales()
    {
        $snippetFiles = [];
        $snippetFiles[] = new SnippetFile(
            'main.ini',
            '/home/shopware/snippets/backend/view/main.ini',
            'backend/view/main',
            [
                'en_GB' => [
                    'snippet_name' => 'translation'
                ],
                'de_DE' => [
                    'snippet_name' => 'german translation'
                ]
            ],
            'core'
        );

        $service = new SnippetWritingExtractor();
        $result = $service->extractForWriting($snippetFiles, [ 'en_GB', 'de_DE' ]);

        $this->assertEquals([
            'backend/view/main.ini' => [
                'en_GB' => [
                    'snippet_name' => 'translation'
                ],
                'de_DE' => [
                    'snippet_name' => 'german translation'
                ]
            ]
        ], $result);
    }

    public function test_it_should_merge_different_contexts_into_one_snippet_namespace()
    {
        $snippetFiles = [];
        $snippetFiles[] = new SnippetFile(
            'main.ini',
            '/home/shopware/snippets/backend/view/main.ini',
            'backend/view/main',
            [
                'en_GB' => [
                    'snippet_name' => 'translation'
                ]
            ],
            'core'
        );
        $snippetFiles[] = new SnippetFile(
            'main.ini',
            '/home/shopware/snippets/backend/view/main.ini',
            'backend/view/main',
            [
                'en_GB' => [
                    'snippet_name_2' => 'translation 2'
                ]
            ],
            'SwagProductAdvisor'
        );

        $service = new SnippetWritingExtractor();
        $result = $service->extractForWriting($snippetFiles, [ 'en_GB' ]);

        $this->assertEquals([
            'backend/view/main.ini' => [
                'en_GB' => [
                    'snippet_name' => 'translation',
                    'snippet_name_2' => 'translation 2'
                ]
            ]
        ], $result);
    }

}
