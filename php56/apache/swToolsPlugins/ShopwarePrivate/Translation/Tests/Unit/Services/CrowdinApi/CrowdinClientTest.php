<?php

namespace ShopwarePrivate\Translation\Tests\Unit\Services\CrowdinApi;

use ShopwareCli\Config;
use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;

class CrowdinClientTest extends \PHPUnit_Framework_TestCase
{
    public function test_it_can_be_created()
    {
        $crowdinClient = new CrowdinClient(
            new ConfigStub(),
            'projectKey'
        );
        $this->assertInstanceOf(CrowdinClient::class, $crowdinClient);
    }
}

class ConfigStub extends Config
{
    public function __construct()
    {
    }
}