<?php

namespace ShopwarePrivate\Tests\Unit\Translation\Services\Crowdin;

use ShopwarePrivate\Translation\Services\Crowdin\CrowdinTranslationHandler;
use ShopwarePrivate\Translation\Tests\Unit\Mocks\ConfigDummy;
use ShopwarePrivate\Translation\Tests\Unit\Mocks\CrowdinClientDummy;
use ShopwarePrivate\Translation\Tests\Unit\Mocks\CrowdinUnzipperDummy;
use ShopwarePrivate\Translation\Tests\Unit\Mocks\IoServiceDummy;

class CrowdinTranslationHandlerTest extends \PHPUnit_Framework_TestCase
{
    public function test_it_can_be_created()
    {
        $crowdinTranslationHandler = new CrowdinTranslationHandler(
            new ConfigDummy(),
            new IoServiceDummy(),
            new CrowdinClientDummy(),
            new CrowdinUnzipperDummy()
        );
        $this->assertInstanceOf(CrowdinTranslationHandler::class, $crowdinTranslationHandler);
    }
}