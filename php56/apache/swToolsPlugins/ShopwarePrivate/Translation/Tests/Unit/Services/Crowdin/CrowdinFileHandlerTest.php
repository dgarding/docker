<?php

namespace ShopwarePrivate\Tests\Unit\Translation\Services\Crowdin;

use ShopwarePrivate\Translation\Services\Crowdin\CrowdinFileHandler;
use ShopwarePrivate\Translation\Tests\Unit\Mocks\ConfigDummy;
use ShopwarePrivate\Translation\Tests\Unit\Mocks\CrowdinClientDummy;
use ShopwarePrivate\Translation\Tests\Unit\Mocks\IoServiceDummy;

class CrowdinFileHandlerTest extends \PHPUnit_Framework_TestCase
{
    public function test_it_can_be_created()
    {
        $crowdinFileHandler = new CrowdinFileHandler(
            new ConfigDummy(),
            new IoServiceDummy(),
            new CrowdinClientDummy()
        );
        $this->assertInstanceOf(CrowdinFileHandler::class, $crowdinFileHandler);
    }
}