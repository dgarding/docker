<?php

namespace ShopwarePrivate\Tests\Unit\Utils;

use PHPUnit\Framework\TestCase;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;

class LanguageSettingsTest extends TestCase
{
    public function test_convertShopwareToCrowdin_should_return_crowdin_language_code()
    {
        $crowdinLanguageCode = LanguageSettings::convertShopwareToCrowdin('en_GB');
        $this->assertEquals('en', $crowdinLanguageCode);
    }
}