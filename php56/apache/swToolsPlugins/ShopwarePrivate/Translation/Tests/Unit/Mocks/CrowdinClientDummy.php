<?php

namespace ShopwarePrivate\Translation\Tests\Unit\Mocks;

use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;

class CrowdinClientDummy extends CrowdinClient
{
    public function __construct()
    {
    }
}