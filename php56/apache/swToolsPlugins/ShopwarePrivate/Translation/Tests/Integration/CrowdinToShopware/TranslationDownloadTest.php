<?php

namespace ShopwarePrivate\Translation\Tests\Integration\CrowdinToShopware;

use GuzzleHttp\Psr7\Response;
use Shopware\Plugin\Services\Install;
use Shopware\Plugin\Services\PluginProvider;
use Shopware\Plugin\Struct\Plugin;
use ShopwareCli\Tests\CommandTestCaseTrait;
use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;
use Symfony\Component\Process\Process;

class TranslationDownloadTest extends \PHPUnit_Framework_TestCase
{
    use CommandTestCaseTrait;

    public static $translationExportCount = 0;
    public static $translationPluginInstallationCount = 0;
    public static $tmpFile = '';

    /**
     * @before
     */
    protected function prepareFixturesBefore()
    {
        self::$translationExportCount = 0;
        self::$translationPluginInstallationCount = 0;

        $process = new Process('zip -r crowdin.zip ' . '*', __DIR__ . '/_backup/crowdin');
        $process->run();

        $process = new Process('mv ' . __DIR__ . '/_backup/crowdin/crowdin.zip ' . __DIR__ . '/_backup/crowdin.zip');
        $process->run();

        $this->assertFileExists(__DIR__ . '/_backup/crowdin.zip', 'Could not create crowdin.zip');

        $process = new Process('cp -r '. __DIR__ . '/_backup ' . __DIR__ . '/_fixtures');
        $process->run();

        $this->assertFileExists(__DIR__ . '/_fixtures/crowdin.zip', 'Could not extract fixtures.');
        $this->assertFileExists(__DIR__ . '/_fixtures/shopware/shopware.php', 'Could not extract fixtures.');


        $container = self::getApplication()->getContainer();
        $container->set('shopware_translation_crowdin_client', new CrowdinApiMock());
        $container->set('install_service', new InstallMockCounter());
        $container->set('plugin_provider', new PluginProviderMock());

        LanguageSettings::$languageCodeMap = [
            'en' => 'en_GB',
            'de' => 'de_DE',
            'fr' => 'fr_FR'
        ];

        LanguageSettings::$languageTargetMap = [
            'en_GB' => 'core',
            'de_DE' => 'core',
            'fr_FR' => 'SwagFrance'
        ];

        LanguageSettings::$premiumPluginList = [
            'SwagTestPremium',
            'SwagNewPluginSystem'
        ];
    }

    /**
     * @after
     */
    protected function removeFilesAfter()
    {
        unlink(self::$tmpFile);
        unlink(__DIR__ . '/_backup/crowdin.zip');

        //Remove _fixtures directory
        $process = new Process('rm -r ' . __DIR__ . '/_fixtures');
        $process->run();
    }

    public function test_it_should_export_snippets()
    {
        $path = __DIR__ . '/_fixtures/shopware';
        $consoleOutput = $this->runCommand("translation:to:shopware $path");

        $this->assertSame(1, self::$translationExportCount);
        $this->assertEquals('Exporting translations...', $consoleOutput[1]);
    }

    public function test_it_should_update_language_plugin_snippets_with_crowdin_snippets()
    {
        $path = __DIR__ . '/_fixtures/shopware';
        $consoleOutput = $this->runCommand("translation:to:shopware $path");

        $this->assertStringStartsWith('Downloading translations from project', $consoleOutput[0]);
        $this->assertSame(1, self::$translationPluginInstallationCount);

        $iniContentFrance = parse_ini_file(__DIR__ . '/_fixtures/shopware/engine/Shopware/Plugins/Local/Core/SwagFrance/snippets/frontend/test/main.ini', true);
        $this->assertEquals([
            'fr_FR' => [
                'test' => 'Plugin de traduction de test crowdin',
                'test_new' => 'New snippet translated into France by crowdin'
            ]
        ], $iniContentFrance);
    }

    public function test_it_should_update_snippets_in_plugins_based_on_the_legacy_plugin_system()
    {
        $path = __DIR__ . '/_fixtures/shopware';
        $consoleOutput = $this->runCommand("translation:to:shopware $path");

        $this->assertSame(1, self::$translationPluginInstallationCount);
        $this->assertEquals('Updating SwagTestPremium...', $consoleOutput[17]);

        $iniContentPlugin = parse_ini_file(__DIR__ . '/_fixtures/shopware/engine/Shopware/Plugins/Local/Frontend/SwagTestPremium/Snippets/frontend/test/test.ini', true);
        $this->assertEquals([
            'en_GB' => [
                'test_plugin' => 'test translation plugin'
            ],
            'de_DE' => [
                'test_plugin' => 'Test Übersetzung Plugin crowdin'
            ]
        ], $iniContentPlugin);
    }

    public function test_it_should_update_core_snippets()
    {
        $path = __DIR__ . '/_fixtures/shopware';
        $consoleOutput = $this->runCommand("translation:to:shopware $path");

        $this->assertStringStartsWith('Downloading translations from project', $consoleOutput[0]);
        $this->assertEquals('Unpacking 47 translation files...', $consoleOutput[5]);
        $this->assertStringStartsWith('Reading core snippets from', $consoleOutput[7]);
        $this->assertSame(1, self::$translationPluginInstallationCount);

        $iniContent = parse_ini_file(__DIR__ . '/_fixtures/shopware/snippets/frontend/test/main.ini', true);
        $this->assertEquals([
            'en_GB' => [
                'test' => 'Test translation crowdin'
            ],
            'de_DE' => [
                'test' => 'Test Übersetzung Crowdin'
            ]
        ], $iniContent);
    }

    public function test_it_should_not_have_an_empty_line_on_end()
    {
        $path = __DIR__ . '/_fixtures/shopware';
        $this->runCommand("translation:to:shopware $path");

        $iniContent = file(__DIR__ . '/_fixtures/shopware/engine/Shopware/Plugins/Local/Core/SwagFrance/snippets/frontend/test/main.ini');
        $originalIniContent = file(__DIR__ . '/_backup/shopware/engine/Shopware/Plugins/Local/Core/SwagFrance/snippets/frontend/test/main.ini');

        $this->assertNotEmpty(trim(end($iniContent)));
        $this->assertNotEmpty(trim(end($originalIniContent)));
    }

    public function test_it_should_read_new_plugin_system_snippets()
    {
        $path = __DIR__ . '/_fixtures/shopware';

        $this->runCommand("translation:to:shopware $path");

        $iniContent = parse_ini_file(__DIR__ . '/_fixtures/shopware/custom/plugins/SwagNewPluginSystem/Resources/snippets/main.ini', true);
        $this->assertEquals([
            'en_GB' => [
                'test' => 'Test translation new plugin system crowdin'
            ],
            'de_DE' => [
                'test' => 'Test Übersetzung new plugin system crowdin'
            ]
        ], $iniContent);
    }
}

class CrowdinApiMock extends CrowdinClient
{
    public function __construct()
    {
    }

    public function export()
    {
        TranslationDownloadTest::$translationExportCount++;
        return new Response(
            200,
            [],
            '{ "success": { "status": true } }'
        );
    }

    public function download($destination, $locale = 'all')
    {
        TranslationDownloadTest::$tmpFile = $destination;
        copy(__DIR__ . '/_fixtures/crowdin.zip', $destination);
    }
}

class InstallMockCounter extends Install
{
    public function __construct()
    {
    }

    public function install(Plugin $plugin, $shopwarePath, $inputActivate = false, $branch = 'master', $useHttp = false)
    {
        TranslationDownloadTest::$translationPluginInstallationCount++;
    }
}

class PluginProviderMock extends PluginProvider
{
    public function __construct()
    {
    }

    public function getPluginByName($name, $exact = false)
    {
        $plugin = new Plugin([
            'name' => 'SwagFrance',
            'cloneUrlHttp' => 'http://example.org',
            'cloneUrlSsh' => 'https://example.org',
            'module' => 'Core',
            'repository' => 'i18n',
            'repoType' => 'Stash'
        ]);

        return [ 'SwagFrance' => $plugin ];
    }
}