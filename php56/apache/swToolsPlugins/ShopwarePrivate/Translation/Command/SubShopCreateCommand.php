<?php

namespace ShopwarePrivate\Translation\Command;

use ShopwareCli\Command\BaseCommand;
use ShopwareCli\Utilities;
use ShopwarePrivate\Translation\Services\PrepareShop\PdoConnectionFactory;
use ShopwarePrivate\Translation\Services\PrepareShop\SubShopCreator;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class SubShopCreateCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('translation:setup:subshops')
            ->setDescription('Create subshops for all languages')
            ->addArgument(
                'installDir',
                InputArgument::OPTIONAL,
                'Path to your shopware installation'
            )
            ->addArgument(
                'locale',
                InputArgument::OPTIONAL,
                'Locale for a subshop you want to create'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Utilities $utilities */
        $utilities = $this->container->get('utilities');

        $connectionFactory = new PdoConnectionFactory();
        $shopwarePath = $utilities->getValidShopwarePath($input->getArgument('installDir'));

        $connection = $connectionFactory->connect($shopwarePath);
        $subShopCreator = new SubShopCreator($connection);

        $locales = LanguageSettings::$languageTargetMap;
        if (
            $input->getArgument('locale')
            && array_key_exists($input->getArgument('locale'), $locales)
        ) {
            $locales = [
                $input->getArgument('locale') => $locales[$input->getArgument('locale')]
            ];
        } 

        $subShopCreator->create($locales);

        $symfonyStyle = new SymfonyStyle($input, $output);
        foreach ($locales as $code => $plugin) {
            $symfonyStyle->note(sprintf('Language subshop for %s was created successfully.', $code));
        }
        $symfonyStyle->success('All subshops were created successfully.');
    }
}