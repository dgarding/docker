<?php


namespace ShopwarePrivate\Translation\Command;


use ShopwareCli\Command\BaseCommand;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class MappingCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('translation:mapping')
            ->setDescription('Displays a table of all language codes with mappings');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $rows = [];
        foreach (LanguageSettings::$languageCodeMap as $crowdinCode => $shopwareCode) {
            $rows[$shopwareCode] = [
                'shopware' => $shopwareCode,
                'crowdin' => $crowdinCode
            ];
        }

        foreach (LanguageSettings::$languageTargetMap as $shopwareCode => $context) {
            $rows[$shopwareCode]['context'] = $context;
        }

        $table = new Table($output);
        $table
            ->setHeaders(['shopware', 'crowdin', 'target'])
            ->setRows($rows)
            ->render();
    }
}