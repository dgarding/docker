<?php

namespace ShopwarePrivate\Translation\Command;

use JiraRestApi\Configuration\ArrayConfiguration;
use JiraRestApi\Issue\IssueField;
use JiraRestApi\Issue\IssueService;
use ShopwareCli\Command\BaseCommand;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CreateTicketCommand extends BaseCommand
{
    const PLUGIN_FIELD = 'customfield_11200';
    const SCHEDULED_VALUE = 10708;
    const CHANGELOG_DE = 'customfield_11900';
    const CHANGELOG_EN = 'customfield_11901';
    const TEST_DOCUMENTATION = 'customfield_11500';

    protected function configure()
    {
        $this
            ->setName('translation:create:ticket')
            ->setDescription('Creates release tickets for each translation plugin including test documentation etc.')
            ->addArgument(
                'assignee',
                InputArgument::REQUIRED,
                'The assignee of the tickets, which is the jira user, i.e. s.baeumer'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $dateTime = new \DateTime();
        $config = $this->container->get('config');

        $pluginNames = array_merge(
            LanguageSettings::$languageTargetMap
        );

        $issueService = new IssueService(new ArrayConfiguration([
            'jiraHost' => 'https://jira.shopware.com',
            'jiraUser' => $config['Github']['jiraUser'],
            'jiraPassword' => $config['Github']['jiraPassword']
        ]));

        foreach ($pluginNames as $languageCode => $pluginName) {
            if ($pluginName === 'core') {
                $output->writeln('<info>Tickets for de_DE and en_GB must be created manually.</info>');
                continue;
            }

            $releaseTicket = $this->createReleaseTicket(
                $pluginName,
                $input->getArgument('assignee'),
                $dateTime,
                $languageCode
            );
            $response = $issueService->create($releaseTicket);
            $output->writeln(sprintf('<info>[%s] %s created.</info>', $response->key, $pluginName));
        }
    }


    /**
     * @param $pluginName
     * @param string $assginee
     * @param \DateTime $dateTime
     * @param string $languageCode
     * @return IssueField
     */
    private function createReleaseTicket($pluginName, $assginee, \DateTime $dateTime, $languageCode)
    {
        $issueField = new IssueField();
        $issueField
            ->setProjectKey('PT')
            ->setSummary(sprintf('Release %s (%s)', $pluginName, $languageCode))
            ->setAssigneeName($assginee)
            ->setDescription('Sync language plugin with crowdin and release the plugin.')
            ->setPriorityName('Major');

        $issueField->fixVersions = [['name' => 'Scheduled (Plugin)']];
        $issueField->addCustomField(self::PLUGIN_FIELD, ['id' => '11103']);
        $issueField->setIssueType('Story');

        $issueField->addCustomField(self::CHANGELOG_DE, $dateTime->format('Y-m-d') . ' Synchronisation von Übersetzungen mit Crowdin');
        $issueField->addCustomField(self::CHANGELOG_EN, $dateTime->format('Y-m-d') . ' Synchronized translations with Crowdin');
        $issueField->addCustomField(self::TEST_DOCUMENTATION, sprintf($this->getTestdocumentationTemplate(), $pluginName));

        return $issueField;
    }

    private function getTestdocumentationTemplate()
    {
        return <<<EOD
# Testdokumentation
 
## Setup
 
 - Shopware 5.2.x
 - Sprachplugin %s
  
**Plugins:**
 
- SwagAboCommerce
- SwagAdvancedCart
- SwagBonusSystem
- SwagBundle
- SwagBusinessEssentials
- SwagCustomProducts
- SwagDigitalPublishing
- SwagEmotionAdvanced
- SwagFuzzy
- SwagImportExport
- SwagLicense
- SwagLiveShopping
- SwagNewsletter
- SwagProductAdvisor
- SwagPromotion
- SwagTicketSystem
- SwagDigitalPublishing
- SwagBackendOrder
 
## Testszenarien
 
**Zielverhalten: Alle Backend Module lassen sich öffnen**
 
----
**Zielverhalten: Die wichtigsten Funktionen im Frontend testen**
 
 - Account
 - Login
 - Checkout
 - Einkaufswelt
 - Artikeldetailseite
 - Listing
  
---
**Zielverhalten: Sprachwechsel funktionieren im Frontend & Backend**

---
**Zielverhalten: Die korrekte Flagge wird für den Shop angezeigt im Frontend angezeigt (Sprachwechsel)**
EOD;
    }
}