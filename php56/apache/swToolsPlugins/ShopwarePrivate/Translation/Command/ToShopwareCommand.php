<?php
namespace ShopwarePrivate\Translation\Command;

use ShopwarePrivate\Translation\Services\Data\SnippetBasket;
use ShopwarePrivate\Translation\Services\Data\SnippetFile;
use ShopwarePrivate\Translation\Services\Crowdin\CrowdinTranslationHandler;
use ShopwarePrivate\Translation\Services\Shopware\ShopwareSnippetHandler;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;
use ShopwareCli\Command\BaseCommand;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ToShopwareCommand extends BaseCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('translation:to:shopware')
            ->setDescription('Downloads ALL translations from Crowdin to your local Shopware installation.')
            ->addArgument(
                'installDir',
                InputArgument::OPTIONAL,
                'Your Shopware installation path.',
                ''
            )
            ->addArgument(
                'file',
                InputArgument::OPTIONAL,
                'Path to snippet archive'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $shopwarePath = $input->getArgument('installDir');
        $snippetArchivePath = $input->getArgument('file');

        $shopwarePath = $this->container->get('utilities')->getValidShopwarePath($shopwarePath);

        $output->writeln(
            sprintf(
                '<info>Downloading translations from project "%s" to "%s"...</info>',
                $this->container->get('config')->offsetGet('Crowdin')['projectId'],
                $shopwarePath
            )
        );

        if (!is_dir($shopwarePath.'/snippets')) {
            throw new \RuntimeException("'$shopwarePath.'/snippets'' is not a valid snippets path");
        }

        $this->downloadTranslations($output, $shopwarePath, $snippetArchivePath);
    }

    /**
     * @param OutputInterface $output
     * @param string $shopwarePath
     */
    private function downloadTranslations(OutputInterface $output, $shopwarePath, $snippetArchivePath = '')
    {
        $shopwarePath = realpath($shopwarePath);

        /** @var CrowdinTranslationHandler $crowdinTranslationHandler */
        $crowdinTranslationHandler = $this->container->get('shopware_translation_crowdin_translation_handler');
        if (!$snippetArchivePath) {
            $crowdinTranslationHandler->exportTranslations();
            $crowdinSnippetBasket = $crowdinTranslationHandler->downloadTranslations();
        } else {
            $crowdinSnippetBasket = $crowdinTranslationHandler->unzipContent($snippetArchivePath);
        }

        /** @var ShopwareSnippetHandler $shopwareSnippetHandler */
        $shopwareSnippetHandler = $this->container->get('shopware_translation_shopware_snippet_handler');

        /** @var SnippetBasket $shopwareSnippetBasket */
        $shopwareSnippetBasket = $shopwareSnippetHandler->readShopwareInstallationSnippets($shopwarePath);

        foreach (LanguageSettings::getTranslationPluginNames() as $target) {
            $translationPluginSnippetData = $shopwareSnippetHandler->getSnippetsForPlugin($shopwarePath, $target);

            if (empty($translationPluginSnippetData)) {
                continue;
            }

            $shopwareSnippetBasket->addTranslations($translationPluginSnippetData);
        }

        $mergedSnippetBasket = $this->mergeSnippetBasket($shopwareSnippetBasket, $crowdinSnippetBasket);

        $shopwareSnippetHandler->writeShopwareInstallationSnippets($shopwarePath, $mergedSnippetBasket);

        $output->writeln('');
    }

    /**
     * @param SnippetBasket $shopwareSnippetBasket
     * @param SnippetBasket $crowdinSnippetBasket
     * @return SnippetBasket
     */
    private function mergeSnippetBasket(SnippetBasket $shopwareSnippetBasket, SnippetBasket $crowdinSnippetBasket)
    {
        foreach ($shopwareSnippetBasket->getAll() as $shopwareSnippetFile) {
            $crowdinSnippetFile = $crowdinSnippetBasket->getByContextPath($shopwareSnippetFile->getContextPath());
            if (!$crowdinSnippetFile) {
                continue;
            }

            $shopwareSnippetBasket->update($this->mergeSnippetFile($shopwareSnippetFile, $crowdinSnippetFile));
        }

        return $shopwareSnippetBasket;
    }

    /**
     * @param SnippetFile $shopwareSnippetFile
     * @param SnippetFile $crowdinSnippetFile
     * @return SnippetFile
     */
    private function mergeSnippetFile(SnippetFile $shopwareSnippetFile, SnippetFile $crowdinSnippetFile)
    {
        $shopwareSnippetFileStructure = array_keys(
            $shopwareSnippetFile->generateSnippetFileStructure(LanguageSettings::getCoreLanguages())
        );

        foreach ($crowdinSnippetFile->data as $language => $crowdinSnippetData) {
            if (!array_key_exists($language, $shopwareSnippetFile->data)) {
                continue;
            }
            $shopwareSnippetLanguageData = $shopwareSnippetFile->data[$language];

            // No preexisting data in the Shopware data
            // Assign and move on
            if (!$shopwareSnippetLanguageData) {
                $shopwareSnippetFile->data[$language] = $crowdinSnippetData;
                continue;
            }

            // Translation plugin language
            // array_merge implements the necessary logic
            $coreLanguages = LanguageSettings::getCoreLanguages();
            if (!in_array($language, $coreLanguages)) {
                $shopwareSnippetFile->data[$language] = array_merge($shopwareSnippetLanguageData, $crowdinSnippetData);
                continue;
            }

            // Core language
            // We need custom logic here:
            // Keep only Shopware keys, but include Crowdin values
            foreach ($shopwareSnippetFileStructure as $key) {
                if (!array_key_exists($key, $crowdinSnippetData)) {
                    continue;
                }

                $shopwareSnippetLanguageData[$key] = $crowdinSnippetData[$key];
            }
            $shopwareSnippetFile->data[$language] = $shopwareSnippetLanguageData;
        }

        return $shopwareSnippetFile;
    }
}
