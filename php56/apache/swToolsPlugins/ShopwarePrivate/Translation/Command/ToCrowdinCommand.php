<?php

namespace ShopwarePrivate\Translation\Command;

use ShopwarePrivate\Translation\Services\Data\SnippetBasket;
use ShopwarePrivate\Translation\Services\Crowdin\CrowdinDirectoryHandler;
use ShopwarePrivate\Translation\Services\Crowdin\CrowdinFileHandler;
use ShopwarePrivate\Translation\Services\Crowdin\CrowdinProjectHandler;
use ShopwarePrivate\Translation\Services\Crowdin\CrowdinTranslationHandler;
use ShopwarePrivate\Translation\Services\Shopware\ShopwareSnippetHandler;
use ShopwarePrivate\Translation\Services\Utils\CrowdinProjectStructure;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;
use ShopwareCli\Command\BaseCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ToCrowdinCommand extends BaseCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('translation:to:crowdin')
            ->setDescription('Uploads ALL translations from your local Shopware installation to Crowdin.')
            ->addArgument(
                'installDir',
                null,
                InputArgument::OPTIONAL,
                'Your Shopware installation path.'
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $shopwarePath = $this->checkPath($input);

        $output->writeln(
            sprintf(
                '<info>Uploading translations from "%s" to project "%s"...</info>',
                $shopwarePath,
                $this->container->get('config')->offsetGet('Crowdin')['projectId']
            )
        );

        /** @var CrowdinTranslationHandler $crowdinTranslationHandler */
        $crowdinTranslationHandler = $this->container->get('shopware_translation_crowdin_translation_handler');

        if (!is_dir($shopwarePath)) {
            throw new \RuntimeException("'$shopwarePath' is not a valid Shopware path");
        }

        /** @var ShopwareSnippetHandler $shopwareSnippetHandler */
        $shopwareSnippetHandler = $this->container->get('shopware_translation_shopware_snippet_handler');

        /** @var SnippetBasket $snippetBasket */
        $snippetBasket = $shopwareSnippetHandler->readShopwareInstallationSnippets($shopwarePath);

        foreach (LanguageSettings::getTranslationPluginNames() as $pluginName) {
            $translationPluginSnippetData = $shopwareSnippetHandler->getSnippetsForPlugin($shopwarePath, $pluginName);

            if (empty($translationPluginSnippetData)) {
                continue;
            }

            $snippetBasket->addTranslations($translationPluginSnippetData);
        }

        $this->syncCrowdinStructure($snippetBasket);
        $crowdinTranslationHandler->uploadTranslations($snippetBasket);

        $output->writeln('Build project and export translations. Can be aborted in command line.');
        $client = $this->container->get('shopware_translation_crowdin_client');
        $client->export();
    }

    /**
     * Syncs Crowdin folder and file structure:
     *  - Deletes old files and folders
     *  - Uploads new files
     *  - Updates existing files
     *
     * @param SnippetBasket $snippetBasket
     */
    private function syncCrowdinStructure($snippetBasket)
    {
        /** @var CrowdinProjectHandler $crowdinProjectHandler */
        $crowdinProjectHandler = $this->container->get('shopware_translation_crowdin_project_handler');

        /** @var CrowdinFileHandler $crowdinFileHandler */
        $crowdinFileHandler = $this->container->get('shopware_translation_crowdin_file_handler');

        /** @var CrowdinDirectoryHandler $crowdinDirectoryHandler */
        $crowdinDirectoryHandler = $this->container->get('shopware_translation_crowdin_directory_handler');

        $zombieFiles = $crowdinProjectHandler->getZombieFiles($snippetBasket);

        if (!empty($zombieFiles)) {
            $crowdinFileHandler->deleteFiles($zombieFiles);
        }
        $zombiePaths = $crowdinProjectHandler->getZombiePaths();
        $crowdinDirectoryHandler->deleteDirectories($zombiePaths);

        /** @var CrowdinProjectStructure $crowdinProjectStructure */
        $crowdinProjectStructure = $crowdinProjectHandler->getProjectStructure();

        $shopwareProjectStructure = $snippetBasket->getContextPaths();

        $newSnippetNamespaces = $this->getNewFiles($shopwareProjectStructure, $crowdinProjectStructure);
        $updateSnippetNamespaces = $this->getUploadFiles($shopwareProjectStructure, $crowdinProjectStructure);

        if (!empty($newSnippetNamespaces)) {
            $crowdinDirectoryHandler->createNewDirectories($newSnippetNamespaces);
            $newSnippetsData = $snippetBasket->getByContextPaths($newSnippetNamespaces);
            $crowdinFileHandler->addNewTranslationFiles($newSnippetsData);
        }

        if (!empty($updateSnippetNamespaces)) {
            $updateSnippetsData = $snippetBasket->getByContextPaths($updateSnippetNamespaces);
            $crowdinFileHandler->updateTranslationFiles($updateSnippetsData);
        }
    }

    /**
     * @param string[] $shopwareProjectStructure
     * @param CrowdinProjectStructure $crowdinProjectStructure
     * @return array
     */
    private function getNewFiles($shopwareProjectStructure, $crowdinProjectStructure)
    {
        $serverFileList = $crowdinProjectStructure->getFileList();

        return array_diff($shopwareProjectStructure, $serverFileList);
    }

    /**
     * @param string[] $shopwareProjectStructure
     * @param CrowdinProjectStructure $crowdinProjectStructure
     * @return array
     */
    private function getUploadFiles($shopwareProjectStructure, $crowdinProjectStructure)
    {
        $serverFileList = $crowdinProjectStructure->getFileList();

        return array_intersect($shopwareProjectStructure, $serverFileList);
    }

    /**
     * @param  InputInterface $input
     * @return string
     */
    private function checkPath(InputInterface $input)
    {
        $shopwarePath = rtrim($input->getArgument('installDir'), '/');

        $shopwarePath = $this->container->get('utilities')->getValidShopwarePath($shopwarePath);

        $input->getArgument('installDir');

        return realpath($shopwarePath);
    }
}
