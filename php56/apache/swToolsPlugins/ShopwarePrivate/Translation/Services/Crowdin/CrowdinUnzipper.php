<?php

namespace ShopwarePrivate\Translation\Services\Crowdin;

use ShopwareCli\Services\IoService;
use ShopwarePrivate\Translation\Services\Data\SnippetBasket;
use ShopwarePrivate\Translation\Services\Data\SnippetFile;
use ShopwareCli\Services\ZipUtil\Zip;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;

class CrowdinUnzipper
{
    /**
     * @var IoService
     */
    private $output;

    /**
     * @param IoService $output
     */
    public function __construct(IoService $output)
    {
        $this->output = $output;
    }

    /**
     * Unpacks downloaded zip content into SnippetFile structs.
     *
     * @param string $zipFilePath
     * @return SnippetBasket
     */
    public function unzipContent($zipFilePath)
    {
        $source = new Zip($zipFilePath);

        $this->output->writeln('<info>Unpacking '.$source->count().' translation files...</info>');

        $snippetBasket = new SnippetBasket();

        foreach ($source as $file) {

            $filename = $file->getName();

            if (!$file->isFile() || substr($file->getName(), -4) !== '.ini') {
                continue;
            }

            $fileLanguage = substr($filename, 0, strpos($filename, '/'));
            $filePath = substr($filename, strpos($filename, '/') + 1);
            try {
                $fileLanguage = LanguageSettings::convertCrowdinToShopware($fileLanguage);
                $context = $this->buildContext($filePath);
                $namespace = $this->buildNamespace($filePath);
            } catch (\RuntimeException $re) {
                try {
                    $fileLanguage = substr($zipFilePath, 0, strpos($zipFilePath, '_'));
                    $fileLanguage = LanguageSettings::convertCrowdinToShopware($fileLanguage);
                    $context = $this->buildContext($filename);
                    $namespace = substr($filePath, 0, -4);
                } catch (\RuntimeException $exception) {
//                    $this->output->writeln("<warning>Could not find language $fileLanguage</warning>");
                    continue;
                }
            }

            $fileContents = $file->getContents();
            $fixedFileContents = preg_replace('/\n\"/', '"', $fileContents);
            $sectionContent = @parse_ini_string($fixedFileContents, true, INI_SCANNER_RAW);
            if ($sectionContent === false) {
                $e = error_get_last();
                throw new \RuntimeException('Syntax error in "'.$filename.'": '.$e['message']);
            }

            if (empty($sectionContent)) {
                continue;
            }

            $fileName = strpos($filePath, '/') === false ? $filePath : substr($filePath, strrpos($filePath, '/') + 1);

            if ($snippetFile = $snippetBasket->getByContextPath($filePath)) {
                $snippetFile->data[$fileLanguage] = $sectionContent;
            } else {
                $snippetFile = new SnippetFile(
                    $fileName,
                    $filePath,
                    $namespace,
                    [$fileLanguage => $sectionContent],
                    $context
                );

                $snippetBasket->add($snippetFile);
            }
        }

        $this->output->writeln('<info>Translation files unpacked</info>');

        return $snippetBasket;
    }

    /**
     * @param $filePath
     * @return bool|string
     */
    private function buildContext($filePath)
    {
        if (strpos($filePath, '/') === false) {
            $context = $filePath;
        } else {
            $context = substr($filePath, 0, strpos($filePath, '/'));
        }
        return $context;
    }

    /**
     * @param $filePath
     * @return bool|string
     */
    private function buildNamespace($filePath)
    {
        $namespace = substr($filePath, strpos($filePath, '/') + 1, -4);
        return $namespace;
    }
}