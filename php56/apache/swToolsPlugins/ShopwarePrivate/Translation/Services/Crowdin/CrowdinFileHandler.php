<?php

namespace ShopwarePrivate\Translation\Services\Crowdin;

use Closure;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Piwik\Ini\IniWriter;
use Psr\Http\Message\ResponseInterface;
use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;
use ShopwarePrivate\Translation\Services\CrowdinApi\RequestGenerator\AddFilesGenerator;
use ShopwarePrivate\Translation\Services\CrowdinApi\RequestGenerator\DeleteFilesGenerator;
use ShopwarePrivate\Translation\Services\CrowdinApi\RequestGenerator\UpdateFilesGenerator;
use ShopwarePrivate\Translation\Services\CrowdinApi\RequestGenerator\RequestGeneratorInterface;
use ShopwarePrivate\Translation\Services\Data\SnippetFile;
use ShopwareCli\Config;
use ShopwareCli\Services\IoService;
use Symfony\Component\Console\Helper\ProgressBar;

class CrowdinFileHandler
{
    /**
     * @var array Crowdin endpoint config
     */
    private $config;

    /**
     * @var IoService
     */
    private $output;

    /**
     * @var ProgressBar
     */
    private $progressBar;

    /**
     * @var IniWriter
     */
    private $iniWriter;

    /**
     * @var CrowdinClient
     */
    private $crowdinClient;

    /**
     * @param Config $config
     * @param IoService $output
     * @param CrowdinClient $crowdinClient
     */
    public function __construct(Config $config, IoService $output, CrowdinClient $crowdinClient)
    {
        $this->config = $config;
        $this->output = $output;
        $this->iniWriter = new IniWriter();
        $this->crowdinClient = $crowdinClient;
    }

    /**
     * Delete files from server
     *
     * @param string[] $filePaths
     */
    public function deleteFiles($filePaths)
    {
        if (empty($filePaths)) {
            $this->output->writeln('<comment>Empty array provided for delete</comment>');

            return;
        }

        $this->output->writeln('<info>Deleting files...</info>');
        $this->progressBar = $this->output->createProgressBar(count($filePaths));
        $this->progressBar->start();

        $this->crowdinClient->uploadTranslation(
            $this->createDeleteFileGenerator($filePaths),
            $this->createFulfilledDeleteFileCallback(),
            $this->createRejectedDeleteFileCallback()
        );

        $this->progressBar->finish();
        $this->output->writeln('');
        $this->output->writeln('<info>Files deleted</info>');
    }

    /**
     * Handles success for delete file request
     *
     * @return Closure
     */
    private function createFulfilledDeleteFileCallback()
    {
        return function (ResponseInterface $response, $index) {
            if ($this->output->isVerbose()) {
                $this->output->writeln('');
                $this->output->writeln(
                    '<info>File deleted successfully</info>'
                );
            }
            $this->progressBar->advance();
        };
    }

    /**
     * @return Closure
     */
    private function createRejectedDeleteFileCallback()
    {
        return function ($reason, $index) {
            $responseContent = json_decode($reason->getResponse()->getBody()->getContents());
            throw new \RuntimeException(
                'Could not delete file: Error code '.$responseContent->error->code.' - '.$responseContent->error->message
            );
        };
    }

    /**
     * Yields Request objects to upload new files to Crowdin
     *
     * @param array $filePaths
     * @return DeleteFilesGenerator
     */
    private function createDeleteFileGenerator(array $filePaths)
    {
        return new DeleteFilesGenerator(
            $this->config->offsetGet('Crowdin'),
            $filePaths
        );
    }

    /**
     * Uploads new translation skeleton files to Crowdin
     * Files that already exist are ignored (marked for update)
     *
     * @param SnippetFile[] $snippetFileData
     * @return \ShopwarePrivate\Translation\Services\Data\SnippetFile[]
     */
    public function addNewTranslationFiles($snippetFileData)
    {
        if (empty($snippetFileData)) {
            $this->output->writeln('<comment>Empty array provided for insert</comment>');

            return;
        }

        $this->output->writeln('<info>Uploading new translation files...</info>');
        $this->progressBar = $this->output->createProgressBar(count($snippetFileData));
        $this->progressBar->start();

        $this->crowdinClient->uploadTranslation(
            $this->createAddFilesGenerator($snippetFileData),
            $this->createHandleFulfilledAddFileCallback(),
            $this->createRejectedAddFileRequestCallback()
        );

        $this->progressBar->finish();
        $this->output->writeln('');
        $this->output->writeln('<info>New translation files uploaded</info>');
    }

    private function createHandleFulfilledAddFileCallback()
    {
        return function (ResponseInterface $response, $index) {
            if ($this->output->isVerbose()) {
                $this->output->writeln('');
                $this->output->writeln(
                    '<info>File uploaded successfully</info>'
                );
            }
            $this->progressBar->advance();
        };
    }

    /**
     * @return Closure
     */
    private function createRejectedAddFileRequestCallback()
    {
        return function ($reason, $index) {
            if ($reason instanceof ClientException) {
                $responseContent = json_decode($reason->getResponse()->getBody()->getContents());

                if ($responseContent->error->code == 5) {
                    // File already exists, queue for update
                    if ($this->output->isVerbose()) {
                        $this->output->writeln('');
                        $this->output->writeln(
                            '<info>File already exists on server, scheduling for update...</info>'
                        );
                    }
                } else {
                    throw new \RuntimeException(
                        'Could not upload snippet files: Error code '.$responseContent->error->code.' - '.$responseContent->error->message
                    );
                }
                $this->progressBar->advance();
                return;
            }

            if ($reason instanceof ServerException) {
                /** @var ResponseInterface $response */
                $response = $reason->getResponse();

                $this->output->writeln(
                    '<comment>Could not upload new snippet file due to server error: '.$response->getStatusCode().' - '.$response->getReasonPhrase().'</comment>'
                );
                return;
            }

            throw $reason;
        };
    }

    /**
     * Yields Request objects to upload new files to Crowdin
     *
     * @param SnippetFile[] $snippetFileData
     * @return RequestGeneratorInterface
     */
    private function createAddFilesGenerator(array $snippetFileData)
    {
        $addFilesGenerator = new AddFilesGenerator(
            $this->config->offsetGet('Crowdin'),
            $this->iniWriter,
            $snippetFileData
        );
        return $addFilesGenerator;
    }

    /**
     * Updates the translation skeleton files that already exist in Crowdin
     *
     * @param SnippetFile[] $snippetFileData
     */
    public function updateTranslationFiles($snippetFileData)
    {
        if (empty($snippetFileData)) {
            if ($this->output->isVerbose()) {
                $this->output->writeln('<comment>Empty array provided for update</comment>');
            }

            return;
        }
        $this->output->writeln('<info>Updating existing translation files...</info>');
        $this->progressBar = $this->output->createProgressBar(count($snippetFileData));
        $this->progressBar->start();

        $this->crowdinClient->uploadTranslation(
            $this->createUpdateFilesGenerator($snippetFileData),
            $this->createFulfilledUpdateCallback(),
            $this->createRejectedUpdateCallback()
        );

        $this->progressBar->finish();
        $this->output->writeln('');
        $this->output->writeln('<info>Existing translation files updated</info>');
    }

    /**
     * @return Closure
     */
    private function createFulfilledUpdateCallback()
    {
        return function (ResponseInterface $response, $index) {
            $responseContent = json_decode($response->getBody()->getContents());
            $files = get_object_vars($responseContent->files);

            if ($this->output->isVerbose()) {
                $this->output->writeln('');
                foreach ($files as $name => $result) {
                    $this->output->writeln(
                        '<info>File update: file '.$name.' was '.$result.'</info>'
                    );
                }
            }
            $this->progressBar->advance();
        };
    }

    /**
     * @return Closure
     */
    private function createRejectedUpdateCallback()
    {
        return function ($reason, $index) {
            if ($reason instanceof ClientException) {
                $responseContent = json_decode($reason->getResponse()->getBody()->getContents());

                if ($responseContent->error->code === 8) {
                    // File doesn't exist
                    if ($this->output->isVerbose()) {
                        $this->output->writeln('');
                        $this->output->writeln(
                            '<info>File doesn\'t exist</info>'
                        );
                    }

                } else {
                    throw new \RuntimeException(
                        'Could not upload snippet files: Error code '.$responseContent->error->code.' - '.$responseContent->error->message
                    );
                }
                $this->progressBar->advance();
            } elseif ($reason instanceof ServerException) {
                /** @var ResponseInterface $response */
                $response = $reason->getResponse();

                $this->output->writeln(
                    '<comment>Could not update snippet file due to server error: '.$response->getStatusCode().' - '.$response->getReasonPhrase().'</comment>'
                );
            } else {
                throw $reason;
            }
        };
    }

    /**
     * @param SnippetFile[] $snippetFileData
     * @return UpdateFilesGenerator
     */
    private function createUpdateFilesGenerator(array $snippetFileData)
    {
        return new UpdateFilesGenerator(
            $this->config->offsetGet('Crowdin'),
            $this->iniWriter,
            $snippetFileData
        );
    }
}
