<?php

namespace ShopwarePrivate\Translation\Services\Crowdin;

use GuzzleHttp\Exception\ClientException;
use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;
use ShopwarePrivate\Translation\Services\Data\SnippetBasket;
use ShopwarePrivate\Translation\Services\Utils\CrowdinProjectStructure;
use ShopwareCli\Services\IoService;

class CrowdinProjectHandler
{
    /**
     * @var CrowdinClient
     */
    private $crowdinClient;

    /**
     * @var IoService
     */
    private $output;

    /**
     * @param IoService $output
     * @param CrowdinClient $crowdinClient
     */
    public function __construct(IoService $output, CrowdinClient $crowdinClient)
    {
        $this->output = $output;
        $this->crowdinClient = $crowdinClient;
    }

    /**
     * Gets a list of Crowdin files that no longer exist on Shopware
     *
     * @param SnippetBasket $snippetBasket
     * @return array
     */
    public function getZombieFiles(SnippetBasket $snippetBasket)
    {
        $this->output->writeln('<info>Loading zombie files info...</info>');

        $shopwareStructure = $snippetBasket->getContextPaths();

        $projectStructure = $this->getProjectStructure()->getFileList();

        $zombieFiles = array_diff($projectStructure, $shopwareStructure);

        $this->output->writeln('<info>'.count($zombieFiles).' zombie files found</info>');

        return $zombieFiles;
    }

    /**
     * Gets a list of Crowdin directories that no longer have matching Shopware namespaces
     *
     * @return array
     */
    public function getZombiePaths()
    {
        $this->output->writeln('<info>Loading zombie paths info...</info>');

        $zombiePaths = $this->getProjectStructure()->getEmptyFolderList();

        $this->output->writeln('<info>'.count($zombiePaths).' zombie paths found</info>');

        return $zombiePaths;
    }

    /**
     * Returns the list of files available on the server
     *
     * @return CrowdinProjectStructure
     */
    public function getProjectStructure()
    {
        try {
            $response = $this->crowdinClient->info();

            $responseContent = json_decode($response->getBody()->getContents());

            return new CrowdinProjectStructure($responseContent->files);
        } catch (ClientException $e) {
            $responseContent = json_decode($e->getResponse()->getBody()->getContents());
            $errorCode = $responseContent->error->code;
            $errorMessage = $responseContent->error->message;
            throw new \RuntimeException(
                'Could not load project info: Error code '.$errorCode.' - '.$errorMessage
            );
        }
    }
}
