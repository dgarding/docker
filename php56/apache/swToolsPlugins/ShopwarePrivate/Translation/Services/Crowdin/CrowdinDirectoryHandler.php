<?php

namespace ShopwarePrivate\Translation\Services\Crowdin;

use GuzzleHttp\Exception\ClientException;
use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;
use ShopwarePrivate\Translation\Services\Data\SnippetFile;
use ShopwareCli\Services\IoService;

class CrowdinDirectoryHandler
{
    /**
     * @var CrowdinClient
     */
    private $apiClient;

    /**
     * @var IoService
     */
    private $output;

    /**
     * @param IoService $output
     * @param CrowdinClient $apiClient
     */
    public function __construct(IoService $output, CrowdinClient $apiClient)
    {
        $this->output = $output;
        $this->apiClient = $apiClient;
    }

    /**
     * Deletes folders in Crowdin folders
     *
     * @param array $zombieDirPaths
     */
    public function deleteDirectories($zombieDirPaths)
    {
        if (empty($zombieDirPaths)) {
            return;
        }

        $this->output->writeln('<info>Deleting Crowdin folders...</info>');
        $progress = $this->output->createProgressBar(count($zombieDirPaths));
        $progress->start();

        foreach ($zombieDirPaths as $zombieDirPath) {
            if ($this->output->isVerbose()) {
                $this->output->writeln(' <info>Deleting "'.$zombieDirPath.'"</info>');
            }

            $this->deleteDirectory($zombieDirPath);
            $progress->advance();
        }
        $progress->finish();
        $this->output->writeln('');
        $this->output->writeln('<info>Crowdin folders deleted</info>');
    }

    /**
     * Deletes a folder path on Crowdin
     *
     * @param string $folderPath
     */
    private function deleteDirectory($folderPath)
    {
        try {
            $this->apiClient->deleteDirectory($folderPath);
        } catch (ClientException $e) {
            $responseContent = json_decode($e->getResponse()->getBody()->getContents());
            $errorCode = $responseContent->error->code;
            $errorMessage = $responseContent->error->message;
            throw new \RuntimeException(
                'Could not create directory "'.$folderPath.'"": Error code '.$errorCode.' - '.$errorMessage
            );
        }
    }

    /**
     * Maps loaded snippet namespaces to Crowdin folders
     *
     * @param string[] $snippetNamespaces
     */
    public function createNewDirectories($snippetNamespaces)
    {
        $folderPaths = $this->explodeNamespaces($snippetNamespaces);

        if (empty($snippetNamespaces)) {
            return;
        }

        $this->output->writeln('<info>Creating Crowdin directories...</info>');
        $progress = $this->output->createProgressBar(count($folderPaths));
        $progress->start();

        foreach ($folderPaths as $folderPath) {
            if ($this->output->isVerbose()) {
                $this->output->writeln(' <info>Creating folder for namespace '.$folderPath.'</info>');
            }

            $this->apiClient->addDirectory($folderPath);

            $progress->advance();
        }
        $progress->finish();
        $this->output->writeln('');
        $this->output->writeln('<info>Crowdin folders created</info>');
    }

    /**
     * Extracts all directory names from the snippet data
     *
     * @param string[] $namespaces
     * @return array
     */
    private function explodeNamespaces($namespaces)
    {
        $folderPaths = [];

        /** @var SnippetFile $snippetFile */
        foreach ($namespaces as $namespace) {
            $lastPos = strrpos($namespace, '/');
            if ($lastPos === false) {
                continue;
            }
            $namespace = substr($namespace, 0, $lastPos);
            $folderPaths[] = $namespace;
            while (strpos($namespace, '/') !== false) {
                $namespace = substr($namespace, 0, strrpos($namespace, '/'));
                $folderPaths[] = $namespace;
            }
        }

        return array_unique(array_reverse($folderPaths));
    }
}
