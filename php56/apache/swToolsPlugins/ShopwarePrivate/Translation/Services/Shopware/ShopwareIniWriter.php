<?php

namespace ShopwarePrivate\Translation\Services\Shopware;

use Piwik\Ini\IniWriter;

class ShopwareIniWriter
{
    /**
     * @var IniWriter
     */
    private $iniWriter;

    /**
     * @param IniWriter $iniWriter
     */
    public function __construct(IniWriter $iniWriter)
    {
        $this->iniWriter = $iniWriter;
    }

    /**
     * @param string $snippetFolderPath
     * @param array $snippetData
     * @param bool $createNewFiles
     */
    public function writeSnippets($snippetFolderPath, $snippetData, $createNewFiles = false)
    {
        foreach ($snippetData as $path => $data) {
            $snippetPath = $snippetFolderPath . '/' . $path;
            if (!file_exists($snippetPath) && !$createNewFiles) {
                throw new \RuntimeException('Trying to write to non-existing "' . $snippetPath . '" file');
            }

            if (!file_exists($snippetPath)) {
                @mkdir(dirname($snippetPath), 0777, true);
            }

            $iniContent = $this->iniWriter->writeToString($data);
            //Remove empty line on end because the piwik ini writer adds always an empty line.
            //This causes very big code reviews.
            file_put_contents($snippetPath, rtrim($iniContent));
        }
    }
}