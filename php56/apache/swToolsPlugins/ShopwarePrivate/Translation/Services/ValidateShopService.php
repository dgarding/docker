<?php

namespace ShopwarePrivate\Translation\Services;

use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class ValidateShopService
{
    /**
     * @param string $path
     * @param string $plugin
     */
    public function validate($path, $plugin)
    {
        $finder = new Finder();
        $finder
            ->in($path)
            ->name($plugin);

        /** @var SplFileInfo $directory */
        foreach ($finder->directories() as $directory) {
            if (basename($directory->getRealPath()) === $plugin) {
                return;
            }
        }

        throw new \RuntimeException('Cannot find plugin ' . $plugin . '. To fix this problem run translation:prepare:shop in your shopware installation.');
    }
}