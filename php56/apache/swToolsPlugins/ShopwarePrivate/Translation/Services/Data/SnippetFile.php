<?php

namespace ShopwarePrivate\Translation\Services\Data;

use ShopwareCli\Struct;

/**
 * Shopware snippet file
 *
 * Class SnippetFile
 * @package Shopware\Translation\Data
 */
class SnippetFile extends Struct
{
    public $fileName;
    public $filePath;
    public $namespace;
    public $data;
    public $context;

    public function __construct($fileName = null, $filePath = null, $namespace = null, $data = null, $context = null)
    {
        $this->fileName = $fileName;
        $this->filePath = $filePath;
        $this->namespace = $namespace;
        $this->data = $data;
        $this->context = $context;
    }

    public function getExistingKeys()
    {
        $keys = [];
        foreach ($this->data as $snippetValues) {
            $keys = array_merge($keys, array_keys($snippetValues));
        }

        return $keys;
    }

    public function getContextPath()
    {
        return $this->context.'/'.$this->namespace.'.ini';
    }

    /**
     * Converts the snippet data into the Crowdin structure format
     *
     * @param $structureLanguages
     * @return array
     */
    public function generateSnippetFileStructure($structureLanguages)
    {
        $valueKey = 'en_GB';

        if (!array_key_exists($valueKey, $this->data)) {
            throw new \RuntimeException('Missing key when merging data for namespace');
        }

        $mergedData = [];

        foreach ($this->data as $key => $snippets) {
            if (!in_array($key, $structureLanguages)) {
                continue;
            }
            foreach ($snippets as $name => $snippet) {
                if ($key == $valueKey) {
                    $mergedData[$name] = $snippet;
                } elseif (!array_key_exists($name, $mergedData) || (empty($mergedData[$name]) && !empty($snippet))) {
                    $mergedData[$name] = $name;
                }
            }
        }

        return $mergedData;
    }
}
