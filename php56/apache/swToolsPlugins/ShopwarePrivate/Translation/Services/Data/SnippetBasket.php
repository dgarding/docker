<?php

namespace ShopwarePrivate\Translation\Services\Data;

class SnippetBasket
{
    /**
     * @var SnippetFile[]
     */
    private $snippetBucket;

    /**
     * SnippetBasket constructor.
     */
    public function __construct()
    {
        $this->snippetBucket = [];
    }

    /**
     * Update existing snippet file
     *
     * @param SnippetFile $snippetFile
     */
    public function update(SnippetFile $snippetFile)
    {
        if (!array_key_exists($snippetFile->getContextPath(), $this->snippetBucket)) {
            throw new \RuntimeException('Trying to update non-existing context path '.$snippetFile->getContextPath());
        }

        $this->snippetBucket[$snippetFile->getContextPath()] = $snippetFile;
    }

    /**
     * Merges two snippet baskets into one
     *
     * @param SnippetBasket $snippetBasket
     */
    public function merge(SnippetBasket $snippetBasket)
    {
        $this->add($snippetBasket->getAll());
    }

    /**
     * Adds snippets to this basket
     *
     * @param $data
     */
    public function add($data)
    {
        if ($data instanceof SnippetFile) {
            $data = [$data];
        }

        if (!reset($data) instanceof SnippetFile) {
            throw new \RuntimeException('SnippetBasket::add() requires a SnippetFile instance or a SnippetFile array');
        }

        /** @var SnippetFile $snippetFile */
        foreach ($data as $snippetFile) {
            if (array_key_exists($snippetFile->getContextPath(), $this->snippetBucket)) {
                throw new \RuntimeException(
                    'Trying to add already existing context path in snippet basket: ' .
                    $snippetFile->getContextPath());
            }
            $this->snippetBucket[$snippetFile->getContextPath()] = $snippetFile;
        }
    }

    /**
     * Returns the full list of SnippetFile instances
     *
     * @return SnippetFile[]
     */
    public function getAll()
    {
        return array_values($this->snippetBucket);
    }

    /**
     * Returns an array of the languages defined in the current snippet bucket
     *
     * @return array
     */
    public function getLanguages()
    {
        $languages = [];

        /** @var SnippetFile $snippet */
        foreach ($this->snippetBucket as $snippetFile) {
            $languages = array_merge(
                $languages,
                array_keys($snippetFile->data)
            );
        }

        return array_unique($languages);
    }

    /**
     * Returns an array of the contexts defined in the current snippet bucket
     *
     * @return array
     */
    public function getContexts()
    {
        $contexts = [];

        /** @var SnippetFile $snippetFile */
        foreach ($this->snippetBucket as $snippetFile) {
            $contexts[$snippetFile->context] = $snippetFile->context;
        }

        return array_values($contexts);
    }

    /**
     * Returns all SnippetFile instances that belong to the given context
     *
     * @param $context
     * @return SnippetFile[]
     */
    public function getByContext($context)
    {
        return array_filter(
            $this->snippetBucket,
            function ($snippetFile) use ($context) {
                return $snippetFile->context == $context;
            }
        );
    }

    /**
     * Returns the list of existing context paths
     *
     * @return string[]
     */
    public function getContextPaths()
    {
        return array_keys($this->snippetBucket);
    }

    /**
     * Returns a SnippetFile by its context path, or null if none was found
     *
     * @param string $contextPath
     * @return SnippetFile|null
     */
    public function getByContextPath($contextPath)
    {
        return array_key_exists($contextPath, $this->snippetBucket) ? $this->snippetBucket[$contextPath] : null;
    }

    /**
     * Returns a SnippetFile list by its context paths, or an empty array if none were found
     *
     * @param string[] $contextPathList
     * @return SnippetFile[]
     */
    public function getByContextPaths($contextPathList)
    {
        return array_intersect_key($this->snippetBucket, array_flip($contextPathList));
    }

    /**
     * Adds translations from the given snippet basket into the snippets in the current basket
     *
     * @param SnippetBasket $translationSnippetBasket
     */
    public function addTranslations(SnippetBasket $translationSnippetBasket)
    {
        foreach ($translationSnippetBasket->getAll() as $translation) {
            foreach ($this->snippetBucket as &$snippet) {
                if ($translation->namespace != $snippet->namespace) {
                    continue;
                }

                foreach ($translation->data as $translationLanguageKey => $translationContent) {
                    if (array_key_exists($translationLanguageKey, $snippet->data)) {
                        throw new \RuntimeException('Language collision detected when merging translations');
                    }

                    $snippet->data[$translationLanguageKey] = $translationContent;
                }

            }
        }
    }
}