<?php

namespace ShopwarePrivate\Translation\Services\CrowdinApi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use GuzzleHttp\Pool;
use GuzzleHttp\Psr7\Uri;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;
use ShopwareCli\Config;
use ShopwarePrivate\Translation\Services\CrowdinApi\RequestGenerator\RequestGeneratorInterface;

class CrowdinClient
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var string
     */
    private $projectKey;

    /**
     * @var array
     */
    private $crowdinConfig;

    /**
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $crowdinConfig = $config->offsetGet('Crowdin');

        $this->crowdinConfig = $crowdinConfig;
        $this->projectKey = $crowdinConfig['projectKey'];
        $this->client = new Client([
            'base_uri' => $crowdinConfig['endpoint'] . $crowdinConfig['projectId'] . '/',
        ]);
    }

    /**
     * @return ResponseInterface
     */
    public function export()
    {
        try {
            return $this->client->get(
                'export',
                [
                    'query' => [
                        'key' => $this->projectKey,
                        'json' => true
                    ]
                ]
            );
        } catch (ClientException $e) {
            $responseContent = json_decode($e->getResponse()->getBody()->getContents());
            $errorCode = $responseContent->error->code;
            $errorMessage = $responseContent->error->message;
            throw new RuntimeException(
                'Could not export translations: Error code '.$errorCode.' - '.$errorMessage
            );
        }
    }

    /**
     * @param string $destination
     * @param string $locale
     * @return ResponseInterface
     */
    public function download($destination, $locale = 'all')
    {
        try {
            return $this->client->get(
                "download/$locale.zip",
                [
                    'query' => [
                        'key' => $this->projectKey,
                        'json' => true
                    ],
                    'save_to' => $destination
                ]
            );
        } catch (ClientException $e) {
            $responseContent = json_decode($e->getResponse()->getBody()->getContents());
            $errorCode = $responseContent->error->code;
            $errorMessage = $responseContent->error->message;
            if ($errorCode == 8) {
                throw new \RuntimeException('Could not download translation file.' . PHP_EOL . $e->getMessage());
            } else {
                throw new \RuntimeException(
                    'Could not download translations: Error code '.$errorCode.' - '.$errorMessage
                );
            }
        }
    }

    /**
     * @param string $path
     * @return ResponseInterface
     */
    public function deleteDirectory($path)
    {
        return $this->client->post(
            'delete-directory',
            [
                'query' => [
                    'key' => $this->projectKey,
                    'name' => $path,
                    'json' => true,
                ]
            ]
        );
    }

    /**
     * @param string $path
     * @return ResponseInterface
     */
    public function addDirectory($path)
    {
        try {
            return $this->client->post(
                'add-directory',
                [
                    'query' => [
                        'key' => $this->projectKey,
                        'name' => $path,
                        'json' => true,
                    ],
                ]
            );
        } catch (ServerException $exception) {
            //Ignore already existing directories
            $body = json_decode($exception->getResponse()->getBody(),true);
            if ($body['error']['code'] !== 50) {
                throw $exception;
            }
        }

    }

    /**
     * @return ResponseInterface
     */
    public function info()
    {
        return $this->client->post(
            new Uri('info'),
            [
                'query' => [
                    'key' => $this->projectKey,
                    'json' => true,
                ],
            ]
        );
    }

    /**
     * @param RequestGeneratorInterface $updateTranslationGenerator
     * @param callable $completeCallback
     * @param callable $errorCallback
     */
    public function uploadTranslation(
        RequestGeneratorInterface $updateTranslationGenerator,
        callable $completeCallback,
        callable $errorCallback
    ) {
        $pool = new Pool(
            $this->client,
            $updateTranslationGenerator->generate($this),
            [
                'concurrency' => $this->crowdinConfig['concurrentRequests'],
                'fulfilled' => $completeCallback,
                'rejected' => $errorCallback,
            ]
        );

        $pool->promise()->wait();
    }

    /**
     * @param string $method
     * @param string $url
     * @param array $options
     * @return RequestInterface
     */
    public function createRequest($method, $url = null, array $options = [])
    {
        return $this->client->createRequest(
            $method,
            $url,
            $options
        );
    }

    /**
     * @return array
     */
    public function getTranslationStatus()
    {
        $result = $this->client->post(
            'status',
            [
                'query' => [
                    'key' => $this->projectKey,
                    'json' => true
                ]
            ]
        );

        return json_decode($result->getBody()->getContents(), true);
    }
}