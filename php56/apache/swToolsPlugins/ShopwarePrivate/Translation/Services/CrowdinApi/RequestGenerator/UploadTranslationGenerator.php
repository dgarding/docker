<?php

namespace ShopwarePrivate\Translation\Services\CrowdinApi\RequestGenerator;

use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use Piwik\Ini\IniWriter;
use Psr\Http\Message\RequestInterface;
use ShopwareCli\Config;
use ShopwareCli\Services\IoService;
use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;
use ShopwarePrivate\Translation\Services\Data\SnippetBasket;
use ShopwarePrivate\Translation\Services\Data\SnippetFile;
use Symfony\Component\Console\Helper\ProgressBar;

class UploadTranslationGenerator implements RequestGeneratorInterface
{
    /**
     * @var SnippetBasket
     */
    private $snippetBasket;

    /**
     * @var string
     */
    private $sourceLanguage;

    /**
     * @var string
     */
    private $targetLanguage;

    /**
     * @var array
     */
    private $crowdinConfig;

    /**
     * @var IniWriter
     */
    private $iniWriter;

    /**
     * @var ProgressBar
     */
    private $progressBar;

    /**
     * @var IoService
     */
    private $ioService;

    /**
     * @param SnippetBasket $snippetBasket
     * @param string $sourceLanguage
     * @param string $targetLanguage
     * @param Config $config
     * @param IniWriter $iniWriter
     * @param ProgressBar $progressBar
     * @param IoService $ioService
     */
    public function __construct(
        SnippetBasket $snippetBasket,
        $sourceLanguage,
        $targetLanguage,
        Config $config,
        IniWriter $iniWriter,
        ProgressBar $progressBar,
        IoService $ioService
    ) {
        $this->snippetBasket = $snippetBasket;
        $this->sourceLanguage = $sourceLanguage;
        $this->targetLanguage = $targetLanguage;
        $this->crowdinConfig = $config['Crowdin'];
        $this->iniWriter = $iniWriter;
        $this->progressBar = $progressBar;
        $this->ioService = $ioService;
    }

    /**
     * @param CrowdinClient $crowdinClient
     * @return RequestInterface[]
     */
    public function generate(CrowdinClient $crowdinClient)
    {
        $requests = [];

        foreach ($this->snippetBasket->getAll() as $snippetFile) {
            /** @var SnippetFile $snippetFile */
            if (!array_key_exists($this->sourceLanguage, $snippetFile->data)) {
                $this->progressBar->advance();
                continue;
            }

            $snippetData = [
                $snippetFile->data[$this->sourceLanguage],
            ];

            if ($this->ioService->isVerbose()) {
                $this->ioService->writeln(
                    '<info>Uploading '.$snippetFile->namespace.'.ini</info>'
                );
            }

            $query = http_build_query(
                [
                    'key' => $this->crowdinConfig['projectKey'],
                    'json' => true,
                    'language' => $this->targetLanguage,
                    'auto_approve_imported' => 1,
                    'import_eq_suggestions' => 1,
                ]
            );
            $url = $this->crowdinConfig['endpoint'].$this->crowdinConfig['projectId'].'/upload-translation?'.$query;

            $fileStream = new MultipartStream([
                [
                    'name' => 'files['.$snippetFile->getContextPath().']',
                    'filename' => $snippetFile->getContextPath(),
                    'contents' => $this->iniWriter->writeToString($snippetData)
                ]
            ]);

            $requests[] = new Request(
                'POST',
                $url,
                [],
                $fileStream
            );
        }

        return $requests;
    }
}