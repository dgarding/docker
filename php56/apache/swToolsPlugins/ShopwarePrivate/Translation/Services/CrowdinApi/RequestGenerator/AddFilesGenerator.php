<?php

namespace ShopwarePrivate\Translation\Services\CrowdinApi\RequestGenerator;

use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use Piwik\Ini\IniWriter;
use Psr\Http\Message\RequestInterface;
use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;
use ShopwarePrivate\Translation\Services\Data\SnippetFile;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;

class AddFilesGenerator implements RequestGeneratorInterface
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var IniWriter
     */
    private $iniWriter;

    /**
     * @var array
     */
    private $snippetFileData;

    /**
     * @param array $config
     * @param IniWriter $iniWriter
     * @param SnippetFile[] $snippetFileData
     */
    public function __construct(array $config, IniWriter $iniWriter, array $snippetFileData)
    {
        $this->config = $config;
        $this->iniWriter = $iniWriter;
        $this->snippetFileData = $snippetFileData;
    }

    /**
     * Yields Request objects to upload new files to Crowdin
     *
     * @param CrowdinClient $crowdinClient
     * @return RequestInterface[]
     */
    public function generate(CrowdinClient $crowdinClient)
    {
        $requests = [];

        /** @var SnippetFile $snippetFile */
        foreach ($this->snippetFileData as $snippetFile) {
            if (empty($snippetFile->data)) {
                continue;
            }
            $snippetData = $snippetFile->generateSnippetFileStructure(LanguageSettings::getCoreLanguages());
            $query = http_build_query(
                [
                    'key' => $this->config['projectKey'],
                    'json' => true,
                    'type' => 'ini',
                ]
            );
            $url = $this->config['endpoint'].$this->config['projectId'].'/add-file?'.$query;

            $fileStream = new MultipartStream(
                [
                    [
                        'contents' => $this->iniWriter->writeToString([$snippetData]),
                        'name' => 'files['.$snippetFile->getContextPath().']',
                        'filename' => $snippetFile->getContextPath()
                    ]
                ]
            );

            $requests[] = new Request(
                'POST',
                $url,
                [],
                $fileStream
            );
        }

        return $requests;
    }

}