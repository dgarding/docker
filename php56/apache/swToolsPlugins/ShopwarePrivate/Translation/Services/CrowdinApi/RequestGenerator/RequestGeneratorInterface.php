<?php

namespace ShopwarePrivate\Translation\Services\CrowdinApi\RequestGenerator;

use Psr\Http\Message\RequestInterface;
use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;

interface RequestGeneratorInterface
{
    /**
     * @param CrowdinClient $crowdinClient
     * @return RequestInterface[]
     */
    public function generate(CrowdinClient $crowdinClient);
}