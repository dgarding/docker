<?php

namespace ShopwarePrivate\Translation\Services\SnippetReader;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use ShopwarePrivate\Translation\Services\Data\SnippetBasket;
use ShopwarePrivate\Translation\Services\Data\SnippetFile;

class SnippetReader
{
    /**
     * @param string $snippetPath
     * @param string $context - Name of a plugin (SwagExample, etc) or "core"
     * @return SnippetBasket
     */
    public function readSnippets($snippetPath, $context = null)
    {
        $snippetBasket = new SnippetBasket();

        $dirIterator = new RecursiveDirectoryIterator($snippetPath, RecursiveDirectoryIterator::SKIP_DOTS);
        $iterator = new RecursiveIteratorIterator(
            $dirIterator,
            RecursiveIteratorIterator::LEAVES_ONLY
        );

        /** @var $entry \SplFileInfo */
        foreach ($iterator as $entry) {
            if (!$entry->isFile() || substr($entry->getFilename(), -4) !== '.ini') {
                continue;
            }

            $snippetValues = @parse_ini_file($entry->getRealPath(), true, INI_SCANNER_RAW);
            if ($snippetValues === false) {
                $e = error_get_last();
                throw new \RuntimeException('Syntax error in "'.$entry->getRealPath().'": '.$e['message']);
            }
            if (empty($snippetValues)) {
                continue;
            }

            $snippetValues = $this->mergeDefaultGroups($snippetValues);

            $filePath = trim(str_replace($snippetPath, '', $entry->getRealPath()), '/');
            $namespace = substr($filePath, 0, strrpos($filePath, '.'));

            $snippetBasket->add(
                new SnippetFile(
                    $entry->getFilename(),
                    $entry->getRealPath(),
                    $namespace,
                    $snippetValues,
                    $context
                )
            );
        }

        return $snippetBasket;
    }

    /**
     * @param array $snippetData
     * @return array
     */
    private function mergeDefaultGroups($snippetData)
    {
        $baseData = [];

        foreach (array_keys($snippetData) as $key) {
            if ($key == 'default') {
                $baseData = array_key_exists('en_GB', $snippetData) ? $snippetData['en_GB'] : [];
                $snippetData['en_GB'] = array_merge($baseData, $snippetData['default']);
                unset($snippetData['default']);
            } elseif (strpos($key, 'default') !== false) {
                /**
                 * Snippets with a key which contains "default" + some other character will change their key. For example:
                 *  'default _' => '_'
                 * @see \ShopwarePrivate\Tests\Unit\Translation\Services\SnippetReader\SnippetReaderTest::test_it_should_merge_default_snippet_group_with_typo
                 */
                $mergedKey = trim(str_replace('default', '', $key), ' :');
                $snippetData[$mergedKey] = array_merge($baseData, $snippetData[$key]);
                unset($snippetData[$key]);
            }
        }

        return $snippetData;
    }
}