<?php

namespace ShopwarePrivate\Translation\Services\SnippetReader;

use DirectoryIterator;
use ShopwareCli\Services\IoService;
use ShopwarePrivate\Translation\Services\Data\SnippetBasket;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;

class ShopwareSnippetReader
{
    /**
     * @var SnippetReader
     */
    private $snippetReader;

    /**
     * @var IoService
     */
    private $ioService;

    /**
     * @param SnippetReader $snippetReader
     */
    public function __construct(SnippetReader $snippetReader, IoService $ioService)
    {
        $this->snippetReader = $snippetReader;
        $this->ioService = $ioService;
    }

    /**
     * @param string $shopwarePath
     * @return SnippetBasket
     */
    public function read($shopwarePath)
    {
        $snippetBasket = new SnippetBasket();

        $snippetPath = realpath($shopwarePath.'/snippets');
        $this->ioService->writeln('<info>Reading core snippets from "'.$snippetPath.'"</info>');

        $coreSnippetBasket = $this->snippetReader->readSnippets($snippetPath, 'core');

        $this->ioService->writeln(
            '<info>'.count($coreSnippetBasket->getAll()).' namespaces read from "'.$snippetPath.'"</info>'
        );
        $this->ioService->writeln('<info>Reading core plugins snippets from "'.$snippetPath.'"</info>');

        $corePluginsSnippetBasket = $this->readLegacyPluginSnippets($shopwarePath.'/engine/Shopware/Plugins/Default/');

        $this->ioService->writeln(
            '<info>'.count($corePluginsSnippetBasket->getAll()).' namespaces read from core plugins</info>'
        );
        $this->ioService->writeln('<info>Reading premium plugins snippets from "'.$snippetPath.'"</info>');

        $legacyPluginSnippets = $this->readLegacyPluginSnippets(
            $shopwarePath.'/engine/Shopware/Plugins/Local/',
            LanguageSettings::getPremiumPluginList()
        );

        $newPluginSystemSnippetBasket = $this->readNewPluginSystem(
            $shopwarePath.'/custom/plugins/',
            LanguageSettings::getPremiumPluginList()
        );

        $plugin = count($legacyPluginSnippets->getAll()) + count($newPluginSystemSnippetBasket->getAll());
        $this->ioService->writeln(
            '<info>'. $plugin .' namespaces read from premium plugins</info>'
        );

        $snippetBasket->merge($coreSnippetBasket);
        $snippetBasket->merge($corePluginsSnippetBasket);
        $snippetBasket->merge($legacyPluginSnippets);
        $snippetBasket->merge($newPluginSystemSnippetBasket);

        return $snippetBasket;
    }

    /**
     * @param string $snippetPath
     * @param array $whiteList
     * @return SnippetBasket
     */
    private function readLegacyPluginSnippets($snippetPath, $whiteList = [])
    {
        $snippetBasket = new SnippetBasket();

        foreach (['Frontend', 'Core', 'Backend'] as $module) {
            $iterator = new DirectoryIterator($snippetPath.$module);
            foreach ($iterator as $entry) {
                if (!$entry->isDir() || (!empty($whiteList) && !in_array($entry->getFilename(), $whiteList))) {
                    continue;
                }

                $pluginSnippetPath = realpath($entry->getRealPath()."/Snippets") ?: realpath(
                    $entry->getRealPath()."/snippets"
                );
                if (empty($pluginSnippetPath)) {
                    continue;
                }

                $context = $entry->getFilename();
                $snippetBasket->merge($this->snippetReader->readSnippets($pluginSnippetPath, $context));
            }
        }

        return $snippetBasket;
    }

    /**
     * @param string $snippetPath
     * @param array $whiteList
     * @return SnippetBasket
     */
    private function readNewPluginSystem($snippetPath, array $whiteList = [])
    {
        $snippetBasket = new SnippetBasket();

        $iterator = new DirectoryIterator($snippetPath);
        foreach ($iterator as $entry) {
            if (!$entry->isDir() || (!empty($whiteList) && !in_array($entry->getFilename(), $whiteList))) {
                continue;
            }

            $pluginSnippetPath = realpath($entry->getRealPath() . '/Resources/snippets');
            if (!$pluginSnippetPath) {
                continue;
            }

            $context = $entry->getFilename();
            $snippetBasket->merge($this->snippetReader->readSnippets($pluginSnippetPath, $context));
        }

        return $snippetBasket;
    }
}