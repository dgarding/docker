<?php

namespace ShopwarePrivate\Translation\Services\Utils;

class LanguageSettings
{
    /**
     * @var array
     */
    public static $languageCodeMap = [
        'en' => 'en_GB',
        'de' => 'de_DE',
        'fr' => 'fr_FR',
        'nl' => 'nl_NL',
        'es-ES' => 'es_ES',
        'bg' => 'bg_BG',
        'cs' => 'cs_CZ',
        'tr' => 'tr_TR',
        'sk' => 'sk_SK',
        'ru' => 'ru_RU',
        'it' => 'it_IT',
        'dei' => 'de_DEI',
        'fi' => 'fi_FI',
        'pt-PT' => 'pt_PT',
        'pl' => 'pl_PL',
        'hy-AM' => 'hy_AM',
    ];

    /**
     * @var array
     */
    public static $languageTargetMap = [
        'en_GB' => 'core',
        'de_DE' => 'core',
        'fr_FR' => 'SwagFrance',
        'nl_NL' => 'SwagNetherlands',
        'es_ES' => 'SwagSpain',
        'bg_BG' => 'SwagBulgaria',
        'cs_CZ' => 'SwagCzech',
        'tr_TR' => 'SwagTurkey',
        'sk_SK' => 'SwagSlovakia',
        'ru_RU' => 'SwagRussia',
        'it_IT' => 'SwagItaly',
        'de_DEI' => 'SwagInformalGerman',
        'fi_FI' => 'SwagFinland',
        'pt_PT' => 'SwagPortuguese',
        'pl_PL' => 'SwagPoland',
        'hy_AM' => 'SwagArmenia',
    ];

    /**
     * @var array
     */
    public static $premiumPluginList = [
        'SwagAboCommerce',
        'SwagAdvancedCart',
        'SwagBonusSystem',
        'SwagBundle',
        'SwagBusinessEssentials',
        'SwagCustomProducts',
        'SwagDigitalPublishing',
        'SwagEmotionAdvanced',
        'SwagFuzzy',
        'SwagImportExport',
        'SwagLicense',
        'SwagLiveShopping',
        'SwagNewsletter',
        'SwagProductAdvisor',
        'SwagPromotion',
        'SwagTicketSystem',
        'SwagDigitalPublishing',
        'SwagBackendOrder'
    ];

    /**
     * Returns a list of supported Shopware language codes
     *
     * @return string
     */
    public static function getSupportedLanguageCodes()
    {
        return array_values(self::$languageCodeMap);
    }

    /**
     * Returns a list of supported translation plugin names
     *
     * @return array
     */
    public static function getTranslationPluginNames()
    {
        return array_filter(
            array_values(self::$languageTargetMap),
            function ($elem) {
                return ($elem !== 'core');
            }
        );
    }

    /**
     * Converts Shopware language code into Crowdin matching value
     *
     * @param string $shopwareLanguageCode
     * @throws \RuntimeException
     * @return string
     */
    public static function convertShopwareToCrowdin($shopwareLanguageCode)
    {
        $workData = array_flip(self::$languageCodeMap);

        if (!array_key_exists($shopwareLanguageCode, $workData)) {
            throw new \RuntimeException('Language code "'.$shopwareLanguageCode.'" is not mapped');
        }

        return $workData[$shopwareLanguageCode];
    }

    /**
     * Converts Crowdin language code into Shopware matching value
     *
     * @param string $crowdinLanguageCode
     * @throws \RuntimeException
     * @return string
     */
    public static function convertCrowdinToShopware($crowdinLanguageCode)
    {
        $workData = self::$languageCodeMap;

        if (!array_key_exists($crowdinLanguageCode, $workData)) {
            throw new \RuntimeException('Language code "'.$crowdinLanguageCode.'" is not mapped');
        }

        return $workData[$crowdinLanguageCode];
    }

    /**
     * Returns a list of languages that should be written into the core.
     *
     * @return array
     */
    public static function getCoreLanguages()
    {
        $result = [];
        foreach (self::$languageTargetMap as $language => $target) {
            if ($target === 'core') {
                $result[] = $language;
            }
        }

        return $result;
    }

    public static function getPluginTargetMap()
    {
        $result = [];
        foreach (self::$languageTargetMap as $language => $target) {
            if ($target !== 'core') {
                $result[$language] = $target;
            }
        }

        return $result;
    }

    public static function getPremiumPluginList()
    {
        return self::$premiumPluginList;
    }
}
