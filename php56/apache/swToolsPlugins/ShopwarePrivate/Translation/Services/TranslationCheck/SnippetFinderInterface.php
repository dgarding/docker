<?php

namespace ShopwarePrivate\Translation\Services\TranslationCheck;

interface SnippetFinderInterface
{
    /**
     * @param TranslationLine $translationLine
     * @return string
     */
    public function find(TranslationLine $translationLine);
}