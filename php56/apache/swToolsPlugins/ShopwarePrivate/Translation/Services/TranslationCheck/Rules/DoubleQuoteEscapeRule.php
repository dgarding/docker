<?php

namespace ShopwarePrivate\Translation\Services\TranslationCheck\Rules;

use ShopwarePrivate\Translation\Services\TranslationCheck\Fixer\FixerInterface;
use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationException;
use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationLine;

class DoubleQuoteEscapeRule implements RuleInterface
{
    /**
     * @var FixerInterface
     */
    private $fixer;

    /**
     * @param FixerInterface $fixer
     */
    public function __construct(FixerInterface $fixer)
    {
        $this->fixer = $fixer;
    }

    /**
     * @param TranslationLine $translationLine
     * @return string
     */
    public function applyFix(TranslationLine $translationLine)
    {
        return $this->fixer->fix($translationLine);
    }

    /**
     * @param TranslationLine $translationLine
     * @throws TranslationException
     */
    public function check(TranslationLine $translationLine)
    {
        /**
         * @example:
         * title = "<a href="test.html">...</a>"
         */
        if ($this->hasUnescapedQuote($translationLine)) {
            throw new TranslationException('Unescaped " detected', $translationLine);
        }
    }

    private function hasUnescapedQuote(TranslationLine $translationLine)
    {
        $quoteCount = substr_count($translationLine->content, '"');
        $escapedQuoteCount = substr_count($translationLine->content, '\"');

        //Two apostrophes will be need unescpaed to mark the translation string
        if ($quoteCount - $escapedQuoteCount === 2 || $quoteCount === 0) {
            return false;
        }
        return true;
    }
}