<?php

namespace ShopwarePrivate\Translation\Services\TranslationCheck\Rules;

use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationException;
use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationLine;

interface RuleInterface
{
    /**
     * @param TranslationLine $translationLine
     * @return string
     */
    public function applyFix(TranslationLine $translationLine);

    /**
     * @param TranslationLine $translationLine
     * @throws TranslationException
     */
    public function check(TranslationLine $translationLine);
}