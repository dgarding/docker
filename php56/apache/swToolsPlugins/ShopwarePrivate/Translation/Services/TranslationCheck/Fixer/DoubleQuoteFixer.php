<?php

namespace ShopwarePrivate\Translation\Services\TranslationCheck\Fixer;

use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationLine;

class DoubleQuoteFixer implements FixerInterface
{
    /**
     * @inheritdoc
     */
    public function fix(TranslationLine $translationLine)
    {
        $ini = parse_ini_string($translationLine->content, false, INI_SCANNER_RAW);
        $key = key($ini);
        $value = array_shift($ini);
        $unfixedContent = str_replace('\"', '"', $value);
        $fixedContent = str_replace('"', '\"', $unfixedContent);

        return $key . ' = "' . $fixedContent . '"';
    }
}