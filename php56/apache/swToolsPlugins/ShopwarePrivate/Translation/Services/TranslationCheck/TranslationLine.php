<?php

namespace ShopwarePrivate\Translation\Services\TranslationCheck;

use Symfony\Component\Finder\SplFileInfo;

class TranslationLine
{
    /**
     * @var int
     */
    public $lineNumber;

    /**
     * @var string
     */
    public $content;

    /**
     * @var SplFileInfo
     */
    public $file;

    /**
     * @param int $lineNumber
     * @param string $content
     * @param SplFileInfo $fileInfo
     * @return TranslationLine
     */
    public static function create($lineNumber, $content, SplFileInfo $fileInfo)
    {
        $translationLine = new TranslationLine();
        $translationLine->lineNumber = $lineNumber;
        $translationLine->content = $content;
        $translationLine->file = $fileInfo;
        return $translationLine;
    }
}