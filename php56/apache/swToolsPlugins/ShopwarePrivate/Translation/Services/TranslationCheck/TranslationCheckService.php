<?php

namespace ShopwarePrivate\Translation\Services\TranslationCheck;

use ShopwarePrivate\Translation\Services\TranslationCheck\Rules\RuleInterface;
use Symfony\Component\Finder\SplFileInfo;

class TranslationCheckService
{
    /**
     * @var RuleInterface[]
     */
    private $rules;

    /**
     * @var SmartyTranslatedException[]
     */
    private $smartyErrors;

    /**
     * @param RuleInterface[] ...$rules
     */
    public function __construct(RuleInterface ...$rules)
    {
        $this->rules = $rules;
    }

    /**
     * @param SplFileInfo $fileInfo
     */
    public function check(SplFileInfo $fileInfo)
    {
        $this->smartyErrors = [];
        $translationContent = file($fileInfo->getRealPath());

        foreach ($translationContent as $lineNumber => $content) {
            $translationLine = TranslationLine::create($lineNumber, $content, $fileInfo);

            foreach ($this->rules as $rule) {
                try {
                    $rule->check($translationLine);
                } catch (TranslationException $e) {
                    $translationContent[$lineNumber] = $rule->applyFix($translationLine);
                } catch (SmartyTranslatedException $e) {
                    $this->smartyErrors[] = $e;
                }
            }
        }

        file_put_contents($fileInfo->getRealPath(), implode('', $translationContent));
    }

    /**
     * @return SmartyTranslatedException[]
     */
    public function getSmartyErrors()
    {
        return $this->smartyErrors;
    }
}