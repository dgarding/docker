<?php

namespace ShopwarePrivate\Benchmark\Services\ResultHandlers;

use ShopwarePrivate\Benchmark\TestScenarios\BaseTestScenario;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Helper\TableSeparator;
use Symfony\Component\Console\Output\BufferedOutput;

/**
 * Class BatchTestLoader
 * @package ShopwareCli\Command\Services
 */
class FileResultHandler implements ResultHandlerInterface
{
    /**
     * @var BufferedOutput
     */
    private $resultOutputBuffer;

    /**
     * @var BufferedOutput
     */
    private $fullOutputBuffer;

    /**
     * @var string
     */
    private $resultFileName;

    /**
     * @var string
     */
    private $outputFileName;

    public function __construct()
    {
        $this->resultOutputBuffer = new BufferedOutput();
        $this->fullOutputBuffer = new BufferedOutput();
    }

    /**
     * @param BaseTestScenario $testScenario
     */
    public function handleResult(BaseTestScenario $testScenario)
    {
        $results = $testScenario->getResult();
        foreach ($results as $url => &$result) {
            $result = array_merge(['URL' => $url], $result);
        }
        $averages = $this->getAverage($results);

        $tableValues = array_merge($results, [new TableSeparator()], [$averages]);

        $headers = array_keys(reset($results));

        $this->resultOutputBuffer->writeln('<info>' . $testScenario->getName() . '</info>');
        $this->resultOutputBuffer->writeln('<info>' . $testScenario->getContext() . '</info>');
        $this->resultOutputBuffer->writeln('<info>Testing results:</info>');

        $tableObject = new Table($this->resultOutputBuffer);
        $tableObject->setHeaders($headers);
        $tableObject->setRows($tableValues);
        $tableObject->render();
        $this->resultOutputBuffer->writeln('<info></info>');

        $fullOutput = $testScenario->getFullOutput();
        foreach ($fullOutput as $url => $log) {
            $this->fullOutputBuffer->writeln($url);
            $this->fullOutputBuffer->writeln($log);
            $this->fullOutputBuffer->writeln('');
        }

    }

    /**
     * @inheritdoc
     */
    public function flush()
    {
        $resultFileName = $this->resultFileName ? : 'testResults.txt';
        file_put_contents($resultFileName, $this->resultOutputBuffer->fetch());

        $fullOutputFileName = $this->outputFileName ? : 'testFullOutput.txt';
        file_put_contents($fullOutputFileName, $this->fullOutputBuffer->fetch());
    }

    /**
     * @param string $resultFileName
     */
    public function setResultFileName($resultFileName)
    {
        $this->resultFileName = $resultFileName;
    }

    /**
     * @param string $outputFileName
     */
    public function setOutputFileName($outputFileName)
    {
        $this->outputFileName = $outputFileName;
    }

    private function getAverage($matrix)
    {
        $averages = array_fill_keys(
            array_keys(reset($matrix)),
            0
        );

        foreach ($matrix as $row) {
            foreach ($row as $key => $value) {
                if (!intval($value) && !floatval($value)) {
                    continue;
                }

                $averages[$key] += $value;
            }
        }

        $count = count($matrix);
        $averages = array_map(
            function ($elem) use ($count) {
                return $elem / $count;
            },
            $averages
        );

        if (empty($averages)) {
            return [];
        }
        array_shift($averages);
        array_unshift($averages, 'Avg');

        return $averages;
    }
}
