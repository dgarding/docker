<?php

namespace ShopwarePrivate\Benchmark\Services\ResultHandlers;

use ShopwarePrivate\Benchmark\TestScenarios\BaseTestScenario;
use ShopwareCli\Services\IoService;
use Symfony\Component\Console\Helper\TableSeparator;

/**
 * Class BatchTestLoader
 * @package ShopwareCli\Command\Services
 */
class ConsoleResultHandler implements ResultHandlerInterface
{
    /**
     * @var IoService
     */
    private $ioService;

    /**
     * @param IoService $ioService
     */
    public function __construct(
        IoService $ioService
    ) {
        $this->ioService = $ioService;
    }

    /**
     * @param BaseTestScenario $testScenario
     */
    public function handleResult(BaseTestScenario $testScenario)
    {
        $results = $testScenario->getResult();
        foreach ($results as $url => &$result) {
            $result = array_merge(['URL' => $url], $result);
        }
        $averages = $this->getAverage($results);

        $tableValues = array_merge($results, [new TableSeparator()], [$averages]);

        $headers = array_keys(reset($results));

        $this->ioService->writeln('<info>Testing results:</info>');
        $this->ioService->createTable(
            $headers,
            $tableValues
        )->render();
    }

    public function flush()
    {
        // Nothing to flush
    }

    private function getAverage($matrix)
    {
        $averages = array_fill_keys(
            array_keys(reset($matrix)),
            0
        );

        foreach ($matrix as $row) {
            foreach ($row as $key => $value) {
                if (!intval($value) && !floatval($value)) {
                    continue;
                }

                $averages[$key] += $value;
            }
        }

        $count = count($matrix);
        $averages = array_map(
            function ($elem) use ($count) {
                return $elem / $count;
            },
            $averages
        );

        if (empty($averages)) {
            return [];
        }
        array_shift($averages);
        array_unshift($averages, 'Avg');

        return $averages;
    }
}
