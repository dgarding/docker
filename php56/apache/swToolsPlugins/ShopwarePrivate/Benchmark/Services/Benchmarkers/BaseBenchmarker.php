<?php

namespace ShopwarePrivate\Benchmark\Services\Benchmarkers;

use ShopwarePrivate\Benchmark\TestScenarios\BaseTestScenario;

abstract class BaseBenchmarker
{
    protected $concurrentRequests;

    protected $testsPerScenario;

    protected $hitsPerTest;

    /**
     * @param BaseTestScenario $testScenario
     */
    public abstract function warmUp(BaseTestScenario $testScenario);

    /**
     * @param BaseTestScenario $testScenario
     * @return BaseTestScenario
     */
    public abstract function benchmark(BaseTestScenario $testScenario);

    /**
     * @return mixed
     */
    public function getConcurrentRequests()
    {
        return $this->concurrentRequests;
    }

    /**
     * @param mixed $concurrentRequests
     */
    public function setConcurrentRequests($concurrentRequests)
    {
        $this->concurrentRequests = $concurrentRequests;
    }

    /**
     * @return mixed
     */
    public function getTestsPerScenario()
    {
        return $this->testsPerScenario;
    }

    /**
     * @param mixed $testsPerScenario
     */
    public function setTestsPerScenario($testsPerScenario)
    {
        $this->testsPerScenario = $testsPerScenario;
    }

    /**
     * @return mixed
     */
    public function getHitsPerTest()
    {
        return $this->hitsPerTest;
    }

    /**
     * @param mixed $hitsPerTest
     */
    public function setHitsPerTest($hitsPerTest)
    {
        $this->hitsPerTest = $hitsPerTest;
    }
}