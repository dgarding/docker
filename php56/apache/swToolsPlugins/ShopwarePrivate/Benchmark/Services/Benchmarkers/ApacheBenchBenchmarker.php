<?php

namespace ShopwarePrivate\Benchmark\Services\Benchmarkers;

use ShopwarePrivate\Benchmark\Services\InstallationBridge;
use ShopwarePrivate\Benchmark\TestScenarios\BaseTestScenario;
use ShopwareCli\Services\IoService;
use Symfony\Component\Process\Process;

class ApacheBenchBenchmarker extends BaseBenchmarker
{
    /**
     * @var InstallationBridge
     */
    private $installationBridge;

    /**
     * @var IoService
     */
    private $ioService;

    public function __construct(InstallationBridge $installationBridge, IoService $ioService)
    {
        $this->installationBridge = $installationBridge;
        $this->ioService = $ioService;
    }

    /**
     * @param BaseTestScenario $testScenario
     */
    public function warmUp(BaseTestScenario $testScenario)
    {
        $this->ioService->writeln('Warming up...');
        $testUrls = $testScenario->getTestUrls(1);
        $testUrl = array_shift($testUrls);

        $this->ab($testUrl);
    }

    /**
     * @param BaseTestScenario $testScenario
     * @return BaseTestScenario
     */
    public function benchmark(BaseTestScenario $testScenario)
    {
        $testUrls = $testScenario->getTestUrls($this->testsPerScenario);

        $context = "Apache benchmark, {$this->concurrentRequests} concurrent requests, {$this->testsPerScenario} tests, {$this->hitsPerTest} hits per test.";
        $this->ioService->writeln($context);
        $testScenario->setContext($context);

        $progressBar = $this->ioService->createProgressBar(count($testUrls));

        $results = [];
        $fullOutput = [];
        foreach ($testUrls as $url) {
            $progressBar->advance();

            $output = $this->ab($url);
            $fullOutput[$url] = $output;
            $results[$url] = $this->getMeanResponseTimeDetails($output);
        }
        $progressBar->finish();
        $this->ioService->writeln('');

        $testScenario->setFullOutput($fullOutput);
        $testScenario->setResult($results);

        return $testScenario;
    }

    private function ab($url)
    {
        $command = "ab -q -c {$this->concurrentRequests} -n {$this->testsPerScenario} {$url}";
        $process = new Process($command, null, null, null, $this->testsPerScenario * 30);
        $process->run();
        return $process->getOutput();
    }

    /**
     * @param $output
     * @return array
     */
    private function getMeanResponseTimeDetails($output)
    {
        $results = array();

        $regex = '!(Total):\s+([0-9\.]+)\s+([0-9\.]+)\s+([0-9\.]+)\s+([0-9\.]+)\s+([0-9\.]+)!i';
        preg_match_all($regex, $output, $matches, PREG_SET_ORDER);
        $match = array_pop($matches);
        $results['min'] = $match[2];
        $results['mean'] = $match[3];
        $results['sd'] = $match[4];
        $results['median'] = $match[5];
        $results['max'] = $match[6];

        return $results;
    }
}