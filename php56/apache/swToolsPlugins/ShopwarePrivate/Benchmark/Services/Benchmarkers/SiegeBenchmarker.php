<?php

namespace ShopwarePrivate\Benchmark\Services\Benchmarkers;

use ShopwarePrivate\Benchmark\Services\InstallationBridge;
use ShopwarePrivate\Benchmark\TestScenarios\BaseTestScenario;
use ShopwareCli\Services\IoService;
use Symfony\Component\Process\Process;

class SiegeBenchmarker extends BaseBenchmarker
{
    /**
     * @var InstallationBridge
     */
    private $installationBridge;

    /**
     * @var IoService
     */
    private $ioService;

    public function __construct(InstallationBridge $installationBridge, IoService $ioService)
    {
        $this->installationBridge = $installationBridge;
        $this->ioService = $ioService;
    }

    /**
     * @param BaseTestScenario $testScenario
     */
    public function warmUp(BaseTestScenario $testScenario)
    {
        $this->ioService->writeln('Warming up...');
        $testUrls = $testScenario->getTestUrls(1);
        $testUrl = array_shift($testUrls);

        $this->siege($testUrl);
    }

    /**
     * @param BaseTestScenario $testScenario
     * @return BaseTestScenario
     */
    public function benchmark(BaseTestScenario $testScenario)
    {
        $testUrls = $testScenario->getTestUrls($this->testsPerScenario);

        $context = "Siege benchmark, {$this->concurrentRequests} concurrent requests, {$this->testsPerScenario} tests, {$this->hitsPerTest} hits per test.";
        $this->ioService->writeln($context);
        $testScenario->setContext($context);

        $progressBar = $this->ioService->createProgressBar(count($testUrls));

        $results = [];
        $fullOutput = [];
        foreach ($testUrls as $url) {
            $progressBar->advance();

            $process = $this->siege($url);
            $output = $process->getOutput();
            $errOutput = $process->getErrorOutput();

            $fullOutput[$url] = $errOutput . $output;
            $results[$url] = $this->getResults($errOutput);
        }
        $progressBar->finish();
        $this->ioService->writeln('');

        $testScenario->setFullOutput($fullOutput);
        $testScenario->setResult($results);

        return $testScenario;
    }

    private function siege($url)
    {
        $command = "siege -c {$this->concurrentRequests} -r {$this->testsPerScenario} {$url}";
        $process = new Process($command, null, null, null, $this->testsPerScenario * 30);
        $process->run();
        return $process;
    }

    /**
     * @param $output
     * @return array
     */
    private function getResults($output)
    {
        $results = [];

        $regex = '/(Shortest transaction):\s+([0-9\.]+)/i';
        preg_match($regex, $output, $match);
        $results['min'] = $match[2];

        $regex = '/(Response time):\s+([0-9\.]+)\s(secs)/i';
        preg_match($regex, $output, $match);
        $results['mean'] = $match[2];

        $regex = '/(Longest transaction):\s+([0-9\.]+)/i';
        preg_match($regex, $output, $match);
        $results['max'] = $match[2];

        return $results;
    }
}