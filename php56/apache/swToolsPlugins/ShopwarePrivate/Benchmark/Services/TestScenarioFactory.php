<?php

namespace ShopwarePrivate\Benchmark\Services;


use ShopwarePrivate\Benchmark\TestScenarios\DetailPageTest;
use ShopwarePrivate\Benchmark\TestScenarios\ListingPageTest;
use ShopwarePrivate\Benchmark\TestScenarios\SearchPageTest;
use Symfony\Component\DependencyInjection\Container;

class TestScenarioFactory
{
    /**
     * @var Container
     */
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function build($testType)
    {
        switch ($testType) {
            case 'listing':
                return new ListingPageTest($this->container->get('shopware_benchmark_installation_bridge'));
            case 'detail':
                return new DetailPageTest($this->container->get('shopware_benchmark_installation_bridge'));
            case 'search':
                return new SearchPageTest($this->container->get('shopware_benchmark_installation_bridge'));
            default:
                return null;
        }

    }
}