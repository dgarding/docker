<?php

namespace ShopwarePrivate\Benchmark\Services\TestLoaders;

use ShopwarePrivate\Benchmark\Services\TestScenarioFactory;
use ShopwareCli\Services\IoService;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * Class InteractiveTestLoader
 * @package ShopwareCli\Command\Services
 */
class InteractiveTestLoader implements TestLoaderInterface
{
    /**
     * @var IoService
     */
    private $ioService;

    /**
     * @var TestScenarioFactory
     */
    private $testScenarioFactory;

    /**
     * @param IoService $ioService
     * @param TestScenarioFactory $testScenarioFactory
     */
    public function __construct(
        IoService $ioService,
        TestScenarioFactory $testScenarioFactory
    ) {
        $this->ioService = $ioService;
        $this->testScenarioFactory = $testScenarioFactory;
    }

    /**
     * @inheritdoc
     */
    public function loadTest()
    {
        $question = new ChoiceQuestion(
            "Select a page to test, or '0' to exit",
            array('<exit>', 'listing', 'detail', 'search'),
            0
        );
        $question->setErrorMessage("Option '%s' is invalid.");
        $testTarget = $this->ioService->ask($question);

        switch ($testTarget) {
            case 'listing':
                $this->ioService->cls();
                $this->ioService->writeln('<info>Testing the listing page...</info>');
                break;
            case 'detail':
                $this->ioService->cls();
                $this->ioService->writeln('<info>Testing the article detail page...</info>');
                break;
            case 'search':
                $this->ioService->cls();
                $this->ioService->writeln('<info>Testing the search page...</info>');
                break;
        }

        return $this->testScenarioFactory->build($testTarget);
    }
}
