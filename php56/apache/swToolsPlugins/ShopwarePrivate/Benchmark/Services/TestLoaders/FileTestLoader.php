<?php

namespace ShopwarePrivate\Benchmark\Services\TestLoaders;

use ShopwarePrivate\Benchmark\Services\TestScenarioFactory;

/**
 * Class FileTestLoader
 * @package ShopwareCli\Command\Services\TestLoaders
 */
class FileTestLoader implements TestLoaderInterface
{
    /**
     * @var TestScenarioFactory
     */
    private $testScenarioFactory;

    /**
     * @var string
     */
    private $fileHandle;

    /**
     * @var string
     */
    private $filePath;

    /**
     * @param TestScenarioFactory $testScenarioFactory
     */
    public function __construct(
        TestScenarioFactory $testScenarioFactory
    ) {
        $this->testScenarioFactory = $testScenarioFactory;
    }

    public function setDefinitionFilePath($filePath)
    {
        $this->filePath = $filePath;
    }

    /**
     * @inheritdoc
     */
    public function loadTest()
    {
        if (!$this->fileHandle) {
            if (!is_readable($this->filePath)) {
                throw new \RuntimeException('Invalid test definition path');
            }

            $this->fileHandle = fopen($this->filePath, "r");
        }

        if (($testTarget = trim(fgets($this->fileHandle))) !== false) {
            return $this->testScenarioFactory->build($testTarget);
        }
        fclose($this->fileHandle);
    }
}
