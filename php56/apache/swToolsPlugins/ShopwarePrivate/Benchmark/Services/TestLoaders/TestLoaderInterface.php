<?php

namespace ShopwarePrivate\Benchmark\Services\TestLoaders;

use ShopwarePrivate\Benchmark\TestScenarios\BaseTestScenario;

/**
 * Interface InteractiveTestLoader
 * @package ShopwareCli\Command\Services\TestLoaders
 */
interface TestLoaderInterface
{
    /**
     * @return BaseTestScenario
     */
    public function loadTest();
}
