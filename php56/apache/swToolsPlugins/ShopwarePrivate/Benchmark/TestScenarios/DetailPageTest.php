<?php

namespace ShopwarePrivate\Benchmark\TestScenarios;

use ShopwarePrivate\Benchmark\Services\InstallationBridge;

class DetailPageTest extends BaseTestScenario
{
    /**
     * @var InstallationBridge
     */
    private $installationBridge;

    function __construct(InstallationBridge $installationBridge)
    {
        $this->installationBridge = $installationBridge;
    }

    /**
     * @inheritdoc
     */
    public function getTestUrls($testCount)
    {
        $shopURL = $this->installationBridge->getShopURL();

        $ids = $this->installationBridge->getData('s_articles', 'id', $testCount);
        $instances = [];

        foreach ($ids as $id) {
            $url = $shopURL . sprintf('/detail/index/sArticle/%s', $id);
            $instances[] = $url;
        }

        return $instances;
    }

    public function getName()
    {
        return 'Detail page test';
    }
}
