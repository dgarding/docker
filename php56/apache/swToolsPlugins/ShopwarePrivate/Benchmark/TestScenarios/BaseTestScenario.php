<?php

namespace ShopwarePrivate\Benchmark\TestScenarios;

/**
 * Base test scenario
 *
 * Class BaseTestScenario
 * @package Shopware\Benchmark\TestScenarios
 */
abstract class BaseTestScenario
{
    /**
     * @var array
     */
    protected $result;

    /**
     * @var array
     */
    protected $fullOutput;

    /**
     * @var string
     */
    protected $context;

    /**
     * @param $testCount Number of tests to load
     * @return string[]
     */
    abstract public function getTestUrls($testCount);

    abstract public function getName();

    /**
     * @return string
     */
    public function getContext()
    {
        return $this->context;
    }

    /**
     * @param string $context
     */
    public function setContext($context)
    {
        $this->context = $context;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }

    /**
     * @param mixed $result
     */
    public function setResult($result)
    {
        $this->result = $result;
    }

    /**
     * @return array
     */
    public function getFullOutput()
    {
        return $this->fullOutput;
    }

    /**
     * @param array $fullOutput
     */
    public function setFullOutput($fullOutput)
    {
        $this->fullOutput = $fullOutput;
    }
}
