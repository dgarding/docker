<?php

namespace ShopwarePrivate\Import\Plugins;

class PluginRegistry
{
    /**
     * @var Plugin[]
     */
    private $plugins = [];

    public function __construct()
    {
        $this->plugins = [
            // Premium Plugins
            new GitlabPlugin('SwagAboCommerce', '10600'),
            new GitlabPlugin('SwagAdvancedCart', '11003'),
            new GitlabPlugin('SwagBonusSystem', '10607'),
            new GitlabPlugin('SwagBundle', '10608'),
            new GitlabPlugin('SwagBusinessEssentials', '10609'),
            new GitlabPlugin('SwagCustomProducts', '10601'),
            new GitlabPlugin('SwagDigitalPublishing', '11101'),
            new GitlabPlugin('SwagEmotionAdvanced', '11002'),
            new GitlabPlugin('SwagFuzzy', '10616'),
            new GitlabPlugin('SwagLiveShopping', '10611'),
            new GitlabPlugin('SwagNewsletter', '10612'),
            new GitlabPlugin('SwagProductAdvisor', '10613'),
            new GitlabPlugin('SwagPromotion', '11000'),
            new GitlabPlugin('SwagTicketSystem', '10617'),

            // free plugins
            new GithubPlugin('SwagBackendOrder', '11100'),
            new GithubPlugin('SwagMigration', '10621'),
            new GithubPlugin('SwagImportExport', '10703'),
            new GithubPlugin('SwagPaymentPayPalUnified', '11402'),
            new GithubPlugin('SwagPaymentPaypal', '10603'),
            new GithubPlugin('SwagPaymentPaypalPlus', '11104'),
            new GithubPlugin('SwagUserPrice', '11303'),

            new GitlabPlugin('SwagVatIdValidation', '11200'),

            // plugins without jira name ID -> sonstige
            new GithubPlugin('SwagBrowserLanguage', '10619'),
            new GithubPlugin('SwagGoogle', '10619'),
            new GithubPlugin('SwagDefaultSort', '10619'),
        ];
    }

    /**
     * @param string $technicalName
     * @return Plugin
     * @throws \Exception
     */
    public function get($technicalName)
    {
        $plugin = array_filter($this->plugins, function ($plugin) use ($technicalName) {
            /** @var Plugin $plugin */
            return $plugin->getTechnicalName() === $technicalName;
        });

        $plugin = array_shift($plugin);

        if (!$plugin instanceof Plugin) {
            throw new \Exception('Plugin ' . $technicalName . ' not found.');
        }

        return $plugin;
    }
}
