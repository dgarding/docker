<?php

namespace ShopwarePrivate\Import\Jira\Issue;

class ConnectIssue extends AbstractIssue
{
    public function getProjectKey()
    {
        return 'CON';
    }

    public function getIssueType()
    {
        return 'Bug';
    }
}