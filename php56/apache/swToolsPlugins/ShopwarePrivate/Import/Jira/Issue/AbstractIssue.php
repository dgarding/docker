<?php

namespace ShopwarePrivate\Import\Jira\Issue;

use ShopwarePrivate\Import\Struct\CodeSource;
use ShopwarePrivate\Import\Struct\PullRequest;

abstract class AbstractIssue
{
    const GITHUB_AUTHOR_FIELD = 'customfield_12101';
    const GITHUB_PULL_REQUEST_FIELD = 'customfield_12100';
    const IS_PUBLIC_FIELD = 'customfield_10202';

    /**
     * @var PullRequest
     */
    protected $pullRequest;

    /**
     * @var CodeSource
     */
    protected $codeSource;

    /**
     * @param PullRequest $pullRequest
     * @param CodeSource $codeSource
     */
    public function __construct(CodeSource $codeSource, PullRequest $pullRequest)
    {
        $this->codeSource = $codeSource;
        $this->pullRequest = $pullRequest;
    }


    public abstract function getProjectKey();
    public abstract function getIssueType();

    public function getSummary()
    {
        return sprintf('[%s] %s', $this->codeSource->getPlatform(), $this->pullRequest->getTitle());
    }

    public function getDescription()
    {
        return $this->pullRequest->getDescription();
    }

    /**
     * @return array
     */
    public function getAdditionalFields()
    {
        return [];
    }

    public function toArray()
    {
        $payload = [
            'project' => ['key' => $this->getProjectKey()],
            'issuetype' => ['name' => $this->getIssueType()],
            'summary' => $this->getSummary(),
            'description' => $this->getDescription(),

            self::GITHUB_AUTHOR_FIELD => $this->pullRequest->getUser(),
            self::GITHUB_PULL_REQUEST_FIELD => $this->pullRequest->getUrl(),
            self::IS_PUBLIC_FIELD => ['id' => '10110'],

        ];

        return ['fields' => array_merge($payload, $this->getAdditionalFields())];
    }
}