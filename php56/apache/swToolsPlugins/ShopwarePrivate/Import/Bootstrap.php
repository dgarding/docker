<?php

namespace ShopwarePrivate\Import;

require __DIR__ . '/../vendor/autoload.php';

use ShopwareCli\Application\ConsoleAwareExtension;
use ShopwarePrivate\Import\Commands\ImportConnectCommand;
use ShopwarePrivate\Import\Commands\ImportShopwareCommand;
use ShopwarePrivate\Import\Commands\ImportPluginCommand;

class Bootstrap implements ConsoleAwareExtension
{
    /**
     * @inheritdoc
     */
    public function getConsoleCommands()
    {
        return [
            new ImportShopwareCommand(),
            new ImportPluginCommand(),
            new ImportConnectCommand(),
        ];
    }
}