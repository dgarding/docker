<?php

namespace ShopwarePrivate\Import\Platform\Client;

use Gitlab\Client;
use ShopwarePrivate\Import\Platform\PlatformClientInterface;
use ShopwarePrivate\Import\Struct\CodeSource;
use ShopwarePrivate\Import\Struct\PullRequest;

class GitlabClient implements PlatformClientInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var CodeSource|null
     */
    private $codeSource;

    /**
     * @var string
     */
    private $gitlabToken;

    /**
     * @var int
     */
    private $projectId;

    /**
     * @param string $gitlabToken
     * @param CodeSource $codeSource
     */
    private function __construct($gitlabToken, CodeSource $codeSource)
    {
        $this->gitlabToken = $gitlabToken;
        $this->codeSource = $codeSource;

        $client = Client::create('https://git.shopware.com');
        $client->authenticate($this->gitlabToken, Client::AUTH_HTTP_TOKEN);

        $this->client = $client;
        $this->projectId = $this->fetchProjectId();

    }

    /**
     * @param array $config
     * @param CodeSource $codeSource
     *
     * @return PlatformClientInterface
     */
    public static function create(array $config, CodeSource $codeSource)
    {
        return new self($config['gitlabToken'], $codeSource);
    }

    /**
     * @param int $number
     *
     * @return PullRequest
     */
    public function getPullRequest($number)
    {
        $data = $this->client->mergeRequests()->show($this->getProjectId(), $number);

        $pullRequest = new PullRequest();
        $pullRequest->setTitle($data['title']);
        $pullRequest->setUrl($data['web_url']);
        $pullRequest->setUser($data['author']['username']);
        $pullRequest->setDescription($data['description']);

        return $pullRequest;
    }

    /**
     * @param int $number
     * @param string $issueUrl
     *
     * @return string Comment url
     */
    public function createComment($number, $issueUrl)
    {
        $content = sprintf(
            PlatformClientInterface::COMMENT_TEMPLATE,
            $issueUrl
        );

        $response = $this->client->mergeRequests()->addComment($this->getProjectId(), $number, $content);

        return sprintf(
            'https://git.shopware.com/plugins/%s/merge_requests/%d#note_%d',
            $this->codeSource->getRepository(),
            $number,
            $response['id']
        );
    }

    /**
     * @return int
     */
    private function fetchProjectId()
    {
        $projects = $this->client->projects()->all(['search' => $this->codeSource->getRepository()]);
        $project = array_shift($projects);

        return (int) $project['id'];
    }

    /**
     * @return int
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * @param int $number
     * @param array $labels
     *
     * @return void
     */
    public function setLabels($number, array $labels)
    {
    }
}