<?php

namespace ShopwarePrivate\Import\Platform;

use ShopwarePrivate\Import\Struct\CodeSource;

class Factory
{
    /**
     * @var string[]
     */
    private $platforms;

    /**
     * @param string[] $platforms
     */
    public function __construct(array $platforms)
    {
        $this->platforms = $platforms;
    }

    public function createClient(array $config, CodeSource $codeSource)
    {
        if (!array_key_exists($codeSource->getPlatform(), $this->platforms)) {
            throw new \RuntimeException(sprintf('No platform client found for "%s".', $codeSource->getPlatform()));
        }

        /** @var PlatformClientInterface $platform */
        $platform = $this->platforms[$codeSource->getPlatform()];

        return $platform::create($config, $codeSource);
    }
}