<?php

namespace ShopwarePrivate\Account\Struct;

use ShopwareCli\Struct;

class StorePlugin extends Struct
{
    public $id;

    public $articleId;

    public $name;

    public $description;

    public $rentModus;

    public $testModus;

    public $price;

    public $license;

    public $versionFrom;

    public $versionTo;

    public $complete;

    public $approved;

    public $released;

    public $denied;

    public $disabled;

    public $denyNotice;

    public $infoMail;

    public $bookingAccount;

    public $deprecated;

    public $priceModus;

    public $rentPrice;

    public $status;

    public $orderNumber;
}
