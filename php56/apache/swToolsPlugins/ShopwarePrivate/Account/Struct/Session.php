<?php

namespace ShopwarePrivate\Account\Struct;

use ShopwareCli\Struct;

class Session extends Struct
{
    public $id;
    public $token;
    public $shopwareId;
}
