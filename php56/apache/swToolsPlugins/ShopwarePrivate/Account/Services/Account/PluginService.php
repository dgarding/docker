<?php

namespace ShopwarePrivate\Account\Services\Account;

use ShopwareCli\Services\Rest\RestInterface;

class PluginService
{
    /**
     * @var \ShopwareCli\Services\Rest\RestInterface
     */
    private $restClient;
    /**
     * @var \ShopwarePrivate\Account\Struct\Session
     */
    private $session;

    public function __construct(RestInterface $restClient)
    {
        $this->restClient = $restClient;
    }

    public function setSession($session)
    {
        $this->session = $session;
    }

    public function getPluginByNumber($number)
    {
        $result = $this->restClient->get(
            "plugins?search={$number}&producerId=1",
            array(),
            array('X-Shopware-Token:' . $this->session->token)
        );

        if ($result->getCode() < 200 || $result->getCode() >= 300) {
            throw new \RuntimeException($result->getRawBody());
        }

        if (!$result->getResult()) {
            throw new \RuntimeException("Could not find plugin with code {$number}");
        }

        $plugins = array_filter(
            $result->getResult(),
            function ($plugin) use ($number) {
                return $plugin['name'] == $number;
            }
        );

        $plugin = array_pop($plugins);

        return $plugin;
    }

    public function getPluginBinary($plugin, $currentVersion)
    {
        $result = $this->restClient->get(
            "plugins/{$plugin['id']}/binaries",
            array(),
            array('X-Shopware-Token:' . $this->session->token)
        );

        if ($result->getCode() < 200 || $result->getCode() >= 300) {
            throw new \RuntimeException($result->getRawBody());
        }

        $binaries = array_filter(
            $result->getResult(),
            function ($binary) use ($currentVersion) {
                return $binary['version'] == $currentVersion;
            }
        );

        if (!$binaries) {
            return null;
        }

        return array_pop($binaries);
    }

    /**
     * Will return all available shopware versions with at least 3 places
     *
     * @return array
     */
    public function getSoftwareVersions()
    {
        $result = $this->restClient->get(
            'pluginstatics/softwareVersions',
            array()
        );

        if ($result->getCode() < 200 || $result->getCode() >= 300) {
            throw new \RuntimeException($result->getErrorMessage());
        }

        // Return a plain list of shopware versions
        return array_map(
            function ($version) {
                return $version['name'];
            },
            array_filter(
                $result->getResult(),
                function ($version) {
                    return $version['selectable'];
                }
            )
        );
    }

    /**
     * Set version and changelog for a given plugin binary
     *
     * @param $sbpPlugin
     * @param $sbpBinary
     * @param $changelogs
     * @param $versions
     * @return mixed
     */
    public function updateBinary($sbpPlugin, $sbpBinary, $changelogs, $versions)
    {
        $sbpBinary['compatibleSoftwareVersions'] = array_map(
            function ($version) {
                return array('name' => $version);
            },
            $versions
        );

        $sbpBinary['changelogs'] = $changelogs;
        $sbpBinary['ionCubeEncrypted'] = false;
        $sbpBinary['licenseCheckRequired'] = false;

        $result = $this->restClient->put(
            'plugins/' . $sbpPlugin['id'] . '/binaries/' . $sbpBinary['id'],
            $sbpBinary,
            array('X-Shopware-Token:' . $this->session->token)
        );

        if ($result->getCode() < 200 || $result->getCode() >= 300) {
            throw new \RuntimeException($result->getRawBody());
        }

        $binary = $result->getResult();

        return $binary;
    }

    /**
     * Upload a plugin file
     *
     * @param $name
     * @param $file
     * @param $pluginId
     * @param null $binaryId
     * @return mixed
     */
    public function upload($name, $file, $pluginId, $binaryId = null)
    {
        if ($binaryId) {
            $target_url = 'https://api.shopware.com/plugins/' . $pluginId . '/binaries/' . $binaryId . '/file?token=' . $this->session->token;
        } else {
            $target_url = 'https://api.shopware.com/plugins/' . $pluginId . '/binaries?token=' . $this->session->token;
        }

        $file = realpath($file);

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $target_url);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-Shopware-Token: ' . $this->session->token));
        curl_setopt(
            $ch,
            CURLOPT_POSTFIELDS,
            array(
                'file' => curl_file_create($file, 'application/zip', $name . '.zip')
            )
        );
        //curl_setopt($ch, CURLOPT_VERBOSE, true);

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);

        return current(json_decode($result, true));
    }

    /**
     * Trigger a review at the SBP core
     *
     * @param $pluginId
     * @return bool|mixed
     */
    public function triggerReview($pluginId)
    {
        $result = $this->restClient->post(
            "plugins/{$pluginId}/reviews",
            array(),
            array('X-Shopware-Token:' . $this->session->token)
        );

        if ($result->getCode() >= 200 && $result->getCode() < 300) {
            return true;
        }

        return $result->getResult();
    }
}
