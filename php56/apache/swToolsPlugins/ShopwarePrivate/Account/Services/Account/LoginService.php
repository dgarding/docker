<?php

namespace ShopwarePrivate\Account\Services\Account;

use ShopwarePrivate\Account\Struct\Session;
use ShopwareCli\Services\Rest\RestInterface;

class LoginService
{
    /**
     * @var RestInterface
     */
    private $restClient;

    public function __construct(RestInterface $restClient)
    {
        $this->restClient = $restClient;
    }

    public function login($shopwareId, $password)
    {
        $response = $this->restClient->post(
            'accesstokens',
            [
                'shopwareId' => $shopwareId,
                'password' => $password
            ]
        );

        if ($response->getErrorMessage()) {
            throw new \RuntimeException($response->getErrorMessage());
        }

        $result = $response->getResult();

        if (!isset($result['token'])) {
            throw new \RuntimeException($response->getErrorMessage());
        }

        $session = new Session();
        $session->id = $result['userId'];
        $session->token = $result['token'];
        $session->shopwareId = $shopwareId;

        return $session;
    }

    public function logout($token)
    {
        $response = $this->restClient->delete(
            'accesstokens/' . $token
        );

        return $response->getResult();
    }
}
