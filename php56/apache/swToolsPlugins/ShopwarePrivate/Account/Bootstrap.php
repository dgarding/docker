<?php

namespace ShopwarePrivate\Account;

use ShopwareCli\Application\ConsoleAwareExtension;
use ShopwareCli\Application\ContainerAwareExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class Bootstrap implements ContainerAwareExtension, ConsoleAwareExtension
{
    protected $container;

    public function setContainer(ContainerBuilder $container)
    {
        $this->container = $container;


        $this->container->register('account_service_factory', '\ShopwarePrivate\Account\Services\Account\AccountFactory')
            ->addArgument(new Reference('service_container'));

        $this->container->register('version_service_factory', '\ShopwarePrivate\Account\Services\VersionServiceFactory');
        $this->container->register('version_validator_factory', '\ShopwarePrivate\Account\Services\VersionValidatorFactory');
    }

    /**
     * Return an array with instances of your console commands here
     *
     * @return mixed
     */
    public function getConsoleCommands()
    {
        return array(
            new Command\UploadCommand()
        );
    }
}
