<?php

namespace ShopwarePrivate\Dumper\Struct;

use ShopwareCli\Struct;

class ConnectionInfo extends Struct
{
    public $username;
    public $password;
    public $port = 3306;
    public $host = 'mysql';
    public $databaseName;
}