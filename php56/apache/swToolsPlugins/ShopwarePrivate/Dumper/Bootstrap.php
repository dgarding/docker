<?php

namespace ShopwarePrivate\Dumper;

use ShopwarePrivate\Dumper\Command\DumpCommand;
use ShopwareCli\Application\ConsoleAwareExtension;
use ShopwareCli\Application\ContainerAwareExtension;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Reference;

class Bootstrap implements ConsoleAwareExtension, ContainerAwareExtension
{
    /**
     * @param ContainerBuilder $container
     */
    public function setContainer(ContainerBuilder $container)
    {
        $container->register('shopware_dump_service', 'ShopwarePrivate\Dumper\Service\Dump')
            ->addArgument( new Reference('process_executor'))
            ->addArgument( new Reference('io_service'));
    }


    /**
     * Return an array with instances of your console commands here
     *
     * @return Command[]
     */
    public function getConsoleCommands()
    {
        return array(
            new DumpCommand()
        );
    }

}