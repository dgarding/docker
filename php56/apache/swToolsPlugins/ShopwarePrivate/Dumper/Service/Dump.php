<?php

namespace ShopwarePrivate\Dumper\Service;

use ShopwarePrivate\Dumper\Struct\ConnectionInfo;
use ShopwareCli\Services\IoService;
use ShopwareCli\Services\ProcessExecutor;
use Symfony\Component\Finder\Iterator\RecursiveDirectoryIterator;

class Dump
{
    /**
     * @var \ShopwareCli\Services\ProcessExecutor
     */
    private $processExecutor;
    /**
     * @var \ShopwareCli\Services\IoService
     */
    private $ioService;

    public function __construct(ProcessExecutor $processExecutor, IoService $ioService)
    {
        $this->processExecutor = $processExecutor;
        $this->ioService = $ioService;
    }

    /**
     * Dump a shopware installation (DB + files)
     *
     * @param $shopwarePath
     * @param $outputPath
     * @param ConnectionInfo $info
     * @param bool $includeMedia
     */
    public function dump($shopwarePath, $outputPath, ConnectionInfo $info, $includeMedia = true)
    {
        $this->ioService->writeln('Dumping database');
        $this->createSqlDump($outputPath . '/dump.sql', $info);

        $this->ioService->writeln('Creating zip');
        $this->zipShopware($shopwarePath, $outputPath . '/dump.zip', $includeMedia);
    }

    /**
     * Dump a given database
     *
     * @param $outputFile
     * @param ConnectionInfo $info
     */
    private function createSqlDump($outputFile, ConnectionInfo $info)
    {
        $this->processExecutor->execute(
            "mysqldump -u{$info->username} -p{$info->password} -P{$info->port} -h{$info->host} {$info->databaseName} > {$outputFile}"
        );
    }

    /**
     * Zip a given shopware directory
     *
     * @param $shopwarePath
     * @param $zipFile
     * @param $includeMedia
     * @throws \RuntimeException
     */
    private function zipShopware($shopwarePath, $zipFile, $includeMedia)
    {
        $zip = new \ZipArchive();

        if ($zip->open($zipFile, \ZipArchive::CREATE) !== TRUE) {
            throw new \RuntimeException("Could not create <$$zipFile>\n");
        }

        $iterator = new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($shopwarePath));

        $count = $this->countFiles($shopwarePath, $includeMedia, $iterator);

        $bar = $this->ioService->createProgressBar($count);
        $bar->setRedrawFrequency(100);
        $bar->start();

        /** @var  $fileInfo \SplFileInfo */
        foreach ($iterator as $name => $fileInfo) {
            $path = str_replace(rtrim($shopwarePath, '/'), '', $fileInfo->getPath());

            if ($this->skipFile($includeMedia, $fileInfo, $path)) {
                continue;
            }


            $bar->advance();

            $zip->addFile($name);
        }

        $this->ioService->writeln("\nWriting archive…");


        $zip->close();

        $this->ioService->writeln("");
        $this->ioService->writeln("Done. Files stored in {$zipFile}");
    }

    /**
     * Step through the iterator and count elements
     *
     * @param $shopwarePath
     * @param $includeMedia
     * @param $iterator
     * @return int
     */
    private function countFiles($shopwarePath, $includeMedia, $iterator)
    {
        $count = 0;

        /** @var  $fileInfo \SplFileInfo */
        foreach ($iterator as $name => $fileInfo) {
            $path = str_replace(rtrim($shopwarePath, '/'), '', $fileInfo->getPath());

            if ($this->skipFile($includeMedia, $fileInfo, $path)) {
                continue;
            }

            $count++;
        }

        return $count;
    }

    /**
     * Check if an element should be skipped
     *
     * @param $includeMedia
     * @param \SplFileInfo $fileInfo
     * @param $path
     * @return bool
     */
    private function skipFile($includeMedia, \SplFileInfo $fileInfo, $path)
    {
        // Skip files in cache folder
        if ($fileInfo->isFile() && strpos($path, '/cache') !== false) {
            return true;
        }

        // Skip git files
        if (strpos($path, '/.git') !== false) {
            return true;
        }

        if (!$includeMedia && (strpos($path, '/media') === 0 || strpos($path, '/files') === 0)) {
            return true;
        }

        return false;
    }
}