# Docker

To start the docker container navigate to your favor php folder and type : `./start.sh`

To open the apache *bash* type: `./bash.sh` to execute ant build-unit for example

To use mailhog: 
 - smtp 
 - ip:172.20.0.3 (To get the ip for example ~`docker inspect php72_mailhog_1 | grep IPAddress`)
 - port: 1025
 