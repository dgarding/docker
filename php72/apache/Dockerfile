FROM php:7.2-apache

RUN mkdir -p /usr/share/man/man1

ADD php/_010_php.ini /usr/local/etc/php/conf.d/

RUN apt-get update && apt-get install --no-install-recommends -y \
    ant \
    libc-bin \
    pkg-config \
    libcurl4-openssl-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libwebp-dev \
    libmcrypt-dev \
    libpng-dev \
    zip \
    zlib1g-dev \
    unzip \
    git \
    wget \
    tar \
    nano \
    libxml2 \
    libxml2-dev \
    openssl \
    dnsmasq \
    dnsutils \
    libssl-dev \
    curl \
    gnupg \
    gnupg1 \
    gnupg2

RUN apt-get -y update \
    && apt-get install -y libicu-dev \
    && docker-php-ext-configure intl \
    && docker-php-ext-install intl

RUN docker-php-ext-install -j$(nproc) iconv
RUN docker-php-ext-install -j$(nproc) mysqli
RUN docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/ --with-webp-dir=/usr/include
RUN docker-php-ext-install -j$(nproc) gd
RUN docker-php-ext-install mbstring
RUN docker-php-ext-install zip
RUN docker-php-ext-install ftp
RUN docker-php-ext-install soap
RUN docker-php-ext-install pdo pdo_mysql

RUN mkdir /etc/ssmtp/
RUN echo "sendmail_path = /usr/sbin/ssmtp -t" > /usr/local/etc/php/conf.d/_011_sendmail.ini
RUN echo "mailhub=smtp:1025\nUseTLS=NO\nFromLineOverride=YES" > /etc/ssmtp/ssmtp.conf

RUN rm -rf /var/lib/apt/lists/*
RUN echo "date.timezone = 'Europe/Berlin'" >> /usr/local/etc/php/php.ini

RUN a2enmod vhost_alias
RUN a2enmod rewrite
RUN a2enmod ssl
RUN a2ensite 000-default
RUN a2ensite default-ssl

RUN echo -e "ENABLED=1\nIGNORE_RESOLVCONF=yes" > /etc/default/dnsmasq
# RUN echo -e "nameserver 127.0.0.1\ndomain localdomain\nsearch localdomain" > /etc/resolv.conf
RUN usermod -u 1000 www-data
RUN chown -R www-data /var/www

COPY --from=composer /usr/bin/composer /usr/bin/composer

WORKDIR /var/www/html

RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -

RUN apt-get update && apt-get install --no-install-recommends -y \
    nodejs \
    build-essential