<?php

namespace ShopwarePrivate\Connect;

use ShopwareCli\Application\ConsoleAwareExtension;
use ShopwarePrivate\Connect\Account\UploadCommandExtension;
use ShopwarePrivate\Connect\Zip\ZipCommandExtension;
use Symfony\Component\Console\Command\Command;

class Bootstrap implements ConsoleAwareExtension
{
    /**
     * Return an array with instances of your console commands here
     *
     * @return Command[]
     */
    public function getConsoleCommands()
    {
        return [
            new ZipCommandExtension(),
            new UploadCommandExtension()
        ];
    }
}