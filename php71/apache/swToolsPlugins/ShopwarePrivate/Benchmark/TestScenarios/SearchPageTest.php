<?php

namespace ShopwarePrivate\Benchmark\TestScenarios;

use ShopwarePrivate\Benchmark\Services\InstallationBridge;

class SearchPageTest extends BaseTestScenario
{
    /**
     * @var InstallationBridge
     */
    private $installationBridge;

    function __construct(InstallationBridge $installationBridge)
    {
        $this->installationBridge = $installationBridge;
    }

    /**
     * @inheritdoc
     */
    public function getTestUrls($testCount)
    {
        $shopURL = $this->installationBridge->getShopURL();

        $names = $this->installationBridge->getData('s_articles', 'name', $testCount);
        $instances = [];

        foreach ($names as $name) {
            $name = explode(' ', trim($name));

            $url = $shopURL . sprintf('/search?sSearch=%s', $name[array_rand($name, 1)]);
            $instances[] = $url;
        }

        return $instances;
    }

    public function getName()
    {
        return 'Search page with partial article name test';
    }
}
