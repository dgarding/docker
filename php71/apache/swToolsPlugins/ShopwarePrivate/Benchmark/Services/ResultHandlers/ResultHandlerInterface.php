<?php

namespace ShopwarePrivate\Benchmark\Services\ResultHandlers;

use ShopwarePrivate\Benchmark\TestScenarios\BaseTestScenario;

/**
 * Interface ResultHandlerInterface
 * @package ShopwareCli\Command\Services\ResultHandlers
 */
interface ResultHandlerInterface
{
    /**
     * @param BaseTestScenario $testScenario
     */
    public function handleResult(BaseTestScenario $testScenario);

    public function flush();
}
