<?php

namespace ShopwarePrivate\Benchmark\Services;

use ShopwarePrivate\Benchmark\Bootstrap;

class InstallationBridge
{
    /**
     * @var string
     */
    private $installDir;

    /**
     * @var array
     */
    private $shopwareConfig;
    /**
     * @var array
     */
    private $shopData;

    /**
     * @var \PDO
     */
    private $connection;

    /**
     * @param mixed $installDir
     */
    public function setInstallDir($installDir)
    {
        if (!is_readable($installDir.'/shopware.php')) {
            throw new \RuntimeException('Invalid Shopware installation path');
        }

        $this->installDir = $installDir;
        $this->reloadConfig();
        $this->getDatabaseConnection();
    }

    private function reloadConfig()
    {
        if (!$this->installDir) {
            throw new \RuntimeException('The Shopware installation dir was not set or is invalid');
        }

        $configFilePath = realpath($this->installDir.'/config.php');

        if (!is_readable($configFilePath)) {
            throw new \RuntimeException('Invalid Shopware config file path');
        }

        $this->shopwareConfig = require $configFilePath;
        $this->shopData = null;
    }

    private function writeConfig()
    {
        $exportContent = '<?php return '.var_export($this->shopwareConfig, true).';';
        file_put_contents($this->installDir.'/config.php', $exportContent);
    }

    public function enableElasticSearch()
    {
        if (!$this->installDir) {
            throw new \RuntimeException('The Shopware installation dir was not set or is invalid');
        }

        if (!array_key_exists('es', $this->shopwareConfig)) {
            $this->shopwareConfig['es'] = [
                'prefix' => 'sw_shop',
                'enabled' => true,
                'client' => [
                    'hosts' => [
                        'localhost:9200'
                    ]
                ]
            ];
        } elseif ($this->shopwareConfig['es']['enabled'] == false) {
            $this->shopwareConfig['es']['enabled'] = true;
        } else {
            return;
        }

        $this->writeConfig();
    }

    public function disablePerformanceFeatures()
    {
        if (!$this->installDir) {
            throw new \RuntimeException('The Shopware installation dir was not set or is invalid');
        }

        $queries = [
            "UPDATE `s_core_config_elements` SET `value`= 'i:0;' WHERE `name` LIKE 'showPriceFacet';",
            "UPDATE `s_core_config_elements` SET `value`= 'i:1;' WHERE `name` LIKE 'topSellerRefreshStrategy';",
            "UPDATE `s_core_config_elements` SET `value`= 'i:1;' WHERE `name` LIKE 'searchRefreshStrategy';",
            "UPDATE `s_core_config_elements` SET `value`= 'i:1;' WHERE `name` LIKE 'seoRefreshStrategy';",
            "UPDATE `s_core_config_elements` SET `value`= 'i:1;' WHERE `name` LIKE 'similarRefreshStrategy';"
        ];

        foreach ($queries as $query) {
            $this->connection->exec($query);
        }

        $this->shopwareConfig['httpCache'] = ['enabled' => false];

        $this->writeConfig();
    }

    private function getDatabaseConnection()
    {
        if (!$this->installDir) {
            throw new \RuntimeException('The Shopware installation dir was not set or is invalid');
        }

        $host = $this->shopwareConfig['db']['host'];
        $username = $this->shopwareConfig['db']['username'];
        $password = $this->shopwareConfig['db']['password'];
        $database = $this->shopwareConfig['db']['dbname'];

        $this->connection = new \PDO("mysql:host={$host};charset=utf8", $username, $password);
        $this->connection->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);
        $this->connection->query("use `{$database}`;");
    }

    public function getData($table, $column, $count)
    {
        $ids = $this->connection->query("SELECT {$column} FROM {$table} ORDER BY RAND() LIMIT {$count}")
            ->fetchAll(\PDO::FETCH_COLUMN);

        return $ids;
    }

    public function getShopURL()
    {
        if (!$this->shopData) {
            $this->shopData = $this->connection->query("SELECT host, base_path FROM s_core_shops WHERE `default` = 1")
                ->fetch(\PDO::FETCH_ASSOC);
        }

        $url = "http://{$this->shopData['host']}";
        if (!empty($this->shopData['base_path'])) {
            $url .= $this->shopData['base_path'];
        }

        return $url;
    }
}