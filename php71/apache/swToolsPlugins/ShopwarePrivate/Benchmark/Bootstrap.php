<?php

namespace ShopwarePrivate\Benchmark;

use ShopwarePrivate\Benchmark\Command\RunBenchmarkCommand;
use ShopwareCli\Application\ConsoleAwareExtension;
use ShopwareCli\Application\ContainerAwareExtension;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class Bootstrap implements ContainerAwareExtension, ConsoleAwareExtension
{
    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerBuilder $container)
    {
        $this->populateContainer($container);
    }

    /**
     * {@inheritdoc}
     */
    public function getConsoleCommands()
    {
        return array(
            new RunBenchmarkCommand(),
        );
    }

    /**
     * @param ContainerBuilder $container
     */
    private function populateContainer(ContainerBuilder $container)
    {
        $container->register(
            'shopware_benchmark_installation_bridge',
            'ShopwarePrivate\Benchmark\Services\InstallationBridge'
        );

        $container->register(
            'shopware_benchmark_test_scenario_factory',
            'ShopwarePrivate\Benchmark\Services\TestScenarioFactory'
        )
            ->addArgument(new Reference('service_container'));

        /**
         * Benchmarkers
         */
        $container->register(
            'shopware_benchmark_apache_bench_benchmarker',
            'ShopwarePrivate\Benchmark\Services\Benchmarkers\ApacheBenchBenchmarker'
        )
            ->addArgument(new Reference('shopware_benchmark_installation_bridge'))
            ->addArgument(new Reference('io_service'));

        $container->register(
            'shopware_benchmark_siege_benchmarker',
            'ShopwarePrivate\Benchmark\Services\Benchmarkers\SiegeBenchmarker'
        )
            ->addArgument(new Reference('shopware_benchmark_installation_bridge'))
            ->addArgument(new Reference('io_service'));


        /**
         * Test loaders
         */
        $container->register(
            'shopware_benchmark_interactive_test_loader',
            'ShopwarePrivate\Benchmark\Services\TestLoaders\InteractiveTestLoader'
        )
            ->addArgument(new Reference('io_service'))
        ->addArgument(new Reference('shopware_benchmark_test_scenario_factory'));

        $container->register(
            'shopware_benchmark_file_test_loader',
            'ShopwarePrivate\Benchmark\Services\TestLoaders\FileTestLoader'
        )
            ->addArgument(new Reference('shopware_benchmark_test_scenario_factory'));

        /**
         * Result handlers
         */
        $container->register(
            'shopware_benchmark_console_result_handler',
            'ShopwarePrivate\Benchmark\Services\ResultHandlers\ConsoleResultHandler'
        )
            ->addArgument(new Reference('io_service'));

        $container->register(
            'shopware_benchmark_file_result_handler',
            'ShopwarePrivate\Benchmark\Services\ResultHandlers\FileResultHandler'
        );
    }
}
