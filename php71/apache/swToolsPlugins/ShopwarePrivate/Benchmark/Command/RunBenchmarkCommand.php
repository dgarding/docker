<?php
namespace ShopwarePrivate\Benchmark\Command;

use ShopwarePrivate\Benchmark\Services\Benchmarkers\BaseBenchmarker;
use ShopwarePrivate\Benchmark\Services\InstallationBridge;
use ShopwarePrivate\Benchmark\Services\InteractiveTestLoader;
use ShopwarePrivate\Benchmark\Services\ResultHandlers\FileResultHandler;
use ShopwarePrivate\Benchmark\Services\TestManager;
use ShopwarePrivate\Benchmark\TestScenarios\BaseTestScenario;
use ShopwarePrivate\DataGenerator\Command\CreateDataCommand;
use Shopware\Plugin\Services\ConsoleInteraction\TestDefinitionManager;
use ShopwareCli\Command\BaseCommand;
use ShopwareCli\Services\IniHandler;
use ShopwareCli\Services\IoService;
use ShopwareCli\Services\ProcessExecutor;
use Symfony\Bundle\FrameworkBundle\Console\Application;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Input\Input;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\Response;

class RunBenchmarkCommand extends BaseCommand
{
    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('benchmark:run')
            ->setDescription('Run benchmark on Shopware installation')
            ->addArgument(
                'installDir',
                InputArgument::OPTIONAL,
                'Your Shopware installation path.',
                ''
            )
            ->addOption(
                'generate',
                null,
                InputOption::VALUE_NONE,
                'If set, demo data will be generated and added to the Shopware installation'
            )
            ->addOption(
                'enable-es',
                null,
                InputOption::VALUE_NONE,
                'If set, Elasticsearch will be enabled'
            )
            ->addOption(
                'test-file',
                null,
                InputOption::VALUE_REQUIRED,
                'If set, tests will be loaded from the given file path instead'
            )
            ->addOption(
                'benchmarker',
                null,
                InputOption::VALUE_REQUIRED,
                'Specify which benchmarker to use. Allowed values: "siege" (default) or "ab"'
            )
            ->addOption(
                'concurrency',
                null,
                InputOption::VALUE_REQUIRED,
                "Specify how many concurrent requests to use. Defaults to 10"
            )
            ->addOption(
                'tests-per-scenario',
                null,
                InputOption::VALUE_REQUIRED,
                "Specify how many tests (different URLS) to run on each scenario. Defaults to 10"
            )
            ->addOption(
                'hits-per-test',
                null,
                InputOption::VALUE_REQUIRED,
                "Specify how many times a specific URL will be queried. Defaults to 10"
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    public function interact(InputInterface $input, OutputInterface $output)
    {
        /** @var $ioService IoService */
        $ioService = $this->container->get('io_service');

        $this->askInstallationDir($input, $ioService);
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $installDir = $input->getArgument('installDir');
        $generate = $input->getOption('generate');
        $enableES = $input->getOption('enable-es');

        /** @var InstallationBridge $installationBridge */
        $installationBridge = $this->container->get('shopware_benchmark_installation_bridge');

        $installationBridge->setinstallDir($installDir);

        // Sample data generation
        if ($generate) {
            $output->writeln('<info></info>');
            $output->writeln('<info>Sample data generation</info>');
            $output->writeln('<info></info>');
            $generateInput = new ArrayInput(['--installDir' => $installDir]);

            $command = new CreateDataCommand();
            $command->setContainer($this->container);
            $command->run($generateInput, $output);
        }

        // Elasticsearch enable and index
        if ($enableES) {
            $output->writeln('<info></info>');
            $output->writeln('<info>Enabling Elasticsearch</info>');
            $output->writeln('<info></info>');


            $installationBridge->enableElasticSearch($installDir);

            $output->writeln('<info>Elasticsearch was enabled, syncing data. This might take a while...</info>');

            /** @var ProcessExecutor $processExecutor */
            $processExecutor = $this->container->get('process_executor');
            $processExecutor->execute("php bin/console sw:es:index:populate --shopId 1", $installDir, false, 3600);
        }

        // Disable caching and performance features
        $output->writeln('<info>Disabling performance features</info>');
        $installationBridge->disablePerformanceFeatures();

        /** @var BaseBenchmarker $benchmarker */
        $benchmarker = $this->getBenchmarker($input);

        $testFilePath = $input->getOption('test-file');
        if (empty($testFilePath)) {
            $testLoader = $this->container->get('shopware_benchmark_interactive_test_loader');
            $resultHandler = $this->container->get('shopware_benchmark_console_result_handler');
        } else {
            $testLoader = $this->container->get('shopware_benchmark_file_test_loader');
            $testLoader->setDefinitionFilePath($testFilePath);
            /** @var FileResultHandler $resultHandler */
            $resultHandler = $this->container->get('shopware_benchmark_file_result_handler');
            $testFilePathInfo = pathinfo($testFilePath);
            $resultHandler->setResultFileName(
                $testFilePathInfo['dirname'].'/'.$testFilePathInfo['filename'].'_results.'.$testFilePathInfo['extension']
            );
            $resultHandler->setOutputFileName(
                $testFilePathInfo['dirname'].'/'.$testFilePathInfo['filename'].'_output.'.$testFilePathInfo['extension']
            );
        }

        while (true) {
            /** @var BaseTestScenario $testScenario */
            $testScenario = $testLoader->loadTest();

            if (!$testScenario instanceof BaseTestScenario) {
                break;
            }

            $benchmarker->warmUp($testScenario);
            $testScenario = $benchmarker->benchmark($testScenario);

            $resultHandler->handleResult($testScenario);
        }

        $resultHandler->flush();

        $output->writeln('<info>Finished.</info>');
    }

    /**
     * Checks if a given input path is a valid path to install Shopware
     *
     * @param $path
     * @return mixed
     */
    public function validateInstallDir($path)
    {
        if (!is_readable($path.'/shopware.php')) {
            throw new \RuntimeException("Path must be an existing Shopware installation path");
        }

        return $path;
    }

    /**
     * @param InputInterface $input
     * @param IoService $ioService
     *
     * @return string
     */
    private function askInstallationDir(InputInterface $input, IoService $ioService)
    {
        $installDir = $input->getArgument('installDir');
        if (!$installDir) {
            $installDir = $ioService->askAndValidate(
                "Please provide the path to an existing Shopware installation: ",
                array($this, 'validateInstallDir')
            );
            $input->setArgument('installDir', trim($installDir));
        }

        return trim($installDir);
    }

    /**
     * @param Input $input
     * @return BaseBenchmarker
     */
    private function getBenchmarker($input)
    {
        $benchmarkerOption = $input->getOption('benchmarker');

        switch ($benchmarkerOption) {
            case "ab":
                $benchmarker = $this->container->get('shopware_benchmark_apache_bench_benchmarker');
                break;
            case "siege":
            default:
                $benchmarker = $this->container->get('shopware_benchmark_siege_benchmarker');
                break;
        }

        /** @var BaseBenchmarker $benchmarker */
        $benchmarker->setConcurrentRequests($input->getOption('concurrency') ?: 10);
        $benchmarker->setTestsPerScenario($input->getOption('tests-per-scenario') ?: 10);
        $benchmarker->setHitsPerTest($input->getOption('hits-per-test') ?: 10);

        return $benchmarker;
    }
}
