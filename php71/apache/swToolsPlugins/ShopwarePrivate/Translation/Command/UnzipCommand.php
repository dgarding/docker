<?php


namespace ShopwarePrivate\Translation\Command;

use ShopwareCli\Command\BaseCommand;
use ShopwareCli\Utilities;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UnzipCommand extends BaseCommand
{
    const ALL_LANGUAGES = 'all';

    protected function configure()
    {
        $this
            ->setName('translation:unzip')
            ->setDescription('Extract crowdin content over your current shopware installation.')
            ->addArgument(
                'file',
                InputArgument::REQUIRED,
                'Path to your zip file.'
            )
            ->addArgument(
                'installDir',
                InputArgument::REQUIRED,
                'Path to shopware installation.'
            )
            ->addOption(
                'locale',
                'l',
                InputOption::VALUE_REQUIRED,
                'Extract a specific locale. Type "all" if you want to extract all. You need a zip with a specifc language downloaded by crowdin.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Utilities $utilities */
        $utilities = $this->container->get('utilities');
        $crowdinUnzipper = $this->container->get('shopware_private.translation.crowdin_unzipper');
        $shopwareSnippetHandler = $this->container->get('shopware_translation_shopware_snippet_handler');

        $snippetBasket = $crowdinUnzipper->unzipContent($input->getArgument('file'));

        $shopwareSnippetHandler->writeShopwareInstallationSnippets(
            $utilities->getValidShopwarePath($input->getArgument('installDir')),
            $snippetBasket
        );
    }
}