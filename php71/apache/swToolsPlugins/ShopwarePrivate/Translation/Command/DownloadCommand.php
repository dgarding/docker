<?php

namespace ShopwarePrivate\Translation\Command;

use ShopwareCli\Command\BaseCommand;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class DownloadCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('translation:download')
            ->setDescription('Download a specifc locale or download all translations.')
            ->addArgument(
                'locale',
                    InputArgument::OPTIONAL,
                'Download a specific language by locale. "all" will download all languages'
            )
            ->addArgument(
                'destination',
                InputArgument::OPTIONAL,
                'Default is your current working directory',
                getcwd()
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $symfonyStyle = new SymfonyStyle($input, $output);
        $crowdinClient = $this->container->get('shopware_translation_crowdin_client');
        $crowdinConfig = $this->container->get('config')['Crowdin'];

        $locale = $input->getArgument('locale');
        try {
            $locale = LanguageSettings::convertShopwareToCrowdin($locale);
        } catch (\RuntimeException $exception) {
            if ($locale !== 'all') {
                $symfonyStyle->error('Language %s not found. Run translation:mapping to see all available languages.');
            }
        }

        $timestamp = (new \DateTime())->getTimestamp();
        //The locale is necessary to determine the language if you want to extract it.
        $fileName = $locale . '_' . $timestamp . '.zip';

        $crowdinClient->download($input->getArgument('destination') . '/' . $fileName, $locale);

        $symfonyStyle->success('Download ' . $locale . ' from ' . $crowdinConfig['projectId']);
        $output->writeln('Downloaded ' . $locale . ' translations');
        $output->writeln('File: ' . $input->getArgument('destination') . '/' . $fileName);
    }
}