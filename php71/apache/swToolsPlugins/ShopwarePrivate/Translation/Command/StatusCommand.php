<?php


namespace ShopwarePrivate\Translation\Command;


use ShopwareCli\Command\BaseCommand;
use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class StatusCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('translation:status')
            ->setDescription('View the current translation status of all languages');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var CrowdinClient $crowdinClient */
        $crowdinClient = $this->container->get('shopware_translation_crowdin_client');
        $config = $this->container->get('config');

        $status = $crowdinClient->getTranslationStatus();

        $symfonyStyle = new SymfonyStyle($input, $output);
        $symfonyStyle->success('Status for: ' . $config['Crowdin']['projectId']);

        usort($status, function ($a, $b) {
            return $b['translated_progress'] - $a['translated_progress'];
        });

        $table = new Table($output);
        $table
            ->setHeaders([ 'name', 'code', 'phrases', 'translated', 'approved', 'word', 'words translated', 'word approved', 'translated progress', 'approved progress' ])
            ->setRows($status)
            ->render();
    }
}