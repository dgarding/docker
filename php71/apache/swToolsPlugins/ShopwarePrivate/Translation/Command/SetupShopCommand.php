<?php

namespace ShopwarePrivate\Translation\Command;

use ShopwareCli\Command\BaseCommand;
use ShopwarePrivate\Translation\Services\PrepareShop\PrepareShopService;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SetupShopCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('translation:setup')
            ->setDescription('Install language plugins, shopware plugins and creates language sub shops.')
            ->addArgument(
                'installDir',
                InputArgument::OPTIONAL,
                'Path to your shopware installation'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $utilities = $this->container->get('utilities');

        $shopwarePath = realpath($utilities->getValidShopwarePath($input->getArgument('installDir')));

        $pluginNames = array_merge(
            LanguageSettings::getPremiumPluginList(),
            LanguageSettings::getTranslationPluginNames()
        );

        /** @var PrepareShopService $prepareShopService */
        $prepareShopService = $this->container->get('shopware_private.translation.prepare_shop_service');
        $prepareShopService->prepare($shopwarePath, $pluginNames, $output);
    }
}