<?php

namespace ShopwarePrivate\Translation\Command;

use ShopwareCli\Utilities;
use ShopwarePrivate\Translation\Services\PrepareShop\PdoConnectionFactory;
use ShopwarePrivate\Translation\Services\TranslationCheck\Fixer\DoubleQuoteFixer;
use ShopwarePrivate\Translation\Services\TranslationCheck\Fixer\SingleQuoteFixer;
use ShopwarePrivate\Translation\Services\TranslationCheck\DatabaseSnippetFinder;
use ShopwarePrivate\Translation\Services\TranslationCheck\Rules\DoubleQuoteEscapeRule;
use ShopwarePrivate\Translation\Services\TranslationCheck\Rules\SingleQuoteEscapeRule;
use ShopwarePrivate\Translation\Services\TranslationCheck\Rules\SmartyRule;
use ShopwarePrivate\Translation\Services\TranslationCheck\SmartyTranslatedException;
use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationCheckService;
use ShopwareCli\Command\BaseCommand;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

class FixCommand extends BaseCommand
{
    /**
     * @var TranslationCheckService
     */
    private $translationCheckService;

    /**
     * @var SymfonyStyle
     */
    private $symfonyStyle;

    protected function configure()
    {
        $this
            ->setName('translation:fix')
            ->setDescription('Automatically fixes all translations with common issues like escaping.')
            ->addArgument(
                'path',
                InputArgument::REQUIRED,
                'Path in which should be looked for ini files'
            )
            ->addArgument(
                'installDir',
                InputArgument::OPTIONAL,
                'Shopware installation directory'
            );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /** @var Utilities $utilities */
        $utilities = $this->container->get('utilities');
        $shopwarePath = $utilities->getValidShopwarePath($input->getArgument('installDir'));

        $factory = $this->container->get('shopware_private.translation.pdo_connection');
        $connection = $factory->connect($shopwarePath);

        $path = $input->getArgument('path');

        $this->symfonyStyle = new SymfonyStyle($input, $output);
        $this->translationCheckService = new TranslationCheckService(
            new SingleQuoteEscapeRule(new SingleQuoteFixer()),
            new DoubleQuoteEscapeRule(new DoubleQuoteFixer()),
            new SmartyRule(new DatabaseSnippetFinder($connection))
        );

        $smartyErrors = [];
        $finder = new Finder();

        /** @var SplFileInfo $iniFile */
        foreach ($finder->name('*.ini')->in($path) as $iniFile) {
            $this->translationCheckService->check($iniFile);
            if ($this->translationCheckService->getSmartyErrors()) {
                array_push($smartyErrors, ...$this->translationCheckService->getSmartyErrors());
            }

            if ($input->getOption('verbose')) {
                $output->writeln('Fix file: ' . $iniFile->getRealPath());
            }
        }

        if (count($smartyErrors) > 0) {
            $this->buildSmartyErrorTable($smartyErrors, $output, $shopwarePath);
        }
        $this->symfonyStyle->success('No more errors found!');
    }

    /**
     * @param SmartyTranslatedException[] $smartyErrors
     * @param OutputInterface $output
     * @param string $shopwarePath
     */
    private function buildSmartyErrorTable($smartyErrors, $output, $shopwarePath)
    {
        $headers = [ 'path', 'name', 'linenumber', 'source', 'translated' ];

        $rows = [];
        foreach ($smartyErrors as $error) {
            if ($error->getMessage()) {
                continue;
            }

            $rows[] = [
                'path' => substr($error->getPath(), strlen('snippets')),
                'name' => $error->getSnippetName(),
                'linenumber' => $error->getLineNumber(),
                'source' => $error->getSourceSmarty(),
                'translated' => $error->getTranslatedSmarty(),
            ];
        }

        if (count($rows) > 0) {
            $this->symfonyStyle->warning('Found ' . count($rows) . ' potential smarty errors.');
            $table = new Table($output);
            $table
                ->setHeaders($headers)
                ->setRows($rows)
                ->render();
        } else {
            $this->symfonyStyle->success('Found 0 snippet warnings');
        }

        $messages = array_map(function (SmartyTranslatedException $error) {
            return $error->getMessage();
        }, $smartyErrors);

        if (count($messages) > 0) {
            $this->symfonyStyle->warning(
                'Snippets were not found in your local installation:' .
                PHP_EOL .
                '----------------------------------------------------' .
                PHP_EOL . PHP_EOL .
                implode(PHP_EOL, array_filter($messages))
            );
        } else {
            $this->symfonyStyle->success('All snippets were found in your local installation.');
        }

    }
}
