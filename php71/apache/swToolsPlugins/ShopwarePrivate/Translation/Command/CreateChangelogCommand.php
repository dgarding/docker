<?php

namespace ShopwarePrivate\Translation\Command;

use Shopware\Plugin\Services\Checkout;
use Shopware\Plugin\Services\PluginProvider;
use ShopwareCli\Command\BaseCommand;
use ShopwareCli\Services\GitUtil;
use ShopwarePrivate\Translation\Services\Deploy\JsonChangelog;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Process\Process;

class CreateChangelogCommand extends BaseCommand
{
    /**
     * @var PluginProvider
     */
    private $pluginProvider;

    /**
     * @var Checkout
     */
    private $checkoutService;
    /**
     * @var SymfonyStyle
     */
    private $symfonyStyle;

    /**
     * @var GitUtil
     */
    private $gitUtil;

    protected function configure()
    {
        $this
            ->setName('translation:create:changelog')
            ->addArgument(
                'changelog-en',
                InputArgument::REQUIRED,
                'Provide a changelog which should be added.'
            )
            ->addArgument(
                'changelog-de',
                InputArgument::REQUIRED,
                'Provide a german changelog'
            )
            ->addArgument(
                'version',
                InputArgument::REQUIRED,
                'Provide the release version'
            )
            ->addArgument(
                'minVersion',
                InputArgument::OPTIONAL,
                'Provide the min shopware version for the plugins.'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->pluginProvider = $this->container->get('plugin_provider');
        $this->checkoutService = $this->container->get('checkout_service');
        $this->gitUtil= $this->container->get('git_util');
        $this->symfonyStyle = new SymfonyStyle($input, $output);
        $path = '/tmp/translation_plugins' . uniqid();

        $output->writeln('<info>Checkout language plugins</info>');

        @mkdir($path);

        $plugins = array_filter(LanguageSettings::$languageTargetMap, function ($item) {
            return ($item !== 'core');
        });

        $this->checkoutPlugins($plugins, $path);
        $this->symfonyStyle->success('Checked out all plugins successfully.');

        $this->checkoutBranches($plugins, $path);
        $this->symfonyStyle->success('Checked out prepeare-release branches');

        $this->updateChangelog($input, $plugins, $path);
        $this->symfonyStyle->success('Updated all changelogs');

        $this->commitChanges($input, $plugins, $path);
        $this->symfonyStyle->success('Committed all changes');

        $this->pushChanges($plugins, $path);
        $this->symfonyStyle->success('Pushed all changes');
    }

    /**
     * @param array $plugins
     * @param string $path
     */
    protected function checkoutPlugins($plugins, $path)
    {
        foreach ($plugins as $plugin) {
            $plugin = $this->pluginProvider->getPluginByName($plugin)[0];
            $plugin->module = null;
            $this->checkoutService->checkout($plugin, $path);
        }
    }

    /**
     * @param InputInterface $input
     * @param array $plugins
     * @param string $path
     */
    protected function updateChangelog(InputInterface $input, $plugins, $path)
    {
        foreach ($plugins as $plugin) {
            $pluginJsonPath = $path . "/$plugin/plugin.json";
            if (!file_exists($pluginJsonPath)) {
                $this->symfonyStyle->warning("[$plugin] Skipped - no plugin.json found.");
                continue;
            }

            $this->symfonyStyle->writeln("<info>[$plugin]</info> Processing");

            $jsonChangelog = new JsonChangelog(file_get_contents($pluginJsonPath));
            $content = $jsonChangelog
                ->setCurrentVersion($input->getArgument('version'))
                ->addChangelog('de', $input->getArgument('changelog-de'))
                ->addChangelog('en', $input->getArgument('changelog-en'))
                ->setMinimumVersion($input->getArgument('minVersion'))
                ->getContent();

            file_put_contents($pluginJsonPath, $content);

            $this->symfonyStyle->writeln("<info>[$plugin]</info> Updated config" . PHP_EOL);
        }
    }

    /**
     * @return \Closure
     */
    protected function getProcessCallback()
    {
        return function ($type, $buffer) {
            if (Process::ERR === $type) {
                echo 'ERR > ' . $buffer;
            } else {
                echo 'OUT > ' . $buffer;
            }
        };
    }

    /**
     * @param InputInterface $input
     * @param $plugins
     * @param $path
     */
    protected function commitChanges(InputInterface $input, $plugins, $path)
    {
        foreach ($plugins as $plugin) {
            $pluginPath = $path . "/$plugin";
            if (!file_exists($pluginPath . '/plugin.json')) {
                continue;
            }

            $process = new Process('git diff', $pluginPath);
            $process->run($this->getProcessCallback());
            $this->symfonyStyle->confirm('Do you want to commit this change?');

            $process = new Process(
                sprintf("git add plugin.json && git commit -m 'Prepare %s for release.'", $input->getArgument('minVersion')),
                $pluginPath
            );
            $process->run($this->getProcessCallback());

            $this->symfonyStyle->success("[$plugin] Commited changes");
        }
    }

    /**
     * @param $plugins
     * @param $path
     */
    protected function pushChanges($plugins, $path)
    {
        foreach ($plugins as $plugin) {
            $pluginPath = $path . "/$plugin";
            if (!file_exists($pluginPath . '/plugin.json')) {
                continue;
            }
            $this->symfonyStyle->confirm('Do you want to push ' . $plugin);

            $process = new Process('git push origin prepare-release', $pluginPath);
            $process->run($this->getProcessCallback());

            $this->symfonyStyle->success("[$plugin] Pushed to master.");
        }
    }

    /**
     * @param $plugins
     * @param $path
     */
    protected function checkoutBranches($plugins, $path)
    {
        foreach ($plugins as $plugin) {
            $pluginPath = $path . "/$plugin";
            if (!file_exists($pluginPath . '/plugin.json')) {
                $this->symfonyStyle->warning("[$plugin] Skipped - no plugin.json found.");
                continue;
            }

            $process = new Process('git checkout -b prepare-release', $pluginPath);
            $process->run($this->getProcessCallback());
            $this->symfonyStyle->writeln("<info>[$plugin]</info> Checkout prepare-release branch");
        }
    }
}