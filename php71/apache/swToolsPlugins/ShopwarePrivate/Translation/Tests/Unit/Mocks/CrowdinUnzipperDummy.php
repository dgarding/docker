<?php

namespace ShopwarePrivate\Translation\Tests\Unit\Mocks;

use ShopwarePrivate\Translation\Services\Crowdin\CrowdinUnzipper;

class CrowdinUnzipperDummy extends CrowdinUnzipper
{
    public function __construct()
    {
    }

    public function unzipContent($zipFilePath)
    {
    }
}