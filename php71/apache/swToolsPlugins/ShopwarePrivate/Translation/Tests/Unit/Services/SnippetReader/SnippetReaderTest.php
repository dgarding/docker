<?php

namespace ShopwarePrivate\Tests\Unit\Translation\Services\SnippetReader;

use ShopwarePrivate\Translation\Services\SnippetReader\SnippetReader;

class SnippetReaderTest extends \PHPUnit_Framework_TestCase
{
    public function test_it_can_be_created()
    {
        $snippetReader = new SnippetReader();
        $this->assertInstanceOf(SnippetReader::class, $snippetReader);
    }

    public function test_it_should_return_empty_SnippetBasket_if_no_ini_files_were_found()
    {
        $snippetReader = new SnippetReader();

        $snippetBasket = $snippetReader->readSnippets(__DIR__ . '/_fixtures/empty');

        $this->assertCount(0, $snippetBasket->getAll());
    }

    /**
     * @expectedException \RuntimeException
     * @expectedExceptionMessage Syntax error in
     */
    public function test_it_should_throw_exception_if_snippet_file_has_syntax_error()
    {
        $snippetReader = new SnippetReader();

        $snippetReader->readSnippets(__DIR__ . '/_fixtures/syntax_error');
    }

    public function test_it_should_ignore_empty_ini_files()
    {
        $snippetReader = new SnippetReader();

        $snippetBasket = $snippetReader->readSnippets(__DIR__ . '/_fixtures/empty_ini');

        $this->assertCount(0, $snippetBasket->getAll());
    }

    public function test_it_should_read_snippets()
    {
        $snippetReader = new SnippetReader();

        $snippetBasket = $snippetReader->readSnippets(__DIR__ . '/_fixtures/example');

        $this->assertCount(1, $snippetBasket->getAll());
        $this->assertEquals('main.ini', $snippetBasket->getAll()[0]->fileName);
        $this->assertEquals('main', $snippetBasket->getAll()[0]->namespace);
        $this->assertEquals(null, $snippetBasket->getAll()[0]->context);
        $this->assertEquals('Translation', $snippetBasket->getAll()[0]->data['en_GB']['test']);
        $this->assertEquals('Translation german', $snippetBasket->getAll()[0]->data['de_DE']['test']);
    }

    public function test_it_should_detect_snippet_namespaces()
    {
        $snippetReader = new SnippetReader();

        $snippetBasket = $snippetReader->readSnippets(__DIR__ . '/_fixtures/namespace');

        $this->assertCount(2, $snippetBasket->getAll());
        $this->assertEquals('another_namespace/main', $snippetBasket->getAll()[0]->namespace);
        $this->assertEquals('test/another', $snippetBasket->getAll()[1]->namespace);
    }

    public function test_it_should_merge_default_snippet_group_with_enGB()
    {
        $snippetReader = new SnippetReader();

        $snippetBasket = $snippetReader->readSnippets(__DIR__ . '/_fixtures/default');

        $this->assertEquals('Translation', $snippetBasket->getAll()[0]->data['en_GB']['test']);
        $this->assertArrayNotHasKey('default', $snippetBasket->getAll()[0]->data);
    }

    public function test_it_should_merge_default_snippet_group_with_typo()
    {
        $snippetReader = new SnippetReader();

        $snippetBasket = $snippetReader->readSnippets(__DIR__ . '/_fixtures/default');

        $this->assertEquals('Translation default typo', $snippetBasket->getAll()[0]->data['_']['test_typo']);
        $this->assertArrayNotHasKey('default', $snippetBasket->getAll()[0]->data);
    }

    public function test_it_should_not_remove_escaping()
    {
        $snippetReader = new SnippetReader();

        $snippetBasket = $snippetReader->readSnippets(__DIR__ . '/_fixtures/escaped_snippet');

        $this->assertEquals('test \" test', $snippetBasket->getAll()[0]->data['de_DE']['test']);
    }
}