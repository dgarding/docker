<?php

namespace ShopwarePrivate\Tests\Unit\Translation\Services\Crowdin;

use ShopwarePrivate\Translation\Services\Crowdin\CrowdinDirectoryHandler;
use ShopwarePrivate\Translation\Tests\Unit\Mocks\CrowdinClientDummy;
use ShopwarePrivate\Translation\Tests\Unit\Mocks\IoServiceDummy;

class CrowdinDirectoryHandlerTest extends \PHPUnit_Framework_TestCase
{
    public function test_it_can_be_created()
    {
        $crowdinDirectoryHandler = new CrowdinDirectoryHandler(
            new IoServiceDummy(),
            new CrowdinClientDummy()
        );

        $this->assertInstanceOf(CrowdinDirectoryHandler::class, $crowdinDirectoryHandler);
    }
}