<?php

namespace ShopwarePrivate\Translations\Tests\Unit\Services\Deploy\Changelog;

use ShopwarePrivate\Translation\Services\Deploy\JsonChangelog;

class JsonChangelogTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var JsonChangelog
     */
    private $service;

    public function test_it_should_be_created()
    {
        $this->assertInstanceOf(JsonChangelog::class, $this->service);
    }

    public function test_setCurrentVersion_should_update_version_in_config_file()
    {
        $result = $this->service
            ->setCurrentVersion('2.0.0')
            ->getDecodedContent();

        $this->assertEquals('2.0.0', $result['currentVersion']);
    }

    public function test_addChangelog_should_update_changelog()
    {
        $result = $this->service
            ->setCurrentVersion('2.0.0')
            ->addChangelog('de', 'Changelog in German')
            ->addChangelog('en', 'Changelog in English')
            ->getDecodedContent();

        $this->assertEquals('Changelog in German', $result['changelog']['de']['2.0.0']);
        $this->assertEquals('Changelog in English', $result['changelog']['en']['2.0.0']);
    }

    public function test_addChangelog_should_throw_exception_if_version_was_not_updated_before()
    {
        $this->setExpectedException(\RuntimeException::class);
        $this->service->addChangelog('de', 'some');
    }

    public function test_setMinimumVersion()
    {
        $result = $this->service
            ->setMinimumVersion('6.0.0')
            ->getDecodedContent();

        $this->assertEquals('6.0.0', $result['compatibility']['minimumVersion']);
    }

    public function test_setMaximumVersion()
    {
        $result = $this->service
            ->setMaximumVersion('6.0.0')
            ->getDecodedContent();

        $this->assertEquals('6.0.0', $result['compatibility']['maximumVersion']);
    }

    public function test_printToFile_should_print_pretty_json()
    {
        $content = $this->service
            ->setCurrentVersion('2.0.0')
            ->addChangelog('de', 'German changelog')
            ->addChangelog('en', 'English changelog')
            ->setMinimumVersion('5.3.0')
            ->setMaximumVersion('6.0.0')
            ->getContent();

        $this->assertEquals(file_get_contents(__DIR__ . '/_fixtures/pretty_content.json'), $content);
    }

    /**
     * @before
     */
    protected function createServiceBefore()
    {
        $this->service = new JsonChangelog(file_get_contents(__DIR__ . '/_fixtures/plugin.json'));
    }
}