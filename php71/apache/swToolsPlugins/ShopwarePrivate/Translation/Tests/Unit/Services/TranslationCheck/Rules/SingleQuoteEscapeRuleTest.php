<?php

namespace ShopwarePrivate\Tests\Unit\Services\TranslationCheck\Rules;

use ShopwarePrivate\Translation\Services\TranslationCheck\Fixer\FixerInterface;
use ShopwarePrivate\Translation\Services\TranslationCheck\Rules\SingleQuoteEscapeRule;
use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationLine;
use Symfony\Component\Finder\SplFileInfo;

class SingleQuoteEscapeRuleTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @expectedException \ShopwarePrivate\Translation\Services\TranslationCheck\TranslationException
     * @expectedExceptionMessage Unescaped ' detected
     */
    public function test_it_should_throw_exception_if_backend_snippet_with_unescaped_single_quote_were_detected()
    {
        $fileInfo = new SplFileInfo(
            __DIR__ . '/_fixtures/backend/test.ini',
            '_fixtures/backend/test.ini',
            '_fixtures/backend'
        );

        $translationLine = new TranslationLine();
        $translationLine->file = $fileInfo;
        $translationLine->lineNumber = 1;

        $translationLine->content = <<<EOD
test_snippet = "Test ' escape"
EOD;

        $singleQuoteRule = new SingleQuoteEscapeRule(new FixerDummy());
        $singleQuoteRule->check($translationLine);
    }
}

class FixerDummy implements FixerInterface
{
    public function fix(TranslationLine $translationLine)
    {
    }
}