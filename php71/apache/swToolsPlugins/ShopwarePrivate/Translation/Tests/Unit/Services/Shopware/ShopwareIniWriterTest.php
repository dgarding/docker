<?php

namespace ShopwarePrivate\Tests\Unit\Translation\Services\Shopware;

use Piwik\Ini\IniWriter;
use ShopwarePrivate\Translation\Services\Shopware\ShopwareIniWriter;
use Symfony\Component\Process\Process;

class ShopwareIniWriterTest extends \PHPUnit_Framework_TestCase
{
    public static $writeToStringCount = 0;

    protected function setUp()
    {
        self::$writeToStringCount = 0;
    }

    protected function tearDown()
    {
        $process = new Process('rm -r ' . __DIR__ . '/_fixtures/IniWriter/not_existing');
        $process->run();
    }


    /**
     * @return string
     */
    private function getSnippetFolderPath()
    {
        return __DIR__ . '/_fixtures/IniWriter';
    }

    public function test_it_can_be_create()
    {
        $shopwareIniWriter = new ShopwareIniWriter(new IniWriterDummy());
        $this->assertInstanceOf(ShopwareIniWriter::class, $shopwareIniWriter);
    }

    public function test_it_should_throw_exception_if_file_does_not_exists()
    {
        $this->assertFileNotExists(__DIR__ . '/_fixtures/IniWriter/does_not_exist.ini');

        $snippetsDataArray = [
            'namespace/does/not/exist/does_not_exist.ini' => []
        ];
        $snippetFolderPath = $this->getSnippetFolderPath();

        $shopwareIniWriter = new ShopwareIniWriter(new IniWriterDummy());

        $this->setExpectedException(\RuntimeException::class);
        $shopwareIniWriter->writeSnippets($snippetFolderPath, $snippetsDataArray, false);
    }

    public function test_it_should_write_ini_file()
    {
        $snippetsDataArray = [
            'example.ini' => [
                'en_GB' => [
                    'test' => 'Test translation crowdin'
                ],
                'de_DE' => [
                    'test' => 'Test Übersetzung Crowdin'
                ]
            ]
        ];

        $shopwareIniWriter = new ShopwareIniWriter(new IniWriterMockReturnsSnippetFileContent());

        $shopwareIniWriter->writeSnippets($this->getSnippetFolderPath(), $snippetsDataArray, false);

        $this->assertEquals(1, self::$writeToStringCount);

        $iniContent = parse_ini_file($this->getSnippetFolderPath() . '/example.ini');
        $this->assertEquals([ 'test' => 'test snippet content' ], $iniContent);
    }

    public function test_it_should_remove_empty_line_at_end_of_file()
    {
        $snippetsDataArray = [
            'ini_with_empty_line.ini' => []
        ];

        $shopwareIniWriter = new ShopwareIniWriter(new IniWriterMockReturnsSnippetFileContent());
        $shopwareIniWriter->writeSnippets($this->getSnippetFolderPath(), $snippetsDataArray, false);

        $content = file_get_contents(__DIR__ . '/_fixtures/IniWriter/ini_with_empty_line.ini');
        $this->assertEquals('test = "test snippet content"', $content);
    }

    public function test_it_should_ignore_blanks_lines_which_are_not_at_the_end_of_file()
    {
        $snippetsData = [
            'ini_with_empty_line.ini' => []
        ];

        $shopwareIniWriter = new ShopwareIniWriter(new IniWriterMockReturnsIniContentWithMultipleLines());
        $shopwareIniWriter->writeSnippets($this->getSnippetFolderPath(), $snippetsData, false);

        $expected = <<<EOD
test = "test"
test_snippet = "test_snippet"
EOD;

        $content = file_get_contents(__DIR__ . '/_fixtures/IniWriter/ini_with_empty_line.ini');
        $this->assertEquals($expected, $content);
    }

    public function test_it_should_create_directories_if_path_does_not_exists()
    {
        $snippetData = [
            'not_existing.ini' => []
        ];

        $shopwareIniWriter = new ShopwareIniWriter(new IniWriterMockReturnsIniContentWithMultipleLines());
        $shopwareIniWriter->writeSnippets(
            $this->getSnippetFolderPath() . '/not_existing/dir',
                $snippetData,
            true
        );

        $this->assertFileExists(__DIR__ . '/_fixtures/IniWriter/not_existing/dir/not_existing.ini');
    }
}

class IniWriterDummy extends IniWriter
{
    public function writeToFile($filename, array $config, $header = '')
    {
        //nth
    }

    public function writeToString(array $config, $header = '')
    {
        //nth
    }
}

class IniWriterStub extends IniWriter
{
    public function writeToFile($filename, array $config, $header = '')
    {
        //nth
    }

    public function writeToString(array $config, $header = '')
    {
        ShopwareIniWriterTest::$writeToStringCount++;
    }
}

class IniWriterMockReturnsSnippetFileContent extends IniWriter
{
    public function writeToFile($filename, array $config, $header = '')
    {
        //nth
    }

    public function writeToString(array $config, $header = '')
    {
        ShopwareIniWriterTest::$writeToStringCount++;
        return <<<EOD
test = "test snippet content"\n
EOD;

    }
}

class IniWriterMockReturnsIniContentWithMultipleLines extends IniWriter
{
    public function writeToFile($filename, array $config, $header = '')
    {
        //nth
    }

    public function writeToString(array $config, $header = '')
    {
        ShopwareIniWriterTest::$writeToStringCount++;
        return <<<EOD
test = "test"\ntest_snippet = "test_snippet"\n
EOD;

    }
}