<?php

namespace ShopwarePrivate\Translation\Tests\Unit\Services\TranslationCheck\Fixer;

use ShopwarePrivate\Translation\Services\TranslationCheck\Fixer\DoubleQuoteFixer;
use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationLine;
use Symfony\Component\Finder\SplFileInfo;

class DoubleQuoteFixerTest extends \PHPUnit_Framework_TestCase
{
    public function test_it_can_be_created()
    {
        $doubleQuoteFixer = $this->createDoubleQuoteFixer();
        $this->assertInstanceOf(DoubleQuoteFixer::class, $doubleQuoteFixer);
    }

    public function test_it_should_escape_a_double_quote()
    {
        $content = <<<EOD
test = "some "content"
EOD;

        $translationLine = TranslationLine::create(1, $content, new SplFileInfoDummy());

        $doubleQuoteFixer = $this->createDoubleQuoteFixer();
        $result = $doubleQuoteFixer->fix($translationLine);

        $expectedResult = <<<EOD
test = "some \"content"
EOD;

        $this->assertEquals($expectedResult, $result);
    }

    public function test_it_should_not_replace_already_escaped_double_quotes()
    {
        $content = <<<EOD
test = "some \" content"
EOD;

        $translationLine = TranslationLine::create(1, $content, new SplFileInfoDummy());

        $doubleQuoteFixer = $this->createDoubleQuoteFixer();
        $result = $doubleQuoteFixer->fix($translationLine);

        $expected = <<<EOD
test = "some \" content"
EOD;

        $this->assertEquals($expected, $result);
    }

    public function test_it_should_escape_multiple_double_quotes()
    {
        $content = <<<EOD
test = "some "content with " more " multiple"
EOD;

        $translationLine = TranslationLine::create(1, $content, new SplFileInfoDummy());

        $doubleQuoteFixer = $this->createDoubleQuoteFixer();
        $result = $doubleQuoteFixer->fix($translationLine);

        $expectedResult = <<<EOD
test = "some \"content with \" more \" multiple"
EOD;

        $this->assertEquals($expectedResult, $result);
    }

    public function test_it_should_apply_fixes_without_escaping_already_escaped_quotes()
    {
        $content = <<<EOD
test = "some \"content with " more " multiple"
EOD;

        $translationLine = TranslationLine::create(1, $content, new SplFileInfoDummy());

        $doubleQuoteFixer = $this->createDoubleQuoteFixer();
        $result = $doubleQuoteFixer->fix($translationLine);

        $expectedResult = <<<EOD
test = "some \"content with \" more \" multiple"
EOD;

        $this->assertEquals($expectedResult, $result);
    }

    /**
     * @return DoubleQuoteFixer
     */
    protected function createDoubleQuoteFixer()
    {
        return new DoubleQuoteFixer();
    }
}

class SplFileInfoDummy extends SplFileInfo
{
    public function __construct()
    {
    }
}