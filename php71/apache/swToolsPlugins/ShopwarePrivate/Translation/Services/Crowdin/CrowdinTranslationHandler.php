<?php

namespace ShopwarePrivate\Translation\Services\Crowdin;

use Closure;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ServerException;
use Piwik\Ini\IniWriter;
use Psr\Http\Message\ResponseInterface;
use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;
use ShopwarePrivate\Translation\Services\CrowdinApi\RequestGenerator\UploadTranslationGenerator;
use ShopwarePrivate\Translation\Services\Data\SnippetBasket;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;
use ShopwareCli\Config;
use ShopwareCli\Services\IoService;
use Symfony\Component\Console\Helper\ProgressBar;

class CrowdinTranslationHandler
{
    /**
     * @var Config
     */
    private $config;

    /**
     * @var CrowdinClient
     */
    private $crowdinClient;

    /**
     * @var IoService
     */
    private $output;

    /**
     * @var ProgressBar
     */
    private $progressBar;

    /**
     * @var IniWriter
     */
    private $iniWriter;

    /**
     * @var CrowdinUnzipper
     */
    private $crowdinUnzipper;

    /**
     * @param Config $config
     * @param IoService $output
     * @param CrowdinClient $crowdinClient
     * @param CrowdinUnzipper $crowdinUnzipper
     */
    public function __construct(
        Config $config,
        IoService $output,
        CrowdinClient $crowdinClient,
        CrowdinUnzipper $crowdinUnzipper
    ) {
        $this->crowdinClient = $crowdinClient;
        $this->output = $output;
        $this->iniWriter = new IniWriter();
        $this->config = $config;
        $this->crowdinUnzipper = $crowdinUnzipper;
    }

    /**
     * Downloads translation from Crowdin for the given language (use "all" for all languages)
     *
     * @return SnippetBasket
     */
    public function downloadTranslations()
    {
        $this->output->writeln('<info>Downloading translations...</info>');
        $tempFilePath = tempnam("/tmp", "sw-cli");

        $this->crowdinClient->download($tempFilePath);

        $this->output->writeln('<info>Translations file downloaded</info>');

        return $this->unzipContent($tempFilePath);
    }

    /**
     * Requests Crowdin to regenerate the translation files
     * This is required prior to the translation download
     */
    public function exportTranslations()
    {
        $this->output->writeln('<info>Exporting translations...</info>');

        $response = $this->crowdinClient->export();

        $responseContent = json_decode($response->getBody()->getContents());
        $buildStatus = $responseContent->success->status;

        $this->output->writeln('<info>Translations exported. Status: '.$buildStatus.'</info>');
    }

    /**
     * Uploads translations to Crowdin
     *
     * @param SnippetBasket $snippetBasket
     * @throws \Piwik\Ini\IniWritingException
     */
    public function uploadTranslations(SnippetBasket $snippetBasket)
    {
        $languages = $snippetBasket->getLanguages();

        foreach ($languages as $sourceLanguage) {
            $this->output->writeln('<info>Uploading translations for "'.$sourceLanguage.'"...</info>');

            $targetLanguage = LanguageSettings::convertShopwareToCrowdin($sourceLanguage);

            $this->progressBar = $this->output->createProgressBar(count($snippetBasket->getAll()));
            $this->progressBar->start();

            $this->crowdinClient->uploadTranslation(
                $this->createUploadTranslationGenerator($snippetBasket, $sourceLanguage, $targetLanguage),
                $this->createFulfilledUpdateCallback(),
                $this->createRejectedUpdateCallback()
            );

            $this->progressBar->finish();
            $this->output->writeln('');
        }
        $this->output->writeln('<info>Translations uploaded</info>');
    }

    /**
     * @return Closure
     */
    private function createFulfilledUpdateCallback()
    {
        return function (ResponseInterface $response, $index) {
            $responseContent = json_decode($response->getBody()->getContents());
            $files = get_object_vars($responseContent->files);

            if ($this->output->isVerbose()) {
                $this->output->writeln('');
                foreach ($files as $name => $result) {
                    $this->output->writeln(
                        '<info>File update: file '.$name.' was '.$result.'</info>'
                    );
                }
            }
            $this->progressBar->advance();
        };
    }

    /**
     * @return Closure
     */
    private function createRejectedUpdateCallback()
    {
        return function ($reason, $index) {
            if ($reason instanceof ClientException) {
                $responseContent = json_decode($reason->getResponse()->getBody()->getContents(), true);

                switch ($responseContent['error']['code']) {
                    case 5:
                        $this->output->writeln('<comment>File already exists</comment>');
                        break;
                    case 4:
                        $this->output->writeln('<comment>No files specified in request</comment>');
                        break;
                    case 8:
                        $this->output->writeln('<comment>Specified translation file not found</comment>');
                        break;
                    case 17:
                        $this->output->writeln('<comment>Specified namespace not found</comment>');
                        break;
                    case 13:
                        break;
                    case 21:
                        // No file included in the request
                        $this->output->writeln(
                            '<comment>Specified value for parameter "files" is not valid. Array expected</comment>'
                        );
                        break;
                    default:
                        throw new \RuntimeException(
                            'Could not upload translation files due to client error: '
                            . $responseContent['error']['code'] . ' - '
                            . $responseContent['error']['message']
                        );
                }

                $this->progressBar->advance();
                return;
            }

            if ($reason instanceof ServerException) {
                /** @var ResponseInterface $response */
                $response = $reason->getResponse();

                $this->output->writeln(
                    '<comment>Could not upload translation files due to server error: '
                    . $response->getStatusCode() . ' - '
                    . $response->getReasonPhrase()
                    . '</comment>'
                );
                return;
            }

            throw $reason;
        };
    }

    /**
     * @param SnippetBasket $snippetBasket
     * @param $sourceLanguage
     * @param $targetLanguage
     * @return UploadTranslationGenerator
     */
    private function createUploadTranslationGenerator(SnippetBasket $snippetBasket, $sourceLanguage, $targetLanguage)
    {
        return new UploadTranslationGenerator(
            $snippetBasket,
            $sourceLanguage,
            $targetLanguage,
            $this->config,
            $this->iniWriter,
            $this->progressBar,
            $this->output
        );
    }

    /**
     * Unpacks downloaded zip content into SnippetFile structs.
     *
     * @param string $snippetArchivePath
     * @return SnippetBasket
     */
    public function unzipContent($snippetArchivePath)
    {
        return $this->crowdinUnzipper->unzipContent($snippetArchivePath);
    }
}
