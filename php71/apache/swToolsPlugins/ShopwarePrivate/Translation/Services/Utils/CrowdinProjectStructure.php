<?php

namespace ShopwarePrivate\Translation\Services\Utils;

class CrowdinProjectStructure
{
    private $rawProjectStructure;

    public function __construct($rawProjectStructure)
    {
        $this->rawProjectStructure = $rawProjectStructure;
    }

    /**
     * Crawls current project data, returning a list of files that exist on the server.
     *
     * @param array $projectStructure
     * @param string $namespace
     * @return array
     */
    public function getFileList($projectStructure = null, $namespace = '')
    {
        $projectStructure = $projectStructure ?: $this->rawProjectStructure;

        $fileList = [];

        foreach ($projectStructure as $file) {
            $newPath = trim($namespace.'/'.$file->name, '/');
            if ($file->node_type == 'file') {
                $fileList[] = $newPath;
            } elseif ($file->files) {
                $fileList = array_merge($this->getFileList($file->files, $newPath), $fileList);
            }
        }

        return $fileList;
    }

    /**
     * Crawls current project data, returning a list of directories that exist on the server.
     *
     * @param array $projectStructure
     * @param string $namespace
     * @return array
     */
    public function getDirectoryList($projectStructure = null, $namespace = '')
    {
        $projectStructure = $projectStructure ?: $this->rawProjectStructure;

        $folderList = [];

        foreach ($projectStructure as $file) {
            if ($file->node_type == 'file') {
                continue;
            }
            $newPath = trim($namespace.'/'.$file->name, '/');

            $folderList[] = $newPath;
            if (!empty($file->files)) {
                $folderList = array_merge($this->getDirectoryList($file->files, $newPath), $folderList);
            }
        }

        return $folderList;
    }

    /**
     * Crawls current project data, returning a list of directories that exist on the server.
     *
     * @param array $projectStructure
     * @param string $namespace
     * @return array
     */
    public function getEmptyFolderList($projectStructure = null, $namespace = '')
    {
        $projectStructure = $projectStructure ?: $this->rawProjectStructure;
        $emptyFolderList = [];

        foreach ($projectStructure as $file) {
            if ($file->node_type == 'file') {
                continue;
            }
            $newPath = trim($namespace.'/'.$file->name, '/');

            if ($this->isEmptyDir($file)) {
                $emptyFolderList[] = $newPath;
            } else {
                $emptyFolderList = array_merge($this->getEmptyFolderList($file->files, $newPath), $emptyFolderList);
            }
        }

        return $emptyFolderList;
    }

    /**
     * Recursively detects if a folder has files
     *
     * @param $fileNode
     * @return bool
     */
    private function isEmptyDir($fileNode)
    {
        if ($fileNode->node_type == 'file') {
            return false;
        }

        if (!empty($fileNode->files)) {
            foreach ($fileNode->files as $subFileNode) {
                if (!$this->isEmptyDir($subFileNode)) {
                    return false;
                }
            }
        }

        return true;
    }
}