<?php

namespace ShopwarePrivate\Translation\Services\Deploy;

class JsonChangelog
{
    /**
     * @var array
     */
    private $json;

    /**
     * @var bool
     */
    private $versionUpdated = false;

    /**
     * @param string $jsonContent
     */
    public function __construct($jsonContent)
    {
        $this->json = json_decode($jsonContent, true);
        if (json_last_error() != JSON_ERROR_NONE) {
            throw new \RuntimeException('Invalid json string.' . PHP_EOL . $jsonContent);
        }
    }

    /**
     * @param string $version
     * @return $this
     */
    public function setCurrentVersion($version)
    {
        $this->json['currentVersion'] = $version;
        $this->versionUpdated = true;
        return $this;
    }

    /**
     * @return array
     */
    public function getDecodedContent()
    {
        return $this->json;
    }

    /**
     * @param string $language
     * @param string $changelog
     * @return $this
     */
    public function addChangelog($language, $changelog)
    {
        if (!$this->versionUpdated) {
            throw new \RuntimeException('Current version was not updated before, please make sure you increase the current version.');
        }

        $this->json['changelog'][$language][$this->json['currentVersion']] = $changelog;
        krsort($this->json['changelog'][$language]);
        return $this;
    }

    /**
     * @param string $minVersion
     * @return $this
     */
    public function setMinimumVersion($minVersion)
    {
        if (strlen($minVersion) === 0) {
            return $this;
        }

        if (version_compare($minVersion, $this->json['compatibility']['minimumVersion'], '<')) {
            throw new \RuntimeException(
                'New min version is lower than the actual min version.' .
                ' NEW: ' . $minVersion .
                ' OLD: ' . $this->json['compatibility']['minimumVersion']
            );
        }

        $this->json['compatibility']['minimumVersion'] = $minVersion;
        return $this;
    }

    /**
     * @param string $maxVersion
     * @return $this
     */
    public function setMaximumVersion($maxVersion)
    {
        $this->json['compatibility']['maximumVersion'] = $maxVersion;
        return $this;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return json_encode($this->json, JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE );
    }
}