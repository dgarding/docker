<?php

namespace ShopwarePrivate\Translation\Services\Shopware;

use DirectoryIterator;
use SplFileInfo;

class PluginMapCreator
{
    /**
     * Returns a map of plugin names to their pathes, i.e.:
     * [
     *  'SwagTest' => '/home/test/www/shopware/custom/plugins/SwagTest/Snippets/',
     *  'SwagLegacy' => '/home/test/www/shopware/engine/Shopware/Plugins/Local/Frontend/SwagLegacy/Resources/snippets',
     * ]
     *
     * @param string $shopwarePath
     * @return array
     */
    public function create($shopwarePath)
    {
        $legacyPluginPathMap = $this->readLegacyPluginSystem($shopwarePath);

        $newPluginSystemMap = $this->generatePluginMap("{$shopwarePath}/custom/plugins", function (SplFileInfo $pluginDir) {
            return realpath($pluginDir->getRealPath()."/Resources/snippets");
        });

        return array_merge($legacyPluginPathMap, $newPluginSystemMap);
    }

    /**
     * @param string $shopwarePath
     * @return array
     */
    protected function readLegacyPluginSystem($shopwarePath)
    {
        $result = [];
        foreach (['Default', 'Local'] as $context) {
            foreach (['Frontend', 'Core', 'Backend'] as $module) {
                $pluginPath = "{$shopwarePath}/engine/Shopware/Plugins/{$context}/{$module}";

                $tmpPluginPathMap = $this->generatePluginMap($pluginPath, function (SplFileInfo $pluginDir) {
                    return realpath($pluginDir->getRealPath() . '/Snippets') ?: realpath(
                        $pluginDir->getRealPath() . '/snippets'
                    );
                });
                $result = array_merge($result, $tmpPluginPathMap);
            }
        }
        return $result;
    }

    /**
     * @param string $pluginPath - Path to the folder where it is located
     * @param callable $snippetPathValidator
     * @return array
     */
    private function generatePluginMap($pluginPath, callable $snippetPathValidator)
    {
        $result = [];
        $iterator = new DirectoryIterator($pluginPath);

        foreach ($iterator as $entry) {
            $pluginSnippetPath = call_user_func($snippetPathValidator, $entry);
            if (empty($pluginSnippetPath)) {
                continue;
            }

            $result[$entry->getFilename()] = $pluginSnippetPath;
        }

        return $result;
    }
}