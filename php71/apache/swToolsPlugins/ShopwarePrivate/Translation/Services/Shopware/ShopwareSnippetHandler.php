<?php

namespace ShopwarePrivate\Translation\Services\Shopware;

use Shopware\Plugin\Services\Install;
use Shopware\Plugin\Services\PluginProvider;
use ShopwarePrivate\Translation\Services\Data\SnippetBasket;
use ShopwarePrivate\Translation\Services\SnippetReader\ShopwareSnippetReader;
use ShopwarePrivate\Translation\Services\SnippetReader\SnippetReader;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;
use ShopwareCli\Services\IoService;
use ShopwareCli\Utilities;

class ShopwareSnippetHandler
{
    /**
     * @var IoService
     */
    private $output;

    /**
     * @var PluginProvider
     */
    private $pluginProvider;

    /**
     * @var \ShopwareCli\Utilities
     */
    private $utilities;

    /**
     * @var Install
     */
    private $installService;

    /**
     * @var ShopwareIniWriter
     */
    private $shopwareIniWriter;

    /**
     * @var SnippetReader
     */
    private $snippetReader;
    /**
     * @var SnippetWritingExtractor
     */
    private $snippetWritingExtractor;

    /**
     * @param IoService $output
     * @param PluginProvider $pluginProvider
     * @param Utilities $utilities
     * @param Install $installService
     * @param ShopwareIniWriter $shopwareIniWriter
     * @param SnippetReader $snippetReader
     * @param SnippetWritingExtractor $snippetWritingExtractor
     */
    public function __construct(
        IoService $output,
        PluginProvider $pluginProvider,
        Utilities $utilities,
        Install $installService,
        ShopwareIniWriter $shopwareIniWriter,
        SnippetReader $snippetReader,
        SnippetWritingExtractor $snippetWritingExtractor
    ) {
        $this->output = $output;
        $this->pluginProvider = $pluginProvider;
        $this->utilities = $utilities;
        $this->installService = $installService;
        $this->shopwareIniWriter = $shopwareIniWriter;
        $this->snippetReader = $snippetReader;
        $this->snippetWritingExtractor = $snippetWritingExtractor;
    }

    /**
     * Writes given snippet data into matching .ini files in the given Shopware path
     * If $writeToFilePath is true, it will write the data to the original file (core usage)
     * Otherwise writes it to the specified path (translation plugin usage)
     *
     * @param string $shopwarePath
     * @param SnippetBasket $snippetBasket
     */
    public function writeShopwareInstallationSnippets($shopwarePath, SnippetBasket $snippetBasket)
    {
        $this->writeCoreSnippets($shopwarePath, $snippetBasket);
        $this->writePluginSnippets($shopwarePath, $snippetBasket);
        $this->writeTranslationPluginSnippets($shopwarePath, $snippetBasket);
    }

    /**
     * Prepares and writes Shopware core snippets
     *
     * @param string $shopwarePath
     * @param SnippetBasket $snippetBasket
     */
    private function writeCoreSnippets($shopwarePath, SnippetBasket $snippetBasket)
    {
        $contextPathCount = count($snippetBasket->getContextPaths());

        $this->output->writeln(
            '<info>Updating '.$contextPathCount.' core snippet namespaces</info>'
        );

        $coreLanguages = LanguageSettings::getCoreLanguages();

        $snippetDataArray = $this->extractForWriting($snippetBasket, $coreLanguages, 'core');

        $this->shopwareIniWriter->writeSnippets($shopwarePath . '/snippets', $snippetDataArray);

        $this->output->writeln(
            '<info>Shopware core snippets updated correctly</info>'
        );
    }

    /**
     * Prepares and writes Shopware plugin (core+premium) snippets
     *
     * @param string $shopwarePath
     * @param SnippetBasket $snippetBasket
     */
    private function writePluginSnippets($shopwarePath, SnippetBasket $snippetBasket)
    {
        $contextPathCount = count($snippetBasket->getContextPaths());

        $this->output->writeln(
            '<info>Updating '.$contextPathCount.' plugin snippet namespaces</info>'
        );

        $coreLanguages = LanguageSettings::getCoreLanguages();

        $pluginMap = $this->getPluginMap($shopwarePath);

        $pluginContexts = $snippetBasket->getContexts();
        if (($key = array_search('core', $pluginContexts)) !== false) {
            unset($pluginContexts[$key]);
        }

        foreach ($pluginContexts as $context) {

            $this->output->writeln(
                '<info>Updating '.$context.'...</info>'
            );

            $snippetDataArray = $this->extractForWriting($snippetBasket, $coreLanguages, $context);

            if (!isset($pluginMap[$context])) {
                $this->output->writeln('<error>' . $context . ' is not available in LanguageSettings configuration. Skipped.</error>');
                continue;
            }
            $this->shopwareIniWriter->writeSnippets($pluginMap[$context], $snippetDataArray);
        }

        $this->output->writeln(
            '<info>Shopware plugin snippets updated correctly</info>'
        );
    }

    /**
     * Prepares and writes translation plugin snippets
     *
     * @param $shopwarePath
     * @param SnippetBasket $snippetBasket
     */
    private function writeTranslationPluginSnippets($shopwarePath, SnippetBasket $snippetBasket)
    {
        $pluginLanguages = LanguageSettings::getPluginTargetMap();

        $this->output->writeln(
            '<info>Updating translation plugin snippets</info>'
        );

        $pluginMap = $this->getPluginMap($shopwarePath);

        foreach ($pluginLanguages as $locale => $pluginName) {
            $this->output->writeln(
                '<info>Updating '.$locale.'...</info>'
            );

            $snippetDataArray = $this->extractForWriting($snippetBasket, [$locale]);

            $this->shopwareIniWriter->writeSnippets($pluginMap[$pluginName], $snippetDataArray, true);
        }

        $this->output->writeln(
            '<info>Translation plugin snippets updated correctly</info>'
        );
    }

    /**
     * Scans the given Shopware path and generates a map of plugin names and their corresponding folders
     *
     * @param string $shopwarePath
     * @return array
     */
    private function getPluginMap($shopwarePath)
    {
        $pluginMapCreator = new PluginMapCreator();
        return $pluginMapCreator->create($shopwarePath);
    }

    /**
     * @param SnippetBasket $snippetBasket
     * @param string[] $localeArray
     * @param string|null $context
     * @return array
     */
    private function extractForWriting(SnippetBasket $snippetBasket, $localeArray, $context = null)
    {
        if ($context) {
            $snippetFiles = $snippetBasket->getByContext($context);
        } else {
            $snippetFiles = $snippetBasket->getAll();
        }

        return $this->snippetWritingExtractor->extractForWriting($snippetFiles, $localeArray);
    }

    /**
     * Reads snippets from given Shopware location
     * Includes snippets from the core, the core plugins and premium plugins
     *
     * @param string $shopwarePath
     * @return SnippetBasket
     */
    public function readShopwareInstallationSnippets($shopwarePath)
    {
        $shopwareSnippetReader = new ShopwareSnippetReader($this->snippetReader, $this->output);
        return $shopwareSnippetReader->read($shopwarePath);
    }

    /**
     * Installs and loads snippets for a plugin
     *
     * @param string $shopwarePath
     * @param string $pluginName
     * @return null|SnippetBasket
     */
    public function getSnippetsForPlugin($shopwarePath, $pluginName)
    {
        $plugins = $this->pluginProvider->getPluginByName($pluginName);

        if (empty($plugins)) {
            throw new \RuntimeException('Plugin "'.$pluginName.'" not found');
        } elseif (count($plugins) > 1) {
            throw new \RuntimeException('Multiple plugins found when searching  for "'.$pluginName.'"');
        }

        $plugin = array_shift($plugins);

        $pluginDirPath = realpath($shopwarePath.'/engine/Shopware/Plugins/Local/');
        $this->utilities->changeDir($pluginDirPath);

        try {
            $this->installService->install(
                $plugin,
                $shopwarePath,
                false,
                null,
                false
            );
        } catch (\RuntimeException $exception) {
            $this->output->writeln("<error>Can't install plugin $plugin->name.</error>");
        }

        $pluginPath = $pluginDirPath.'/'.$plugin->module."/".$pluginName;
        $pluginSnippetPath = realpath($pluginPath."/Snippets") ?: realpath($pluginPath."/snippets");

        if (empty($pluginSnippetPath)) {
            return null;
        }

        return $this->snippetReader->readSnippets($pluginSnippetPath);
    }
}
