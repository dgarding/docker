<?php

namespace ShopwarePrivate\Translation\Services\Shopware;

use ShopwarePrivate\Translation\Services\Data\SnippetFile;

class SnippetWritingExtractor
{
    /**
     * @param SnippetFile[] $snippetFiles
     * @param string[] $localeArray
     * @return array
     */
    public function extractForWriting(array $snippetFiles, $localeArray)
    {
        $snippetDataArray = [];

        foreach ($snippetFiles as $snippetFile) {
            $dataPath = $snippetFile->namespace.'.ini';

            foreach ($localeArray as $locale) {
                if (!array_key_exists($locale, $snippetFile->data)) {
                    continue;
                }

                if (empty($snippetFile->data[$locale])) {
                    continue;
                }

                if (!array_key_exists($dataPath, $snippetDataArray)) {
                    $snippetDataArray[$dataPath] = [];
                }
                if (!array_key_exists($locale, $snippetDataArray[$dataPath])) {
                    $snippetDataArray[$dataPath][$locale] = [];
                }

                $snippetDataArray[$dataPath][$locale] = array_merge($snippetDataArray[$dataPath][$locale], $snippetFile->data[$locale]);
            }
        }

        return $snippetDataArray;
    }
}