<?php

namespace ShopwarePrivate\Translation\Services\CrowdinApi\RequestGenerator;

use GuzzleHttp\Psr7\Request;
use Psr\Http\Message\RequestInterface;
use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;
use ShopwarePrivate\Translation\Services\Data\SnippetFile;

class DeleteFilesGenerator implements RequestGeneratorInterface
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var array
     */
    private $filePaths;

    /**
     * @param array $config
     * @param array $filePaths
     */
    public function __construct(array $config, array $filePaths)
    {
        $this->config = $config;
        $this->filePaths = $filePaths;
    }

    /**
     * Yields Request objects to upload new files to Crowdin
     *
     * @param CrowdinClient $crowdinClient
     * @return RequestInterface[]
     */
    public function generate(CrowdinClient $crowdinClient)
    {
        $requests = [];

        /** @var SnippetFile $snippetFile */
        foreach ($this->filePaths as $filePath) {
            $query = http_build_query(
                [
                    'key' => $this->config['projectKey'],
                    'json' => true,
                    'file' => $filePath,
                ]
            );
            $url = $this->config['endpoint'].$this->config['projectId'].'/delete-file?'.$query;

            $requests[] = new Request(
                'POST',
                $url
            );
        }

        return $requests;
    }
}