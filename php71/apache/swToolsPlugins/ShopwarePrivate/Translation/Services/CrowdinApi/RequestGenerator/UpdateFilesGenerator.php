<?php

namespace ShopwarePrivate\Translation\Services\CrowdinApi\RequestGenerator;

use GuzzleHttp\Psr7\MultipartStream;
use GuzzleHttp\Psr7\Request;
use Piwik\Ini\IniWriter;
use Psr\Http\Message\RequestInterface;
use ShopwarePrivate\Translation\Services\CrowdinApi\CrowdinClient;
use ShopwarePrivate\Translation\Services\Data\SnippetFile;
use ShopwarePrivate\Translation\Services\Utils\LanguageSettings;

class UpdateFilesGenerator implements RequestGeneratorInterface
{
    /**
     * @var array
     */
    private $config;

    /**
     * @var IniWriter
     */
    private $iniWriter;

    /**
     * @var SnippetFile[]
     */
    private $snippetFileData;

    /**
     * @param array $config
     * @param IniWriter $iniWriter
     * @param SnippetFile[] $snippetFileData
     */
    public function __construct(array $config, IniWriter $iniWriter, array $snippetFileData)
    {
        $this->config = $config;
        $this->iniWriter = $iniWriter;
        $this->snippetFileData = $snippetFileData;
    }

    /**
     * Yields Request objects to update files in Crowdin
     *
     * @param CrowdinClient $crowdinClient
     * @return RequestInterface[]
     */
    public function generate(CrowdinClient $crowdinClient)
    {
        $requests = [];

        foreach ($this->snippetFileData as $snippetFile) {
            if (empty($snippetFile->data)) {
                continue;
            }
            $snippetData = $snippetFile->generateSnippetFileStructure(LanguageSettings::getCoreLanguages());

            $query = http_build_query(
                [
                    'key' => $this->config['projectKey'],
                    'json' => true,
                    'type' => 'ini',
                    'update_option' => 'update_without_changes',
                ]
            );
            $url = $this->config['endpoint'].$this->config['projectId'].'/update-file?'.$query;

            $fileStream = new MultipartStream([
                [
                    'name' => 'files[' . $snippetFile->getContextPath() . ']',
                    'filename' => $snippetFile->getContextPath(),
                    'contents' => $this->iniWriter->writeToString([$snippetData])
                ]
            ]);

            $requests[] = new Request(
                'POST',
                $url,
                [],
                $fileStream
            );
        }

        return $requests;
    }
}