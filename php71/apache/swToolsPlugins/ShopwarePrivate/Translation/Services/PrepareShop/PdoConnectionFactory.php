<?php

namespace ShopwarePrivate\Translation\Services\PrepareShop;

use PDO;

class PdoConnectionFactory
{
    /**
     * @param string $shopwareRootPath
     * @return PDO
     */
    public function connect($shopwareRootPath)
    {
        if (!file_exists($shopwareRootPath . '/config.php')) {
            throw new \RuntimeException(
                'Could not create language subshops. config.php was not found in shopware path ' . $shopwarePath
            );
        }

        $config = require($shopwareRootPath . '/config.php');
        $config = $config['db'];

        return new PDO(
            'mysql:host='.$config['host'].';dbname='.$config['dbname'], $config['username'], $config['password'],
            [
                PDO::MYSQL_ATTR_LOCAL_INFILE => true, // if this still does not work, php5-mysqnd might work
                PDO::ERRMODE_EXCEPTION => 1,
            ]
        );
    }
}