<?php


namespace ShopwarePrivate\Translation\Services\PrepareShop;


class SubShopCreator
{
    /**
     * @var \PDO
     */
    private $connection;

    /**
     * @param \PDO $connection
     */
    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param array $languages
     */
    public function create($languages)
    {
        foreach ($languages as $code => $plugin) {
            if ($plugin === 'core') {
                continue;
            }

            if (count($this->getPlugin($plugin)) === 0) {
                continue;
            }

            $locale = $this->getLocaleByLanguageCode($code);

            $sql = <<<SQL
INSERT INTO s_core_shops
(main_id, title, name, position, hosts, secure, customer_scope, `default`, active, locale_id, category_id, customer_group_id, fallback_id, currency_id)
VALUES 
(1, :name, :name, 0, "", 0, 0, 2, 1, :locale, 3, 1, 3, 1)
SQL;

            $stmt = $this->connection->prepare($sql);
            $stmt->bindValue('name', $plugin);
            $stmt->bindValue('locale', $locale['id']);
            if (!$stmt->execute()) {
                throw new \RuntimeException('Couldn\'t create language subshop ' . $code);
            }
        }
    }

    private function getPlugin($name)
    {
        $stmt = $this->connection->prepare('SELECT * FROM s_core_plugins WHERE name = :name');
        $stmt->bindValue('name', $name);
        $stmt->execute();
        return $stmt->fetchAll();
    }

    private function getLocaleByLanguageCode($localeCode)
    {
        $stmt = $this->connection->prepare('SELECT * FROM s_core_locales WHERE locale = :locale');
        $stmt->bindValue('locale', $localeCode);
        $stmt->execute();
        $result = $stmt->fetchAll();

        return $result[0];
    }
}