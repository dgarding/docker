<?php


namespace ShopwarePrivate\Translation\Services\PrepareShop;

use PDO;
use Shopware\Plugin\Services\Install;
use Shopware\Plugin\Services\PluginProvider;
use ShopwareCli\Utilities;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PrepareShopService
{
    /**
     * @var PluginProvider
     */
    private $pluginProvider;

    /**
     * @var Install
     */
    private $installService;

    /**
     * @param Utilities $utilities
     * @param PluginProvider $pluginProvider
     * @param Install $installService
     */
    public function __construct(
        Utilities $utilities,
        PluginProvider $pluginProvider,
        Install $installService
    ) {
        $this->pluginProvider = $pluginProvider;
        $this->installService = $installService;
    }

    /**
     * @param string $shopwarePath
     * @param array $pluginNames
     * @param OutputInterface $output
     */
    public function prepare($shopwarePath, array $pluginNames, OutputInterface $output)
    {
        foreach ($pluginNames as $pluginName) {
            try {
                $plugins = $this->pluginProvider->getPluginByName($pluginName, true);
                $this->installService->install($plugins[0], $shopwarePath, true);
            } catch (\Exception $e) {
                throw new \RuntimeException(
                    $pluginName . ' could not be installed or updated.' . PHP_EOL . PHP_EOL .
                    'Check branch tracking, origin and current branch.' . PHP_EOL . PHP_EOL .
                    $e->getMessage()
                );
            }
        }
    }
}