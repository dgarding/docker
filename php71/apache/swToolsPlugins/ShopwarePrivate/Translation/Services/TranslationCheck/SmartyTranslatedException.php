<?php

namespace ShopwarePrivate\Translation\Services\TranslationCheck;

class SmartyTranslatedException extends \RuntimeException
{
    /**
     * @var string
     */
    private $path;

    /**
     * @var string
     */
    private $lineNumber;

    /**
     * @var string
     */
    private $sourceSmarty;

    /**
     * @var string
     */
    private $translatedSmarty;

    /**
     * @var string
     */
    private $snippetName;

    /**
     * @param string $path
     * @param int $lineNumber
     * @param string $sourceSmarty
     * @param string $translatedSmarty
     * @param string $snippetName
     * @param string $message
     */
    public function __construct($path, $lineNumber, $sourceSmarty, $translatedSmarty, $snippetName, $message = '')
    {
        parent::__construct($message);
        $this->path = $path;
        $this->lineNumber = $lineNumber;
        $this->sourceSmarty = $sourceSmarty;
        $this->translatedSmarty = $translatedSmarty;
        $this->snippetName = $snippetName;
    }

    /**
     * @param TranslationLine $translationLine
     * @param string $sourceSmarty
     * @param string $translatedSmarty
     * @param string $message
     * @return SmartyTranslatedException
     */
    public static function createFromTranslationLine(TranslationLine $translationLine, $sourceSmarty, $translatedSmarty, $message = '')
    {
        return new self(
            $translationLine->file->getRelativePath() . '/' . $translationLine->file->getFilename(),
            $translationLine->lineNumber,
            $sourceSmarty,
            $translatedSmarty,
            key(parse_ini_string($translationLine->content)),
            $message
        );
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getLineNumber()
    {
        return $this->lineNumber;
    }

    /**
     * @return string
     */
    public function getSourceSmarty()
    {
        return $this->sourceSmarty;
    }

    /**
     * @return string
     */
    public function getTranslatedSmarty()
    {
        return $this->translatedSmarty;
    }

    /**
     * @return string
     */
    public function getSnippetName()
    {
        return $this->snippetName;
    }
}