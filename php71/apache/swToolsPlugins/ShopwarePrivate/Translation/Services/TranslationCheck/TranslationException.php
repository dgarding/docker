<?php

namespace ShopwarePrivate\Translation\Services\TranslationCheck;

class TranslationException extends \Exception
{
    /**
     * @var TranslationLine
     */
    private $translationLine;

    /**
     * @param string $message
     * @param TranslationLine $translationLine
     */
    public function __construct($message, TranslationLine $translationLine)
    {
        parent::__construct($message);
        $this->translationLine = $translationLine;
    }

    /**
     * @return string
     */
    public function getErrorMessage()
    {
        return 'Error at line ' . $this->translationLine->lineNumber . ' in ' . $this->translationLine->file->getRealPath() . ' for ' . $this->translationLine->content . ' with error ' . $this->getMessage();
    }

    public function getTranslationLine()
    {
        return $this->translationLine;
    }
}