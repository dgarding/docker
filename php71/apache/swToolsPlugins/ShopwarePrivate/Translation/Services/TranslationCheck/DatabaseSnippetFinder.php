<?php

namespace ShopwarePrivate\Translation\Services\TranslationCheck;

class DatabaseSnippetFinder implements SnippetFinderInterface
{
    /**
     * @var \PDO
     */
    private $connection;

    /**
     * @param \PDO $connection
     */
    public function __construct(\PDO $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @param TranslationLine $translationLine
     * @return string
     */
    public function find(TranslationLine $translationLine)
    {
        $parsedIni = parse_ini_string($translationLine->content);
        $pos = strpos($translationLine->file->getRelativePath(), 'snippets') + strlen('snippets') + 1;

        $path = substr($translationLine->file->getRelativePath(), $pos);
        $namespace = $path . '/' . $translationLine->file->getBasename('.ini');

        $stmt = $this->connection->prepare(
            'SELECT * FROM s_core_snippets WHERE namespace = :namespace AND name = :name AND localeID = :id'
        );
        $stmt->bindParam('namespace', $namespace);
        $stmt->bindValue('id', '2');
        $stmt->bindValue('name', key($parsedIni));
        $stmt->execute();
        $result = $stmt->fetchAll();

        if (count($result) === 0) {
            throw SmartyTranslatedException::createFromTranslationLine(
                $translationLine,
                '',
                '',
                'Snippet ' . $namespace . '::' . key($parsedIni)
            );
        }
        return $result[0]['value'];
    }
}