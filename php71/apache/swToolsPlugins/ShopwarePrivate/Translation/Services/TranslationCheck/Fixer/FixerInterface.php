<?php

namespace ShopwarePrivate\Translation\Services\TranslationCheck\Fixer;

use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationLine;

interface FixerInterface
{
    /**
     * @return string
     */
    public function fix(TranslationLine $translationLine);
}