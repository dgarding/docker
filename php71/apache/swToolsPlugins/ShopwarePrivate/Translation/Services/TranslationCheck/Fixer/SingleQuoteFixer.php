<?php

namespace ShopwarePrivate\Translation\Services\TranslationCheck\Fixer;

use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationLine;

class SingleQuoteFixer implements FixerInterface
{
    /**
     * @inheritdoc
     */
    public function fix(TranslationLine $translationLine)
    {
        if ($this->isFixed($translationLine)) {
            return $translationLine->content;
        }

        $content = $this->unescapeAllSingleQuotes($translationLine);
        return str_replace('\'', '\\\'', $content);
    }

    /**
     * @param TranslationLine $translationLine
     * @return string
     */
    private function unescapeAllSingleQuotes(TranslationLine $translationLine)
    {
        return str_replace('\\\'', '\'', $translationLine->content);
    }

    /**
     * @param TranslationLine $translationLine
     * @return bool
     */
    private function isFixed(TranslationLine $translationLine)
    {
        return strpos($translationLine->content, '\\\'')
            && !strpos($translationLine->content, '\'');
    }
}