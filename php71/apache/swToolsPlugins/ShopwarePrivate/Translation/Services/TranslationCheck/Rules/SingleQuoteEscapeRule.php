<?php

namespace ShopwarePrivate\Translation\Services\TranslationCheck\Rules;

use ShopwarePrivate\Translation\Services\TranslationCheck\Fixer\FixerInterface;
use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationException;
use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationLine;

class SingleQuoteEscapeRule implements RuleInterface
{
    /**
     * @var FixerInterface
     */
    private $fixer;

    private $blacklist = [
        'backend/mail/view/navigation' => [ 'mails_user' ]
    ];

    private $whitelist = [
        'frontend/index/index' => [ 'IndexNoCookiesNotice' ]
    ];

    /**
     * @param FixerInterface $fixer
     */
    public function __construct(FixerInterface $fixer)
    {
        $this->fixer = $fixer;
    }

    /**
     * @param TranslationLine $translationLine
     * @return string
     */
    public function applyFix(TranslationLine $translationLine)
    {
        return $this->fixer->fix($translationLine);
    }

    /**
     * @param TranslationLine $translationLine
     * @throws TranslationException
     */
    public function check(TranslationLine $translationLine)
    {
        if (!strpos($translationLine->file->getRealPath(), 'backend')) {
            return;
        }

        /**
         * Match only ' which are not escaped in backend ini files
         *
         * @example:
         * title = "An 'error occured"
         */
        if (preg_match("/(?<!\\\)'/", $translationLine->content)) {
            throw new TranslationException('Unescaped \' detected', $translationLine);
        }
    }
}