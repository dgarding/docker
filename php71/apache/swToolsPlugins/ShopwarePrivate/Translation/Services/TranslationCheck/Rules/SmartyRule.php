<?php


namespace ShopwarePrivate\Translation\Services\TranslationCheck\Rules;


use ShopwarePrivate\Translation\Services\TranslationCheck\SmartyTranslatedException;
use ShopwarePrivate\Translation\Services\TranslationCheck\SnippetFinderInterface;
use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationException;
use ShopwarePrivate\Translation\Services\TranslationCheck\TranslationLine;

class SmartyRule implements RuleInterface
{
    /**
     * @var SnippetFinderInterface
     */
    private $snippetFinder;

    /**
     * @param SnippetFinderInterface $snippetFileFinder
     */
    public function __construct(SnippetFinderInterface $snippetFileFinder)
    {
        $this->snippetFinder = $snippetFileFinder;
    }

    /**
     * @param TranslationLine $translationLine
     * @return string
     */
    public function applyFix(TranslationLine $translationLine)
    {
        throw new \Exception('Should not be implemented, smarty code can not be autofixed due translations inside of smarty tags etc.');
    }

    /**
     * @param TranslationLine $translationLine
     * @throws TranslationException
     */
    public function check(TranslationLine $translationLine)
    {
        $result = $this->getSmartyCode($translationLine->content);
        $translatedSmartyCode = $result['result'];

        if ($result['amount'] > 0) {
            $sourceSnippetValue = $this->snippetFinder->find($translationLine);
            $sourceSmartyCode = $this->getSmartyCode($sourceSnippetValue)['result'];

            foreach ($sourceSmartyCode as $idx => $source) {
                if ($sourceSmartyCode[$idx] !== $translatedSmartyCode[$idx]) {
                    throw SmartyTranslatedException::createFromTranslationLine(
                        $translationLine,
                        $sourceSmartyCode[$idx],
                        $translatedSmartyCode[$idx]
                    );
                }
            }
        }
    }

    /**
     * @param string $content
     * @param $smartyCode
     * @return array
     */
    protected function getSmartyCode($content)
    {
        $smartyCode = [];
        $amount = preg_match_all('/(?={)(.*?)(?<=})/', $content, $smartyCode);
        return [ 'result' => array_values(array_filter(array_shift($smartyCode))), 'amount' => $amount ];
    }
}