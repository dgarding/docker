<?php

namespace ShopwarePrivate\Translation;

require_once __DIR__ . '/../vendor/autoload.php';

use ShopwarePrivate\Translation\Command\CreateChangelogCommand;
use ShopwarePrivate\Translation\Command\FixCommand;
use ShopwarePrivate\Translation\Command\CreateTicketCommand;
use ShopwarePrivate\Translation\Command\SubShopCreateCommand;
use ShopwarePrivate\Translation\Command\ToShopwareCommand;
use ShopwarePrivate\Translation\Command\DownloadCommand;
use ShopwarePrivate\Translation\Command\MappingCommand;
use ShopwarePrivate\Translation\Command\SetupShopCommand;
use ShopwarePrivate\Translation\Command\SyncCommand;
use ShopwarePrivate\Translation\Command\StatusCommand;
use ShopwarePrivate\Translation\Command\UnzipCommand;
use ShopwarePrivate\Translation\Command\ToCrowdinCommand;
use ShopwareCli\Application\ConsoleAwareExtension;
use ShopwareCli\Application\ContainerAwareExtension;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\XmlFileLoader;

class Bootstrap implements ContainerAwareExtension, ConsoleAwareExtension
{

    /**
     * Concurrent requests are fully supported in current implementation
     * However, there seems to be a bug when using concurrent requests, where snippets
     * from different files with a common name get mixed translations.
     * At the moment, I cannot confirm if this is a server or client error.
     * Until this is fixed, we can only use 1 concurrent request (no concurrency)
     */
    const DEFAULT_CONCURRENT_REQUESTS = 1;

    /**
     * {@inheritdoc}
     */
    public function setContainer(ContainerBuilder $container)
    {
        $xmlFileLoader = new XmlFileLoader(
            $container,
            new FileLocator()
        );
        $xmlFileLoader->load(__DIR__ . '/services.xml');
    }

    /**
     * {@inheritdoc}
     */
    public function getConsoleCommands()
    {
        return [
            new ToCrowdinCommand(),
            new ToShopwareCommand(),
            new FixCommand(),
            new SetupShopCommand(),
            new UnzipCommand(),
            new DownloadCommand(),
            new CreateTicketCommand(),
            new MappingCommand(),
            new StatusCommand(),
            new SubShopCreateCommand(),
            new CreateChangelogCommand()
        ];
    }
}
