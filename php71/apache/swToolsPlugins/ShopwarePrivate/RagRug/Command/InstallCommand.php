<?php

namespace ShopwarePrivate\RagRug\Command;

use ShopwareCli\Command\BaseCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class InstallCommand extends BaseCommand
{

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setName('rr:install')
            ->setDescription('Install a plugin from the rag rug network.')
            ->addArgument(
                'name',
                InputArgument::OPTIONAL,
                'Name of the plugin to install'
            )
            ->setHelp(<<<EOF
The <info>%command.name%</info> installs a plugin from the rag rug network.
EOF
            );;
    }

    /**
     * {@inheritdoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $file = __DIR__ . '/../simple_repo.csv';

        $result = $this->readCSV($file);
        echo "<pre>";
        print_r($result);
        exit();

    }

    function readCSV($csvFile)
    {
        $file_handle = fopen($csvFile, 'r');

        // skip first line
        fgetcsv($file_handle, 1024);

        $lines = array();
        while (!feof($file_handle)) {
            $lines[] = fgetcsv($file_handle, 1024, ';');
        }
        fclose($file_handle);

        return $lines;
    }

}