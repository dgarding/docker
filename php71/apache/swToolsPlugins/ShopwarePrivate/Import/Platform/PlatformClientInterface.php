<?php

namespace ShopwarePrivate\Import\Platform;

use ShopwarePrivate\Import\Struct\CodeSource;
use ShopwarePrivate\Import\Struct\PullRequest;

interface PlatformClientInterface
{
    const COMMENT_TEMPLATE = <<<EOF
Hello,

thank you for creating this pull request.
I have opened an issue on our Issue Tracker for you. See the issue link: %s

Please use this issue to track the state of your pull request.
EOF;

    /**
     * @param int $number
     *
     * @return PullRequest
     */
    public function getPullRequest($number);

    /**
     * @param int $number
     * @param string $issueUrl
     * @return string Comment url
     */
    public function createComment($number, $issueUrl);

    /**
     * @param array $config
     * @param CodeSource $codeSource
     *
     * @return PlatformClientInterface
     */
    public static function create(array $config, CodeSource $codeSource);

    /**
     * @param int $number
     * @param array $labels
     *
     * @return void
     */
    public function setLabels($number, array $labels);
}