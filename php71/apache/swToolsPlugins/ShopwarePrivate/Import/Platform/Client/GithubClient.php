<?php

namespace ShopwarePrivate\Import\Platform\Client;

use Github\Client;
use ShopwarePrivate\Import\Platform\PlatformClientInterface;
use ShopwarePrivate\Import\Struct\CodeSource;
use ShopwarePrivate\Import\Struct\PullRequest;

class GithubClient implements PlatformClientInterface
{
    /**
     * @var Client
     */
    private $client;

    /**
     * @var CodeSource|null
     */
    private $codeSource;

    /**
     * @var string
     */
    private $githubToken;

    /**
     * @param string $githubToken
     * @param CodeSource $codeSource
     */
    private function __construct($githubToken, CodeSource $codeSource)
    {
        $this->githubToken = $githubToken;
        $this->codeSource = $codeSource;

        $client = new Client();
        $client->authenticate($this->githubToken, null, Client::AUTH_HTTP_TOKEN);

        $this->client = $client;
    }

    /**
     * @param array $config
     * @param CodeSource $codeSource
     *
     * @return PlatformClientInterface
     */
    public static function create(array $config, CodeSource $codeSource)
    {
        return new self($config['githubToken'], $codeSource);
    }

    /**
     * @param int $number
     *
     * @return PullRequest
     */
    public function getPullRequest($number)
    {
        $data = $this->client->pullRequest()->show(
            $this->codeSource->getOrganization(),
            $this->codeSource->getRepository(),
            $number
        );

        $pullRequest = new PullRequest();
        $pullRequest->setTitle($data['title']);
        $pullRequest->setUrl($data['html_url']);
        $pullRequest->setUser($data['user']['login']);
        $pullRequest->setDescription($data['body']);

        return $pullRequest;
    }

    /**
     * @param int $number
     * @param string $issueUrl
     *
     * @return string Comment url
     */
    public function createComment($number, $issueUrl)
    {
        $content = sprintf(
            PlatformClientInterface::COMMENT_TEMPLATE,
            $issueUrl
        );

        $response = $this->client->issue()->comments()->create(
            $this->codeSource->getOrganization(),
            $this->codeSource->getRepository(),
            $number,
            ['body' => $content]
        );

        return $response['html_url'];
    }

    public function setLabels($number, array $labels)
    {
        $this->client->issue()->labels()->add(
            $this->codeSource->getOrganization(),
            $this->codeSource->getRepository(),
            $number,
            $labels
        );
    }
}