<?php

namespace ShopwarePrivate\Import\Struct;

class CodeSource
{
    /**
     * @var string
     *
     * e.g. github, gitlab, ...
     */
    private $platform;

    /**
     * @var string
     *
     * username or organization name
     */
    private $organization;

    /**
     * @var string
     */
    private $repository;

    /**
     * @param string $platform
     * @param string $organization
     * @param string $repository
     */
    public function __construct($platform, $organization, $repository)
    {
        $this->platform = $platform;
        $this->organization = $organization;
        $this->repository = $repository;
    }

    /**
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * @return string
     */
    public function getOrganization()
    {
        return $this->organization;
    }

    /**
     * @return string
     */
    public function getRepository()
    {
        return $this->repository;
    }


}