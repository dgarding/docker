<?php

namespace ShopwarePrivate\Import\Plugins;

class GitlabPlugin extends Plugin
{
    public function getOrganization()
    {
        return 'plugins';
    }

    public function getPlatform()
    {
        return 'gitlab';
    }
}