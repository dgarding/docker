<?php

namespace ShopwarePrivate\Import\Plugins;

class GithubPlugin extends Plugin
{
    public function getOrganization()
    {
        return 'shopwareLabs';
    }

    public function getPlatform()
    {
        return 'github';
    }
}