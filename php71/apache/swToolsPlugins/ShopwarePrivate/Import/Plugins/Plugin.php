<?php

namespace ShopwarePrivate\Import\Plugins;

abstract class Plugin
{
    /**
     * @var string - i.e. SwagBackendOrder
     */
    private $technicalName;

    /**
     * @var string - i.e. BackendOrder
     */
    private $jiraNameId;

    /**
     * @param string $technicalName
     * @param string $jiraNameId
     */
    public function __construct($technicalName, $jiraNameId)
    {
        $this->technicalName = $technicalName;
        $this->jiraNameId = $jiraNameId;
    }

    /**
     * @return string
     */
    public function getTechnicalName()
    {
        return $this->technicalName;
    }

    /**
     * @return string
     */
    public function getJiraNameId()
    {
        return $this->jiraNameId;
    }

    public abstract function getOrganization();
    public abstract function getPlatform();
}
