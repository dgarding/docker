<?php

namespace ShopwarePrivate\Import\Jira\Issue;

use ShopwarePrivate\Import\Struct\CodeSource;
use ShopwarePrivate\Import\Struct\PullRequest;

class ShopwareIssue extends AbstractIssue
{
    /**
     * @var bool
     */
    private $isQuickPick = false;

    /**
     * @param CodeSource $codeSource
     * @param PullRequest $pullRequest
     * @param bool $isQuickPick
     */
    public function __construct(CodeSource $codeSource, PullRequest $pullRequest, $isQuickPick = false)
    {
        $this->isQuickPick = $isQuickPick;

        parent::__construct($codeSource, $pullRequest);
    }

    public function getProjectKey()
    {
        return 'SW';
    }

    public function getIssueType()
    {
        return 'Bug';
    }

    public function getAdditionalFields()
    {
        $epic = 'SW-13943';
        if ($this->isQuickPick) {
            $epic = 'SW-19976';
        }

        return [
            'customfield_10801' => $epic, // epic
            'fixVersions' => [['id' => '11309']], // misc tasks
        ];
    }
}
