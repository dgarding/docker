<?php

namespace ShopwarePrivate\Import\Jira\Issue;

use ShopwarePrivate\Import\Plugins\Plugin;
use ShopwarePrivate\Import\Struct\CodeSource;
use ShopwarePrivate\Import\Struct\PullRequest;

class PluginIssue extends AbstractIssue
{
    /**
     * @var Plugin
     */
    private $plugin;

    /**
     * @param Plugin $plugin
     * @param CodeSource $codeSource
     * @param PullRequest $pullRequest
     */
    public function __construct(Plugin $plugin, CodeSource $codeSource, PullRequest $pullRequest)
    {
        $this->plugin = $plugin;

        parent::__construct($codeSource, $pullRequest);
    }

    public function getProjectKey()
    {
        return 'PT';
    }

    public function getIssueType()
    {
        return 'Bug';
    }

    public function getAdditionalFields()
    {
        return [
            'fixVersions' => [['name' => 'In Verification (Plugin)']],
            'customfield_11200' => ['id' => $this->plugin->getJiraNameId()],
            'customfield_10801' => 'PT-8950', // epic
        ];
    }
}
