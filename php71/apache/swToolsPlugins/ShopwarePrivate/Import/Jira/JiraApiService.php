<?php
namespace ShopwarePrivate\Import\Jira;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\ClientException;
use ShopwareCli\Config;
use ShopwarePrivate\Import\Jira\Issue\AbstractIssue;

class JiraApiService
{
    /**
     * @var HttpClient
     */
    private $client;

    /**
     * @param Config $config
     * @return JiraApiService
     */
    public static function create(Config $config)
    {
        $client = new HttpClient([
            'auth' => [
                $config->offsetGet('Import')['jiraUser'],
                $config->offsetGet('Import')['jiraPassword']
            ]
        ]);

        return new JiraApiService($client);
    }

    /**
     * @param HttpClient $client
     */
    public function __construct(HttpClient $client)
    {
        $this->client = $client;
    }

    /**
     * @param AbstractIssue $issue
     * @return array
     * @throws \RuntimeException
     */
    public function createJiraIssue(AbstractIssue $issue)
    {
        try {
            $res = $this->client->request('POST', 'https://jira.shopware.com/rest/api/2/issue/', [
                'json' => $issue->toArray()
            ]);
        } catch (ClientException $e) {
            $errorArray = json_decode($e->getResponse()->getBody()->getContents(), true);
            $errorMessage = 'Jira API error: ' . $e->getMessage() . "\n";

            if (is_array($errorArray)) {
                /** @var array $errors */
                $errors = $errorArray['errors'];
                foreach ($errors as $error) {
                    $errorMessage .= $error . ' ';
                }
            }

            throw new \RuntimeException($errorMessage);
        }

        if (in_array('SonicWALL SSL-VPN Web Server', $res->getHeaders()['Server'], true)) {
            throw new \RuntimeException('The sonic wall fucked your ticket <3');
        }

        return json_decode($res->getBody(), true);
    }
}
