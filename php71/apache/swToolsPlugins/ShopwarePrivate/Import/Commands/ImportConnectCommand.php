<?php

namespace ShopwarePrivate\Import\Commands;

use ShopwareCli\Command\BaseCommand;
use ShopwarePrivate\Import\Jira\Issue\ConnectIssue;
use ShopwarePrivate\Import\Platform\Factory;
use ShopwarePrivate\Import\Platform\Client\GithubClient;
use ShopwarePrivate\Import\Struct\CodeSource;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use ShopwarePrivate\Import\Jira\JiraApiService;

class ImportConnectCommand extends BaseCommand
{
    protected function configure()
    {
        $this
            ->setName('import:connect')
            ->setDescription('Imports a gihub pull request into JIRA and responds to the Pull Request with an issue URL')
            ->addArgument('project', InputArgument::REQUIRED, 'organization/repository syntax')
            ->addArgument('PR-Number', InputArgument::REQUIRED, 'Number of pull Request')
        ;
    }

    /**
     * @inheritdoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $number = $input->getArgument('PR-Number');

        list($organization, $repository) = explode('/', $input->getArgument('project'));
        $codeSource = new CodeSource('github', $organization, $repository);

        $apiClientFactory = $this->getApiClientFactory();
        $platformClient = $apiClientFactory->createClient($this->container->get('config')['Import'], $codeSource);

        $pullRequest = $platformClient->getPullRequest($number);
        $issue = new ConnectIssue($codeSource, $pullRequest);

        $jiraService = JiraApiService::create($this->container->get('config'));
        $result = $jiraService->createJiraIssue($issue);

        $ticketNo = $result['key'];
        if (empty($ticketNo)) {
            $output->writeln('Could not create JIRA ticket.');

            return;
        }

        $issueTrackerLink = $this->buildIssueTrackerLink($ticketNo);
        $commentLink = $platformClient->createComment($number, $issueTrackerLink);

        $output->writeln('Internal Issue Link: ' . $this->buildInternalIssueTrackerLink($ticketNo));
        $output->writeln('External Issue Link: ' . $issueTrackerLink);
        $output->writeln('Comment Link:        ' . $commentLink);
    }

    /**
     * @param string $ticketNo
     * @return string
     */
    private function buildIssueTrackerLink($ticketNo)
    {
        return 'https://issues.shopware.com/issues/' . $ticketNo;
    }

    /**
     * @param string $ticketNo
     * @return string
     */
    private function buildInternalIssueTrackerLink($ticketNo)
    {
        return 'https://jira.shopware.com/browse/' . $ticketNo;
    }

    private function getApiClientFactory()
    {
        return new Factory([
            'github' => GithubClient::class
        ]);
    }
}
