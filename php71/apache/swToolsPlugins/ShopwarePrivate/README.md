# Shopware cli-tool plugins

## Requirements

 * In order to upload plugins, you need to have the php extension enabled (pecl install -f ssh2)
 * In order to use `github:checkout:pullrequest` you need to install `hub`, [more info](https://github.com/github/hub). 

## Installation

### Directly into the cli-tools (recommended)

1. Go to your sw-cli-tools `src/Extensions` directory 
2. Check it out into a `ShopwarePrivate` directory: `$ git clone ssh://git@stash.shopware.com/tool/sw-cli-tools-plugins.git ShopwarePrivate`
3. Go to `ShopwarePrivate` and run `$ composer install`

### In ~/.config

1. `$ cd ~/.config/sw-cli-tools/extensions`
2. Check it out into a `ShopwarePrivate` directory: `$ git clone ssh://git@stash.shopware.com/tool/sw-cli-tools-plugins.git ShopwarePrivate`
3. Go to `ShopwarePrivate` and run `$ composer install`

## Import from GitHub/GitLab

### Configuration
`~/.config/sw-cli-tools/config.yml`:

```
Import:
    jiraUser:
    jiraPassword:
    githubToken:
    gitlabToken:
```

### Commands

```
import:shopware - Imports a pull request from https://github.com/shopware/shopware as a jira issue, creates a comment and updates Labels (arguments: PR number; options: quick-pick)​
import:plugin - Imports a pull request from GitHub or GitLab, depending on the plugin as a jira issue and creates a comment (arguments: Plugin name, PR number)
import:connect - Imports a pull request from the Connect project as jira issue and creates a comment (arguments: project, PR number)
```

## Crowdin - Translations

### Configuration

`~/.config/sw-cli-tools/config.yml`:

```
Crowdin:
    projectId: shopware
    projectKey: API-Key
    concurrentRequests:
    endpoint: https://api.crowdin.com/api/project/
```

You find the API credentials on Crowdin, [find more information here.](https://support.crowdin.com/api/api-integration-setup/)

## Connect

At the moment it is not possible to use the default plugin commands for the connect shopware plugin.

```
connect:upload - Release a new connect version (will create a release package automatically)
connect:zip:vcs - Create a release package
```

## Run phpunit tests

1. Install the extension in the `src/Extensions/ShopwarePrivate` directory
2. Go to `src/Extensions/ShopwarePrivate` and run `../../../vendor/phpunit/phpunit/phpunit`
