<?php

include __DIR__ . '/../../../test_bootstrap.php';

/** @var \Composer\Autoload\ClassLoader $loader */
$loader = TestLoaderProvider::getLoader();
$loader->addPsr4('ShopwarePrivate\\', __DIR__ . '/../ShopwarePrivate');