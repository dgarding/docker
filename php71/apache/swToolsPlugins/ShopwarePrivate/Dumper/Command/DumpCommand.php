<?php

namespace ShopwarePrivate\Dumper\Command;

use ShopwarePrivate\Dumper\Struct\ConnectionInfo;
use ShopwareCli\Command\BaseCommand;
use ShopwareCli\Services\IoService;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DumpCommand extends BaseCommand
{

    protected function configure()
    {
        $this->setName('dump')
            ->setDescription('Dump a shopware installation into a zip file')
            ->addOption(
                'shopware-root',
                'r',
                InputOption::VALUE_OPTIONAL,
                'Root of your shopware installation'
            )
            ->addOption(
                'no-media',
                null,
                InputOption::VALUE_NONE,
                'Skip media files'
            )
            ->setHelp(
                <<<EOF
                            The <info>%command.name%</info> dumps a shopware installation including database into a file
EOF
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $shopwarePath = $input->getOption('shopware-root') ? : null;
        $noMedia = $input->getOption('no-media');

        $shopwarePath = rtrim($this->container->get('utilities')->getValidShopwarePath($shopwarePath), '/') . '/';

        $tempDir = $this->getTempDir();

        $this->container->get('shopware_dump_service')->dump(
            $shopwarePath,
            $tempDir,
            $this->getDatabaseConfiguration($shopwarePath),
            !$noMedia
        );
    }

    protected function getTempDir()
    {
        $tempDirectory = sys_get_temp_dir();
        $tempDirectory .= '/shopware-dump-' . uniqid();
        mkdir($tempDirectory, 0777, true);

        return $tempDirectory;
    }

    private function getDatabaseConfiguration($shopwarePath)
    {
        $content = include $shopwarePath . 'config.php';

        $user = isset($content['db']['username']) ? $content['db']['username'] : null;
        $password = isset($content['db']['password']) ? $content['db']['password'] : null;
        $dbname = isset($content['db']['dbname']) ? $content['db']['dbname'] : null;
        $host = isset($content['db']['host']) ? $content['db']['host'] : null;
        $port = isset($content['db']['port']) ? $content['db']['port'] : 3306;

        $struct = new ConnectionInfo();

        $struct->databaseName = $dbname;
        $struct->username = $user;
        $struct->password = $password;
        $struct->host = $host;
        $struct->port = $port;

        return $struct;


    }
}