<?php

namespace ShopwarePrivate\Account\Command;

use ShopwarePrivate\Account\Services\Account\AccountFactory;
use Shopware\PluginInfo\InfoDecorator;
use ShopwareCli\Command\BaseCommand;
use ShopwareCli\Services\GitUtil;
use ShopwareCli\Utilities;
use Symfony\Component\Console\Helper\DialogHelper;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class UploadCommand extends BaseCommand
{
    /** @var  Utilities */
    protected $utilities;

    protected $zipDir;

    /**
     * @return GitUtil
     */
    private function getGitUtility()
    {
        return $this->container->get('git_util');
    }

    /**
     * @return AccountFactory
     */
    private function getAccountFactory()
    {
        return $this->container->get('account_service_factory');
    }

    protected function configure()
    {
        $this->setName('plugin:up')->setDescription('Upload/update a plugin to/for the store')->addArgument(
            'names',
            InputArgument::IS_ARRAY,
            'Name of the plugin to update'
        )->addOption(
            'useZip',
            null,
            InputOption::VALUE_OPTIONAL,
            'Instead of checking out from stash, use a given zip to upload a file'
        )->addOption(
            'small',
            null,
            InputOption::VALUE_NONE,
            'If set, the task will yell in uppercase letters'
        )->addOption(
            'useHttp',
            null,
            InputOption::VALUE_NONE,
            'Checkout the repo via HTTP'
        )->addOption(
            'branch',
            '-b',
            InputOption::VALUE_OPTIONAL,
            'Checkout the given branch'
        );
    }

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->zipDir = getcwd();
    }

    protected function prepareZipFile($zipFile)
    {
        if (file_exists($this->zipDir . '/' . $zipFile)) {
            return $this->zipDir . '/' . $zipFile;
        } elseif (file_exists($zipFile)) {
            return $zipFile;
        }

        throw new \RuntimeException("Could not find file {$zipFile}");
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $names = $input->getArgument('names');
        $small = $input->getOption('small');
        $useHttp = $input->getOption('useHttp');

        $branch = $input->getOption('branch');
        $zip = $input->getOption('useZip');

        if ($zip) {
            $zip = $this->prepareZipFile($zip);
        }

        /** @var DialogHelper $dialog */
        $dialog = $this->getHelperSet()->get('dialog');

        $this->container->get('io_service')->cls();

        $this->container->get('plugin_column_renderer')->setSmall($small);
        $interactionManager = $this->container->get('plugin_operation_manager');

        $params = array(
            'output' => $output,
            'branch' => $branch,
            'zip' => $zip
        );

        if ($zip) {
            $this->doUpdate('', $params);

            return;
        }

        if (!empty($names)) {
            $interactionManager->searchAndOperate($names, array($this, 'doUpdate'), $params);

            return;
        }

        $interactionManager->operationLoop(array($this, 'doUpdate'), $params);
    }

    public function doUpdate($plugin, $params)
    {
        /** @var DialogHelper $dialog */
        $dialog = $this->getHelperSet()->get('dialog');
        /** @var OutputInterface $output */
        $output = $params['output'];

        $zipFile = $params['zip'];
        if (!$zipFile) {
            $tempDir = $this->getTempDir();
            $zipFile = $this->container->get('zip_service')->zip($plugin, $tempDir, $tempDir, $params['branch']);
        } else {
            $tempDir = $this->getTempDir();
            $this->container->get('process_executor')->execute("unzip -d {$tempDir} $zipFile");
        }

        /** @var InfoDecorator $info */
        $info = $this->container->get('plugin_info')->get($zipFile);

        $updated = $this->getAccountFactory()->factory()->updatePlugin(
            $info,
            $params['zip'] ? $this->getPluginNameFromBootstrap($zipFile) : $plugin->name,
            $zipFile
        );

        if ($updated && !$params['zip']) {
            $tag = $info->getCurrentVersion();
            $this->tag(
                $tempDir . '/' . $plugin->module . '/' . $plugin->name . '/',
                $tag
            );

            $this->createBranch(
                $tempDir . '/' . $plugin->module . '/' . $plugin->name . '/',
                $tag,
                $info->getCompatibilities()['minimumVersion']
            );
        }

        if ($params['zip']) {
            exit(0);
        }
    }

    /**
     * Tags a git release
     *
     * @param string $dir Git dir
     * @param string $tag tag to set
     */
    protected function tag($dir, $tag)
    {
        $tag = escapeshellarg($tag);
        $this->getGitUtility()->run(
            "-C {$dir} tag -f {$tag}"
        );
        $this->getGitUtility()->run(
            "-C {$dir} push --tags -f"
        );

        $this->container->get('io_service')->writeln('Tag ' . $tag . ' created / updated');
    }

    /**
     * @param string $dir
     * @param string $version
     * @param string $swMinVersion
     * @throws \RuntimeException
     */
    private function createBranch($dir, $version, $swMinVersion)
    {
        $versions = explode('.', $version);
        $majorVersion = (int) $versions[0];
        $minorVersion = (int) $versions[1];

        try {
            $this->getGitUtility()->run(
                "-C {$dir} checkout -b {$majorVersion}.{$minorVersion}-SW{$swMinVersion}"
            );
            $this->getGitUtility()->run(
                "-C {$dir} push origin {$majorVersion}.{$minorVersion}-SW{$swMinVersion}"
            );
            $this->container->get('io_service')->writeln(
                'Branch ' .
                $majorVersion . '.' . $minorVersion .
                '-SW' . $swMinVersion .
                ' created / updated'
            );
        } catch (\RuntimeException $runtimeException) {
            if (strpos($runtimeException->getMessage(), 'already exists') !== false) {
                $this->container->get('io_service')->ask('Branch already exists. No new branch will be created. Confirm:');
            } else {
                throw $runtimeException;
            }
        }
    }

    private function getZip($zipFile)
    {
        $zip = new \ZipArchive();
        if ($zip->open($zipFile) !== true) {
            throw new \RuntimeException("Could not open zip archive {$zipFile}");
        }

        return $zip;
    }

    private function getPluginNameFromBootstrap($file)
    {
        $zip = $this->getZip($file);

        $pattern = "#(?P<namespace>Backend|Core|Frontend)/(?P<name>.*)/(?P<file>Bootstrap\.php)#i";

        for ($i = 0; $i < $zip->numFiles; $i++) {
            $filename = $zip->getNameIndex($i);

            $matches = array();
            if (preg_match($pattern, $filename, $matches)) {
                return $matches['name'];
            }
        }

        throw new  \RuntimeException("Could not determine the plugin's technical name from the zip archive");
    }

    protected function getTempDir()
    {
        $tempDirectory = sys_get_temp_dir();
        $tempDirectory .= '/plugin-inst-' . uniqid();
        mkdir($tempDirectory, 0777, true);

        $this->container->get('utilities')->changeDir($tempDirectory);

        return $tempDirectory;
    }

    protected function getZipDir()
    {
        return $this->zipDir;
    }
}
