<?php

namespace ShopwarePrivate\Account\Services\Account;

use ShopwareCli\Services\Rest\Curl\RestClient;
use Symfony\Component\Console\Question\Question;

class AccountFactory
{
    protected $container;

    public function __construct($container)
    {
        $this->container = $container;
    }

    public function factory()
    {
        $config = $this->container->get('config');

        $user = @$config['account']['user'];
        $password = @$config['account']['password'];

        while (empty($user)) {
            $user = $this->container->get('io_service')->ask("Please enter your account username: ");
        }
        while (empty($password)) {
            $question = new Question("Please enter your account password (output will be hidden): ");
            $question->setHidden(true);
            $password = $this->container->get('io_service')->ask($question);
        }


        $api = new RestClient('https://api.shopware.com', "", "", array(
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => 'gzip'
        ));

        $loginService = new LoginService($api);

        return new Account(
            new PluginService(
                $api
            ),
            $this->container->get('io_service'),
            $loginService,
            $user,
            $password
        );

    }
}
