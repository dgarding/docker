<?php

namespace ShopwarePrivate\Account\Services\Account;

use Shopware\PluginInfo\InfoDecorator;
use ShopwareCli\Services\IoService;

class Account
{
    /** @var \ShopwarePrivate\Account\Services\Account\PluginService */
    protected $pluginService;

    /**
     * @var IoService
     */
    private $ioService;
    /**
     * @var LoginService
     */
    private $loginService;
    private $username;
    private $password;

    public function __construct(
        PluginService $pluginService,
        IoService $ioService,
        LoginService $loginService,
        $username,
        $password
    ) {
        $this->pluginService = $pluginService;
        $this->ioService = $ioService;
        $this->loginService = $loginService;
        $this->username = $username;
        $this->password = $password;
    }

    public function updatePlugin(InfoDecorator $info, $name, $zipFile)
    {
        $changelogs = $this->prepareChangelogs($info);
        $versions = $this->prepareVersions($info);
        sort($versions);

        $this->reviewChanges($name, $info, $changelogs, $versions);

        $this->ioService->write('Logging in as ' . $this->username);
        $session = $this->loginService->login($this->username, $this->password);
        $this->pluginService->setSession($session);
        $this->ioService->writeln('. <fg=green>Done</fg=green>');


        $this->ioService->write('Finding plugin ' . $name);
        // Get the (existing) plugin that's going to be updated
        $plugin = $this->pluginService->getPluginByNumber($name);
        $this->ioService->writeln('. <fg=green>Done</fg=green>');

        $this->ioService->write('Finding binary for ' . $info->getCurrentVersion());
        // get an existing binary object for the current version (if available)
        $binary = $this->pluginService->getPluginBinary($plugin, $info->getCurrentVersion());
        $this->ioService->writeln('. <fg=green>Done</fg=green>');

        $this->ioService->write('Uploading file');
        // update the existing binary or create a new one
        $binary = $this->pluginService->upload(
            $name,
            $zipFile,
            $plugin['id'],
            $binary ? $binary['id'] : null
        );
        $this->ioService->writeln('. <fg=green>Done</fg=green>');

        $this->ioService->write('Setting version and changelog for ' . $info->getCurrentVersion());
        $binary['version'] = $info->getCurrentVersion();
        $binary = $this->pluginService->updateBinary(
            $plugin,
            $binary,
            $changelogs,
            $versions
        );
        $this->ioService->writeln('. <fg=green>Done</fg=green>');

        $this->ioService->write('Trigger review');
        $result = $this->pluginService->triggerReview($plugin['id']);
        $this->ioService->writeln('. <fg=green>Done</fg=green>');
        if ($result === true) {
            $this->ioService->writeln("Review requested");
        } else {
            $this->ioService->writeln("Review requested: " . $result['reason']);
        }

        $this->ioService->write('Logging out');
        $this->loginService->logout($session->token);
        $this->ioService->writeln('. <fg=green>Done</fg=green>');

        $this->ioService->writeln("");
        $this->ioService->writeln('<fg=green>All Done</fg=green>');
        $this->ioService->writeln("");

        return true;
    }

    /**
     * @param InfoDecorator $info
     * @return array
     */
    private function prepareVersions(InfoDecorator $info)
    {
        // Get shopware versions - but remove 3.x versions
        // also remove versions we are not compatible with
        return array_filter(
            $this->pluginService->getSoftwareVersions(),
            function ($version) use ($info) {
                return version_compare($version, '4.0.0', '>') && $info->isCompatibleWith($version);
            }
        );
    }

    /**
     * @param InfoDecorator $info
     * @return array
     * @throws \RuntimeException
     */
    private function prepareChangelogs(InfoDecorator $info)
    {
        $currentVersion = $info->getCurrentVersion();

        $logs = array();

        try {
            $logs[] = array('text' => $info->getChangelog($currentVersion, 'de'), 'locale' => array('name' => 'de_DE'));
        } catch(\OutOfRangeException $e) {
        }

        try {
            $logs[] = array('text' => $info->getChangelog($currentVersion, 'en'), 'locale' => array('name' => 'en_GB'));
        } catch(\OutOfRangeException $e) {
        }

        if (empty($logs)) {
            throw new \RuntimeException("No changelogs found for version {$currentVersion}");
        }

        // Enable newlines
        $logs = array_map(
            function ($log) {
                $log['text'] = str_replace(';', "<br>\r\n", $log['text']);

                return $log;
            },
            $logs
        );

        return $logs;
    }

    /**
     * @param $name
     * @param $info
     * @param $changelogs
     * @param $versions
     */
    private function reviewChanges($name, $info, $changelogs, $versions)
    {
        $this->ioService->writeln("");
        $this->ioService->writeln("Will upload $name in version {$info->getCurrentVersion()}");

        $this->ioService->writeln("Changelogs:");
        foreach ($changelogs as $log) {
            $this->ioService->writeln('<fg=green>' . $log['locale']['name'] . "</fg=green>:\r\n " . $log['text']);
        }
        $this->ioService->writeln("Compatible versions: " . implode(', ', $versions));
        $this->ioService->writeln("");
        $this->ioService->ask("Hit enter to start, STRG+C to cancel");
    }
}
